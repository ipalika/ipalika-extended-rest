CREATE TABLE IF NOT EXISTS `user` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`full_name` VARCHAR(30) NOT NULL COLLATE 'utf8_general_ci',
	`username` VARCHAR(30) NOT NULL COLLATE 'utf8_general_ci',
	`email` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`password` VARCHAR(60) NOT NULL COLLATE 'utf8_general_ci',
	`mobile_number` VARCHAR(10) NOT NULL COLLATE 'utf8_general_ci',
	`locked` BIT(1) NOT NULL DEFAULT b'1',
	`first_login` BIT(1) NULL DEFAULT b'1',
	`enabled` BIT(1) NOT NULL DEFAULT b'1',
	`expired` BIT(1) NOT NULL DEFAULT b'1',
	`registered_date` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`ward_no` INT(11) NOT NULL DEFAULT '1',
	`signature` varchar(200) DEFAULT NULL,
	`stamp` varchar(200) DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `username` (`username`) USING BTREE,
	UNIQUE INDEX `mobile_number` (`mobile_number`) USING BTREE,
	UNIQUE INDEX `email` (`email`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `user` (`id`, `full_name`, `username`, `email`, `password`, `mobile_number`, `locked`, `first_login`, `enabled`, `expired`, `registered_date`, `ward_no`)
 VALUES
(1, 'Umesh Bhujel', 'yoomes', 'yoomesbhujel@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849931288', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 0),
(2, 'Pujan KC', 'pujanov', 'pujanov69@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399058', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 1),
(3, 'Demo SuperAdmin', 'demosuperadmin', 'demosuperadmin@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399051', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 0),
(4, 'Demo CentralAdmin', 'democentraladmin', 'democentraladmin@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399052', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 1),
(5, 'Demo WardAdmin', 'demowardadmin', 'demowardadmin@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399053', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 1),
(6, 'Demo Surveyor', 'demosurveyor', 'demosurveyor@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399054', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 1),
(7, 'Demo WardSecretary', 'demowardsecretary', 'demowardsecretary@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399055', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 1),
(8, 'Demo Registrar', 'demoregistrar', 'demoregistrar@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399056', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 1),
(9, 'Demo Printer', 'demoprinter', 'demoprinter@gmail.com', '$2a$11$H9wDLxAPTX5qp0doFKank.w6vgB7xPo1CJojH2AC0ovBY4Iu31oTS', '9849399059', 0, 0, 1, 0, CURRENT_TIMESTAMP(), 1);


CREATE TABLE IF NOT EXISTS `role` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`role` VARCHAR(15) NOT NULL,
	`role_nepali` VARCHAR(15) NOT NULL,
	PRIMARY KEY (`id`)
);


CREATE TABLE IF NOT EXISTS `registration_report` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`form_info_id` INT(11) NOT NULL,
	`form_title` VARCHAR(150) NOT NULL,
	 `unverified` INT(11) NOT NULL,
	 `stamped` INT(11) NOT NULL,
	 `registered` INT(11) NOT NULL,
	 `processed` INT(11) NOT NULL,
	 `verified` INT(11) NOT NULL,
	 `total` INT(11) NOT NULL,

	 PRIMARY KEY (`id`),
	 UNIQUE KEY `form_info_id` (`form_info_id`)
);

CREATE TABLE IF NOT EXISTS `form_info` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`form_type` VARCHAR(150) NOT NULL,
	`form_description` VARCHAR(150) NOT NULL,

	PRIMARY KEY (`id`),
	 UNIQUE KEY `form_type` (`form_type`)
);

INSERT INTO `role` (`id`, `role`, `role_nepali`) VALUES
    (1, 'SUPER_ADMIN','सुपर एडमिन' ),
    (2, 'CENTRAL_ADMIN', 'केन्द्रिय एडमिन'),
	(3, 'WARD_ADMIN', 'वडा एडमिन'),
	(4, 'SURVEYOR', 'तथ्यांक संकलक'),
	(5, 'WARD_SECRETARY', 'वडा सचिव'),
	(6, 'REGISTRAR', 'रजिस्ट्रार'),
	(7, 'PRINTER', 'प्रिन्टर');

CREATE TABLE IF NOT EXISTS `user_role` (
	`user_id` INT(11) NOT NULL,
	`role_id` INT(11) NOT NULL,
	PRIMARY KEY (`user_id`, `role_id`)
);

INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (1, 1);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (2, 3);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (3, 1);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (4, 2);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (5, 3);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (6, 4);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (7, 5);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (8, 6);
INSERT INTO `user_role`(`user_id`, `role_id`) VALUES (9, 7);


CREATE TABLE IF NOT EXISTS `form` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`form_id` VARCHAR(50) NOT NULL DEFAULT '0',
	`form_name` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `form_id` (`form_id`)
);


CREATE TABLE IF NOT EXISTS `question` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`indx` INT(11) NOT NULL,
	`question_id` VARCHAR(50) NOT NULL,
	`description` TEXT NOT NULL,
	`group` VARCHAR(50) NOT NULL,
	`required` INT(11) NOT NULL DEFAULT 0,
	`type_id` INT(11) NOT NULL DEFAULT 1,
	`form_id` INT(11) NOT NULL,
	`reportable` BIT(1) NOT NULL DEFAULT b'0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `question_id` (`question_id`),
	UNIQUE INDEX `indx` (`indx`)
);


CREATE TABLE IF NOT EXISTS `question_type` (
	`type_id` INT(11) NOT NULL AUTO_INCREMENT,
	`type_name` VARCHAR(20) NOT NULL DEFAULT '0',
	PRIMARY KEY (`type_id`)
);

INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('1', 'CHECKBOX');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('2', 'RADIO');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('3', 'TEXT');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('4', 'NUMBER');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('5', 'DATE');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('6', 'GPS');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('7', 'IMAGE');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('8', 'MULTI_TEXT');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('9', 'RADIO_D');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('10', 'RATING_M');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('11', 'RATING');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('12', 'CHECKBOX_N');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('13', 'DROPDOWN');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('14', 'DISTRICT');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('15', 'WARD');
INSERT INTO `ipalika`.`question_type` (`type_id`, `type_name`) VALUES ('16', 'RADIO_M');

CREATE TABLE IF NOT EXISTS `options` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`option_id` INT NOT NULL,
	`option_text` VARCHAR(100) NOT NULL,
	`question_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
);


CREATE TABLE IF NOT EXISTS `differently_abled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `differently_abled_nep` varchar(200) DEFAULT NULL,
  `differently_abled_eng` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;



CREATE TABLE `answer` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`form_id` INT(11) NOT NULL DEFAULT 0,
	`entry_date` DATETIME NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`duration` VARCHAR(50) NOT NULL DEFAULT '0',
	`filled_id` VARCHAR(50) NOT NULL DEFAULT '0',
	`added_by` INT(11) NOT NULL DEFAULT 0,
	`verified_by` INT(11) NOT NULL DEFAULT 0,
	`modified_by` INT(11) NOT NULL DEFAULT 0,
	`answer_1` TEXT NULL,
	`answer_2` TEXT NULL,
	`answer_3` TEXT NULL,
	`answer_4` TEXT NULL,
	`answer_5` TEXT NULL,
	`answer_6` TEXT NULL,
	`answer_7` TEXT NULL,
	`answer_8` TEXT NULL,
	`answer_9` TEXT NULL,
	`answer_10` TEXT NULL,
	`answer_11` TEXT NULL,
	`answer_12` TEXT NULL,
	`answer_13` TEXT NULL,
	`answer_14` TEXT NULL,
	`answer_15` TEXT NULL,
	`answer_16` TEXT NULL,
	`answer_17` TEXT NULL,
	`answer_18` TEXT NULL,
	`answer_19` TEXT NULL,
	`answer_20` TEXT NULL,
	`answer_21` TEXT NULL,
	`answer_22` TEXT NULL,
	`answer_23` TEXT NULL,
	`answer_24` TEXT NULL,
	`answer_25` TEXT NULL,
	`answer_26` TEXT NULL,
	`answer_27` TEXT NULL,
	`answer_28` TEXT NULL,
	`answer_29` TEXT NULL,
	`answer_30` TEXT NULL,
	`answer_31` TEXT NULL,
	`answer_32` TEXT NULL,
	`answer_33` TEXT NULL,
	`answer_34` TEXT NULL,
	`answer_35` TEXT NULL,
	`answer_36` TEXT NULL,
	`answer_37` TEXT NULL,
	`answer_38` TEXT NULL,
	`answer_39` TEXT NULL,
	`answer_40` TEXT NULL,
	`answer_41` TEXT NULL,
	`answer_42` TEXT NULL,
	`answer_43` TEXT NULL,
	`answer_44` TEXT NULL,
	`answer_45` TEXT NULL,
	`answer_46` TEXT NULL,
	`answer_47` TEXT NULL,
	`answer_48` TEXT NULL,
	`answer_49` TEXT NULL,
	`answer_50` TEXT NULL,
	`answer_51` TEXT NULL,
	`answer_52` TEXT NULL,
	`answer_53` TEXT NULL,
	`answer_54` TEXT NULL,
	`answer_55` TEXT NULL,
	`answer_56` TEXT NULL,
	`answer_57` TEXT NULL,
	`answer_58` TEXT NULL,
	`answer_59` TEXT NULL,
	`answer_60` TEXT NULL,
	`answer_61` TEXT NULL,
	`answer_62` TEXT NULL,
	`answer_63` TEXT NULL,
	`answer_64` TEXT NULL,
	`answer_65` TEXT NULL,
	`answer_66` TEXT NULL,
	`answer_67` TEXT NULL,
	`answer_68` TEXT NULL,
	`answer_69` TEXT NULL,
	`answer_70` TEXT NULL,
	`answer_71` TEXT NULL,
	`answer_72` TEXT NULL,
	`answer_73` TEXT NULL,
	`answer_74` TEXT NULL,
	`answer_75` TEXT NULL,
	`answer_76` TEXT NULL,
	`answer_77` TEXT NULL,
	`answer_78` TEXT NULL,
	`answer_79` TEXT NULL,
	`answer_80` TEXT NULL,
	`answer_81` TEXT NULL,
	`answer_82` TEXT NULL,
	`answer_83` TEXT NULL,
	`answer_84` TEXT NULL,
	`answer_85` TEXT NULL,
	`answer_86` TEXT NULL,
	`answer_87` TEXT NULL,
	`answer_88` TEXT NULL,
	`answer_89` TEXT NULL,
	`answer_90` TEXT NULL,
	`answer_91` TEXT NULL,
	`answer_92` TEXT NULL,
	`answer_93` TEXT NULL,
	`answer_94` TEXT NULL,
	`answer_95` TEXT NULL,
	`answer_96` TEXT NULL,
	`answer_97` TEXT NULL,
	`answer_98` TEXT NULL,
	`answer_99` TEXT NULL,
	`answer_100` TEXT NULL,
	`deleted` bit(1) NOT NULL DEFAULT b'0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `filled_id` (`filled_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `favourite_place` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`fav_place_id` VARCHAR(100) NOT NULL,
	`fav_place_name` TEXT,
	`fav_place_type` TEXT,
	`fav_place_desc` TEXT,
	`fav_place_photo` TEXT,
	`fav_place_location` TEXT,
	`fav_place_ward` TEXT,
	`deleted` bit(1) NOT NULL DEFAULT b'0',
	PRIMARY KEY (`id`),
	UNIQUE KEY `fav_place_id_UNIQUE` (`fav_place_id`)
);


CREATE TABLE IF NOT EXISTS `family_member` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`family_id` VARCHAR(50) NOT NULL,
	`full_name` VARCHAR(50) NOT NULL,
	`relation_id` INT(11) NOT NULL,
	`age` INT(11) NOT NULL,
	`gender_id` INT(11) NOT NULL,
	`marital_status` INT(11) NOT NULL,
	`qualification_id` INT(11) NOT NULL,
	`occupation` VARCHAR(50) NOT NULL,
	`has_voter_id` BIT(1) NOT NULL DEFAULT b'0',
	`health_status` VARCHAR(50) NOT NULL DEFAULT '0',
	`member_id` VARCHAR(50) NOT NULL DEFAULT '0',
	`is_dead` BIT(1) NOT NULL DEFAULT b'0',
	`dob_ad` VARCHAR(45) NOT NULL,
	`dob_bs` VARCHAR(45) NOT NULL,
	`deleted` bit(1) NOT NULL DEFAULT b'0',
	PRIMARY KEY (`id`),
	UNIQUE KEY `member_id_UNIQUE` (`member_id`)
);


CREATE TABLE IF NOT EXISTS `family_relation` (
	`relation_id` INT(11) NOT NULL AUTO_INCREMENT,
	`relation_nepali` VARCHAR(50) NOT NULL DEFAULT '0',
	`relation_english` VARCHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`relation_id`)
);

CREATE TABLE IF NOT EXISTS `gender` (
	`gender_id` INT(11) NOT NULL AUTO_INCREMENT,
	`gender_nepali` VARCHAR(45) DEFAULT NULL,
	`gender_english` VARCHAR(45) DEFAULT NULL,
	PRIMARY KEY (`gender_id`)
);

CREATE TABLE IF NOT EXISTS `academic_qualification` (
  `qualification_id` INT(11) NOT NULL AUTO_INCREMENT,
  `qualification_nep` VARCHAR(50) DEFAULT NULL,
  `qualification_eng` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`qualification_id`)
);


CREATE TABLE `population_report` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`based_on` VARCHAR(50) NOT NULL DEFAULT '0',
	`option_1` DOUBLE NOT NULL DEFAULT 0,
	`option_2` DOUBLE NOT NULL DEFAULT 0,
	`option_3` DOUBLE NOT NULL DEFAULT 0,
	`option_4` DOUBLE NOT NULL DEFAULT 0,
	`option_5` DOUBLE NOT NULL DEFAULT 0,
	`option_6` DOUBLE NOT NULL DEFAULT 0,
	`option_7` DOUBLE NOT NULL DEFAULT 0,
	`option_8` DOUBLE NOT NULL DEFAULT 0,
	`option_9` DOUBLE NOT NULL DEFAULT 0,
	`option_10` DOUBLE NOT NULL DEFAULT 0,
	`option_11` DOUBLE NOT NULL DEFAULT 0,
	`option_12` DOUBLE NOT NULL DEFAULT 0,
	`option_13` DOUBLE NOT NULL DEFAULT 0,
	`option_14` DOUBLE NOT NULL DEFAULT 0,
	`option_15` DOUBLE NOT NULL DEFAULT 0,
	`total` DOUBLE NOT NULL DEFAULT 0,
	`ward` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `based_on` (`based_on`)
);


CREATE TABLE `question_report` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`question_id` INT(11) NOT NULL DEFAULT 0,
	`option_1` DOUBLE NOT NULL DEFAULT 0,
	`option_2` DOUBLE NOT NULL DEFAULT 0,
	`option_3` DOUBLE NOT NULL DEFAULT 0,
	`option_4` DOUBLE NOT NULL DEFAULT 0,
	`option_5` DOUBLE NOT NULL DEFAULT 0,
	`option_6` DOUBLE NOT NULL DEFAULT 0,
	`option_7` DOUBLE NOT NULL DEFAULT 0,
	`option_8` DOUBLE NOT NULL DEFAULT 0,
	`option_9` DOUBLE NOT NULL DEFAULT 0,
	`option_10` DOUBLE NOT NULL DEFAULT 0,
	`option_11` DOUBLE NOT NULL DEFAULT 0,
	`option_12` DOUBLE NOT NULL DEFAULT 0,
	`option_13` DOUBLE NOT NULL DEFAULT 0,
	`option_14` DOUBLE NOT NULL DEFAULT 0,
	`option_15` DOUBLE NOT NULL DEFAULT 0,
	`total` DOUBLE NOT NULL DEFAULT 0,
	`ward` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `question_id` (`question_id`)
);

CREATE TABLE IF NOT EXISTS `districts` (
  `district_id` INT(11) NOT NULL AUTO_INCREMENT,
  `district_name_nep` VARCHAR(50) NOT NULL,
  `district_name_eng` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`district_id`)
);

CREATE TABLE IF NOT EXISTS `favourite_place_type` (
  `type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `place_type_nep` VARCHAR(50) NOT NULL,
  `place_type_eng` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`type_id`)
);

CREATE TABLE IF NOT EXISTS `marital_status` (
  `marital_status_id` INT(11) NOT NULL AUTO_INCREMENT,
  `marital_status_nep` VARCHAR(50) NOT NULL,
  `marital_status_eng` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`marital_status_id`)
);

CREATE TABLE IF NOT EXISTS `ward` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`ward_number` INT(11) NOT NULL,
	`location` VARCHAR(100) NOT NULL DEFAULT '0',
	`name` VARCHAR(100) NOT NULL DEFAULT '0',
	`ward_description` TEXT,
	`main_person` VARCHAR(5000) NOT NULL DEFAULT '0',
	`contact_no` VARCHAR(50) NOT NULL DEFAULT '0',
	`building_image` VARCHAR(300) DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE KEY `ward_number_UNIQUE` (`ward_number`)
);


CREATE TABLE `death_record` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`registration_number` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`member_id` VARCHAR(15) NOT NULL COLLATE 'utf8_general_ci',
	`death_cause` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	`demise_date` DATETIME NOT NULL,
	`place` VARCHAR(150) NOT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `registration_number` (`registration_number`) USING BTREE,
	UNIQUE INDEX `member_id` (`member_id`) USING BTREE
);


CREATE TABLE `extra_report` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`report_name` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`data` INT(11) NOT NULL DEFAULT '0',
	`ward` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `report_name` (`report_name`) USING BTREE
);

CREATE TABLE `favourite_place_report` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`place_type` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`data` INT(11) NOT NULL DEFAULT '0',
	`ward` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `place_type` (`place_type`) USING BTREE
);


-- Dumping structure for table igas.lg_districts
CREATE TABLE IF NOT EXISTS `lg_districts` (
  `district_id` int NOT NULL AUTO_INCREMENT,
  `district_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `province_id` int NOT NULL,
  `disabled` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`district_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='List of Districts';


-- Dumping structure for table ipalika.lg_municipality
CREATE TABLE IF NOT EXISTS `lg_municipality` (
  `municipality_id` int NOT NULL AUTO_INCREMENT,
  `municipality_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `province_id` int NOT NULL,
  `district_id` int NOT NULL,
  `disabled` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`municipality_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='List of Municipality';


-- Dumping structure for table ipalika.lg_province
CREATE TABLE IF NOT EXISTS `lg_province` (
  `province_id` int NOT NULL AUTO_INCREMENT,
  `province_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `disabled` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`province_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Provinces of Nepal';


-- Dumping structure for table ipalika.lg_ward
CREATE TABLE IF NOT EXISTS `lg_ward` (
  `ward_id` int NOT NULL AUTO_INCREMENT,
  `ward_description` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `municipality_id` int NOT NULL DEFAULT '0',
  `disabled` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ward_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;


-- Dumping structure for table ipalika.egovernance_birth_certificate
CREATE TABLE IF NOT EXISTS `egovernance_birth_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
--  `ward_no` int(2) NOT NULL,
  `lg_province` int NOT NULL DEFAULT '0',
  `lg_district` int NOT NULL DEFAULT '0',
  `lg_municipality` int NOT NULL DEFAULT '0',
  `lg_ward` int NOT NULL DEFAULT '0',
  `tole` varchar(80) DEFAULT '',
  `name_eng` varchar(80) DEFAULT '',
  `name_nep` varchar(80) DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `dob_ad` varchar(45) NOT NULL DEFAULT '',
  `dob_bs` varchar(45) DEFAULT '',
  `informant_name_eng` varchar(80) DEFAULT '',
  `informant_name_nep` varchar(80) DEFAULT '',
  `informant_id_eng` varchar(80) DEFAULT '',
  `informant_id_nep` varchar(80) DEFAULT '',
  `dor_ad` varchar(45) DEFAULT NULL,
  `dor_bs` varchar(45) DEFAULT '',
  `father_name_eng` varchar(80) DEFAULT '',
  `father_name_nep` varchar(80) DEFAULT '',
  `father_id_eng` varchar(80) DEFAULT '',
  `father_id_nep` varchar(80) DEFAULT NULL,
  `mother_name_eng` varchar(80) DEFAULT NULL,
  `mother_name_nep` varchar(80) DEFAULT NULL,
  `mother_id_eng` varchar(80) DEFAULT NULL,
  `mother_id_nep` varchar(80) DEFAULT NULL,
  `permanent_address_eng` varchar(200) DEFAULT NULL,
  `permanent_address_nep` varchar(200) DEFAULT NULL,
  `birth_place_eng` varchar(200) DEFAULT NULL,
  `birth_place_nep` varchar(200) DEFAULT NULL,
  `birth_place_certificate` varchar(200) DEFAULT NULL,
  `registrar_name_eng` varchar(80) DEFAULT NULL,
  `registrar_name_nep` varchar(80) DEFAULT NULL,
  `registrar_signature` varchar(100) DEFAULT NULL,
  `registrar_signature_date` varchar(45) DEFAULT NULL,
  `ward_secretary_name` varchar(80) DEFAULT NULL,
  `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) NOT NULL DEFAULT b'0',
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL DEFAULT '0',
  `grandfather_name_eng` varchar(80) DEFAULT '0',
  `grandfather_name_nep` varchar(80) DEFAULT '0',
  `father_id_img` varchar(200) DEFAULT NULL,
  `mother_id_img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Dumping structure for table ipalika.egovernance_relationship_living
CREATE TABLE IF NOT EXISTS `egovernance_relationship_living` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `ward_no` int(2) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL,
  `relative_name` varchar(200) NOT NULL,
  `relationship` varchar(30) NOT NULL,
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `application_photo` varchar(200) NOT NULL,
  `citizenship_photo_applicant` varchar(200) NOT NULL,
  `pp_photo_applicant` varchar(200) NOT NULL,
  `citizenship_photo_relative` varchar(200) NOT NULL,
  `pp_photo_relative` varchar(200) NOT NULL,
  `relationship_photo` varchar(200) NOT NULL,
  `tax_information_photo` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) NOT NULL DEFAULT b'0',
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='Relationship with living individual';



-- Dumping structure for table ipalika.egovernance_relationship_deceased
CREATE TABLE IF NOT EXISTS `egovernance_relationship_deceased` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `ward_no` int(2) NOT NULL DEFAULT 0,
  `name` varchar(200) NOT NULL,
  `deceased_name` varchar(200) NOT NULL,
  `relationship` varchar(30) NOT NULL,
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `application_photo` varchar(200) NOT NULL,
  `citizenship_photo_applicant` varchar(200) NOT NULL,
  `pp_photo_applicant` varchar(200) NOT NULL,
  `citizenship_photo_deceased` varchar(200) NOT NULL,
  `pp_photo_deceased` varchar(200) NOT NULL,
  `relationship_photo` varchar(200) NOT NULL,
  `death_certificate` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) NOT NULL DEFAULT b'0',
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='Relationship with deceased individual';



-- Dumping structure for table ipalika.egovernance_marriage_certificate
CREATE TABLE IF NOT EXISTS `egovernance_marriage_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL DEFAULT '0',
  `registration_no` varchar(50) DEFAULT '0',
  `marriage_application_photo` varchar(200) NOT NULL DEFAULT '0',
  `husband_name` varchar(200) NOT NULL DEFAULT '0',
  `husband_citizenship_photo` varchar(200) NOT NULL DEFAULT '0',
  `husband_citizenship_id` varchar(100) NOT NULL DEFAULT '0',
  `wife_name` varchar(200) NOT NULL DEFAULT '0',
  `wife_citizenship_photo` varchar(200) DEFAULT '0',
  `wife_citizenship_id` varchar(200) DEFAULT '0',
  `registrar_name_eng` varchar(80) DEFAULT NULL,
  `registrar_name_nep` varchar(80) DEFAULT NULL,
  `registrar_signature` varchar(100) DEFAULT NULL,
  `registrar_signature_date` varchar(45) DEFAULT NULL,
  `ward_secretary_name` varchar(80) DEFAULT NULL,
  `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `bride_family_citizenship_photo` varchar(200) NOT NULL DEFAULT '0',
  `bride_family_citizenship_id` varchar(200) DEFAULT '0',
  `tax_information_photo` varchar(200) DEFAULT '0',
  `email` varchar(200) NOT NULL DEFAULT '0',
  `phone` varchar(20) NOT NULL DEFAULT '0',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Marriage Registration';


-- Dumping structure for table ipalika.egovernance_death_certificate
CREATE TABLE IF NOT EXISTS `egovernance_death_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(2) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `dob_ad` varchar(45) NOT NULL DEFAULT '',
  `dob_bs` varchar(45) DEFAULT '',
  `date_of_demise_ad` varchar(45) NOT NULL DEFAULT '',
  `date_of_demise_bs` varchar(45) DEFAULT '',
  `demise_cause` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `informant_document` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `relationship_certificate_photo` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Death Registration';


-- Dumping structure for table ipalika.egovernance_divorce_certificate
CREATE TABLE IF NOT EXISTS `egovernance_divorce_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL DEFAULT '0',
  `registration_no` varchar(50) DEFAULT '0',
  `date_of_marriage_ad` varchar(45) NOT NULL DEFAULT '',
  `date_of_marriage_bs` varchar(45) NOT NULL DEFAULT '',
  `husband_name` varchar(200) NOT NULL DEFAULT '0',
  `permanent_address_husband` varchar(200) DEFAULT NULL,
  `ward_no_husband` int(2) NOT NULL,
  `husband_citizenship_id` varchar(100) NOT NULL DEFAULT '0',
  `husband_citizenship_photo` varchar(200) NOT NULL DEFAULT '0',
  `wife_name` varchar(200) NOT NULL DEFAULT '0',
  `permanent_address_wife` varchar(200) DEFAULT NULL,
  `ward_no_wife` int(2) NOT NULL,
  `wife_citizenship_id` varchar(200) DEFAULT '0',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `wife_citizenship_photo` varchar(200) NOT NULL DEFAULT '0',
  `application_photo` varchar(200) NOT NULL DEFAULT '0',
  `dissolution_certificate` varchar(200) NOT NULL DEFAULT '0',
  `email` varchar(200) NOT NULL DEFAULT '0',
  `phone` varchar(20) NOT NULL DEFAULT '0',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Divorce Registration';


-- Dumping structure for table ipalika.egovernance_disabled_certificate
CREATE TABLE IF NOT EXISTS `egovernance_disabled_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `disability_type` varchar(200) NOT NULL DEFAULT '',
  `disability_certificate_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Disability Registration';


-- Dumping structure for table ipalika.egovernance_marriage_verification
CREATE TABLE IF NOT EXISTS `egovernance_marriage_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(2) NOT NULL,
  `husband_name` varchar(200) NOT NULL DEFAULT '',
  `wife_name` varchar(200) NOT NULL DEFAULT '',
  `husband_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `husband_citizenship_id` varchar(100) NOT NULL DEFAULT '',
  `wife_citizenship_photo` varchar(200) DEFAULT '',
  `wife_citizenship_id` varchar(100) DEFAULT '',
  `migration_certificate_photo` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
  `marriage_certificate_photo` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_verification` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Marriage Verification';


-- Dumping structure for table ipalika.egovernance_alive_certificate
CREATE TABLE IF NOT EXISTS `egovernance_alive_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `pp_photo` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Alive Registration';


-- Dumping structure for table ipalika.egovernance_minor_certificate
CREATE TABLE IF NOT EXISTS `egovernance_minor_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `birth_certificate` varchar(200) NOT NULL DEFAULT '',
  `father_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `mother_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `pp_photo` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Minor Registration';



-- Dumping structure for table ipalika.egovernance_scholarship_certificate
CREATE TABLE IF NOT EXISTS `egovernance_scholarship_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `edu_qualification_certificate` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Student Scholarship Recommendation';


-- Dumping structure for table ipalika.egovernance_birth_verification
CREATE TABLE IF NOT EXISTS `egovernance_birth_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `birth_certificate_minor` varchar(200) DEFAULT '',
  `resettlement_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_verification` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Birth Date Verification';


-- Dumping structure for table ipalika.egovernance_permanent_residence
CREATE TABLE IF NOT EXISTS `egovernance_permanent_residence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `resettlement_certificate` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Permanent Residence Registration';


-- Dumping structure for table ipalika.egovernance_pipeline_connection
CREATE TABLE IF NOT EXISTS `egovernance_pipeline_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `proof_of_map_pass` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Water Pipeline Connection Recommendation';


-- Dumping structure for table ipalika.egovernance_land_marking
CREATE TABLE IF NOT EXISTS `egovernance_land_marking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `concerned_office_letter` varchar(200) NOT NULL DEFAULT '',
  `technical_report` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Land Marking Work Registration';


-- Dumping structure for table ipalika.egovernance_bahal_tax
CREATE TABLE IF NOT EXISTS `egovernance_bahal_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `bahal_agreement_photo` varchar(200) NOT NULL DEFAULT '',
  `registration_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Bahal Tax Registration';


-- Dumping structure for table ipalika.egovernance_house_building
CREATE TABLE IF NOT EXISTS `egovernance_house_building` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `building_map_pass` varchar(200) NOT NULL DEFAULT '',
  `construction_completion_certificate` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='House Building Authority';


-- Dumping structure for table ipalika.egovernance_ghar_kayam
CREATE TABLE IF NOT EXISTS `egovernance_ghar_kayam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_lalpurja` varchar(200) NOT NULL DEFAULT '',
  `onsite_report` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Ghar Kayam Sifaris';


-- Dumping structure for table ipalika.egovernance_poor_economy
CREATE TABLE IF NOT EXISTS `egovernance_poor_economy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `document_confirming_poor_1` varchar(200) DEFAULT '',
  `document_confirming_poor_2` varchar(200) DEFAULT '',
  `document_confirming_poor_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Poor Economy Condition Verification';


-- Dumping structure for table ipalika.egovernance_english_verification
CREATE TABLE IF NOT EXISTS `egovernance_english_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `proof_document_1` varchar(200) DEFAULT '',
  `proof_document_2` varchar(200) DEFAULT '',
  `proof_document_3` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='English Verification';


-- Dumping structure for table ipalika.egovernance_manjurinama_verification
CREATE TABLE IF NOT EXISTS `egovernance_manjurinama_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `proof_document_1` varchar(200) DEFAULT '',
  `proof_document_2` varchar(200) DEFAULT '',
  `proof_document_3` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Manjurinama Verification';


-- Dumping structure for table ipalika.egovernance_ghar_patal
CREATE TABLE IF NOT EXISTS `egovernance_ghar_patal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `house_map` varchar(200) NOT NULL DEFAULT '',
  `map_pass_certificate` varchar(200) NOT NULL DEFAULT '',
  `onsite_inspection_report` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Ghar Patal Verification';


-- Dumping structure for table ipalika.egovernance_strong_economy
CREATE TABLE IF NOT EXISTS `egovernance_strong_economy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `income_source_document` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) NOT NULL DEFAULT '',
  `other_document_1` varchar(200) DEFAULT '',
  `other_document_2` varchar(200) DEFAULT '',
  `other_document_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Strong Economic Condition Verification';


-- Dumping structure for table ipalika.egovernance_electricity_connection
CREATE TABLE IF NOT EXISTS `egovernance_electricity_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `source_of_right_document` varchar(200) NOT NULL DEFAULT '',
  `proof_of_map_pass` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `other_document_1` varchar(200) DEFAULT '',
  `other_document_2` varchar(200) DEFAULT '',
  `other_document_3` varchar(200) DEFAULT '',
  `ward_admin_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Electricity Connection Registration';


-- Dumping structure for table ipalika.egovernance_two_names
CREATE TABLE IF NOT EXISTS `egovernance_two_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) DEFAULT '',
  `doc_confirming_name_change_1` varchar(200) DEFAULT '',
  `doc_confirming_name_change_2` varchar(200) DEFAULT '',
  `doc_confirming_name_change_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Registration to confirm that a person with two names is same';


-- Dumping structure for table ipalika.egovernance_land_valuation
CREATE TABLE IF NOT EXISTS `egovernance_land_valuation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `current_land_price` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Land Valuation Verification';


-- Dumping structure for table ipalika.egovernance_business_not_operating
CREATE TABLE IF NOT EXISTS `egovernance_business_not_operating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `business_registration_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
  `onsite_report` varchar(200) NOT NULL DEFAULT '',
  `ghar_bahal_agreement` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Business Not Operating Registration';


-- Dumping structure for table ipalika.egovernance_land_dhanipurja
CREATE TABLE IF NOT EXISTS `egovernance_land_dhanipurja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) DEFAULT '1',
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Land Dhani-Purja Lost Registration';


-- Dumping structure for table ipalika.egovernance_rightful_person
CREATE TABLE IF NOT EXISTS `egovernance_rightful_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `relationship_proof_certificate` varchar(200) NOT NULL DEFAULT '',
  `onsite_sarjamin` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) DEFAULT '',
  `proof_document_1` varchar(200) DEFAULT '',
  `proof_document_2` varchar(200) DEFAULT '',
  `proof_document_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Rightful Person Verification';


-- Dumping structure for table ipalika.egovernance_details_disclosure
CREATE TABLE IF NOT EXISTS `egovernance_details_disclosure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `office_letter` varchar(200) NOT NULL DEFAULT '',
  `other_document_1` varchar(200) DEFAULT '',
  `other_document_2` varchar(200) DEFAULT '',
  `other_document_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Disclosure of details as per demand of other office';


-- Dumping structure for table ipalika.egovernance_कोठा_खोल्न_कार्य
CREATE TABLE IF NOT EXISTS `egovernance_कोठा_खोल्न_कार्य` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `bahal_agreement_photo` varchar(200) NOT NULL DEFAULT '',
  `district_admin_office_letter` varchar(200) NOT NULL DEFAULT '',
  `local_sarjamin_muchulka` varchar(200) DEFAULT '',
  `police_sarjamin_muchulka` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT=' कोठा खोल्न कार्य/रोहबरमा बस्ने कार्य';


-- Dumping structure for table ipalika.egovernance_बाटो_कायम
CREATE TABLE IF NOT EXISTS `egovernance_बाटो_कायम` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `survey_map` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `land_owner_approval_letter` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='नेपाल सरकारको नाममा बाटो कायम सिफारिस ';


-- Dumping structure for table ipalika.egovernance_संरक्षक_व्यक्तिगत
CREATE TABLE IF NOT EXISTS `egovernance_संरक्षक_व्यक्तिगत` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `guardian_giver_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `guardian_giver_birth_certificate` varchar(200) NOT NULL DEFAULT '',
  `guardian_receiver_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `guardian_receiver_birth_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `onsite_sarjamin_muchulka` varchar(200) DEFAULT '',
  `local_sarjamin_muchulka` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='संरक्षक सिफारिस (व्यक्तिगत)';


-- Dumping structure for table ipalika.egovernance_संरक्षक_संस्थागत
CREATE TABLE IF NOT EXISTS `egovernance_संरक्षक_संस्थागत` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `institution_renewal_certificate` varchar(200) NOT NULL DEFAULT '',
  `copy_of_legislation` varchar(200) DEFAULT '',
  `copy_of_rules` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `bahal_agreement_photo` varchar(200) DEFAULT '',
  `bahal_tax_information` varchar(200) DEFAULT '',
  `sarjamin_muchulka` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='संरक्षक सिफारिस (संस्थागत)';


-- Dumping structure for table ipalika.egovernance_व्यक्तिगत_विवरण
CREATE TABLE IF NOT EXISTS `egovernance_व्यक्तिगत_विवरण` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `proof_document_1` varchar(200) DEFAULT '',
  `proof_document_2` varchar(200) DEFAULT '',
  `proof_document_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='व्यक्तिगत विवरण सिफारिस';


-- Dumping structure for table ipalika.egovernance_विद्यालय_ठाउँसारी
CREATE TABLE IF NOT EXISTS `egovernance_विद्यालय_ठाउँसारी` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `school_registration_certificate` varchar(200) NOT NULL DEFAULT '',
  `local_level_renewal_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_current_place` varchar(200) DEFAULT '',   -- except government and community schools
  `tax_information_shifting_place` varchar(200) DEFAULT '',  -- except government and community schools
  `bahal_agreement_photo` varchar(200) DEFAULT '',
  `bahal_tax_information` varchar(200) DEFAULT '',
  `permanent_account_no_certificate` varchar(200) NOT NULL DEFAULT '',
  `inspection_report` varchar(200) NOT NULL DEFAULT '',
  `ward_office_permit_letter` varchar(200) NOT NULL DEFAULT '', -- Permit letter from the ward office of the place to be transferred
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='विद्यालय ठाउँसारी सिफारिस';


-- Dumping structure for table ipalika.egovernance_उद्योग_ठाउँसारी
CREATE TABLE IF NOT EXISTS `egovernance_उद्योग_ठाउँसारी` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `industry_registration_certificate` varchar(200) NOT NULL DEFAULT '',
  `local_level_renewal_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',   -- if it's your own house
  `bahal_agreement_photo` varchar(200) DEFAULT '',
  `bahal_tax_information` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='उद्योग ठाउँसारी सिफारिस';


-- Dumping structure for table ipalika.egovernance_mohi_lagat
CREATE TABLE IF NOT EXISTS `egovernance_mohi_lagat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `land_survey_map` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `landlord_proof_photo` varchar(200) NOT NULL DEFAULT '',
  `field_book` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='मोही लगत कट्टा सिफारिस';


-- Dumping structure for table ipalika.egovernance_house_land_transfer
CREATE TABLE IF NOT EXISTS `egovernance_house_land_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `relationship_certificate_of_deceased_petitioner` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_witness_citizenship_photo` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='घर जग्गा नामसारी सिफारिस';


-- Dumping structure for table ipalika.egovernance_home_road
CREATE TABLE IF NOT EXISTS `egovernance_home_road` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) DEFAULT '',
  `gross_survey_map_of_land` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `giver_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `receiver_citizenship_photo` varchar(200) DEFAULT '',
  `onsite_inspection_report` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='घर बाटो प्रमाणित';


-- Dumping structure for table ipalika.egovernance_मालपोत_भूमिकर
CREATE TABLE IF NOT EXISTS `egovernance_मालपोत_भूमिकर` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate_for_first_year` varchar(200) NOT NULL DEFAULT '',
  `land_tax_receipt_for_renewal` varchar(200) DEFAULT '',
  `land_revenue_renewal_book` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='मालपोत वा भूमिकर';


-- Dumping structure for table ipalika.egovernance_advertisement_tax
CREATE TABLE IF NOT EXISTS `egovernance_advertisement_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `organization_certified_document_1` varchar(200) DEFAULT '',
  `organization_certified_document_2` varchar(200) DEFAULT '',
  `local_level_business_tax` varchar(200) NOT NULL DEFAULT '',
  `other_tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Advertisement Tax';


-- Dumping structure for table ipalika.egovernance_property_tax
CREATE TABLE IF NOT EXISTS `egovernance_property_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `building_map_approval_certificate` varchar(200) NOT NULL DEFAULT '',
  `copy_of_map` varchar(200) NOT NULL DEFAULT '',
  `registration_pass_certificate` varchar(200) DEFAULT '',
  `local_level_survey_map` varchar(200) DEFAULT '',
  `onsite_technical_report` varchar(200) DEFAULT '',
  `tax_paid_to_inland_revenue_office` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='एकीकृत सम्पति कर / घर जग्गा कर';


-- Dumping structure for table ipalika.egovernance_reconciliation_paper
CREATE TABLE IF NOT EXISTS `egovernance_reconciliation_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo_person_1` varchar(200) DEFAULT '',
  `citizenship_photo_person_2` varchar(200) DEFAULT '',
  `citizenship_photo_person_3` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo_person_4` varchar(200) NOT NULL DEFAULT '',
  `other_document_1` varchar(200) DEFAULT '',
  `other_document_2` varchar(200) DEFAULT '',
  `other_document_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='मिलापत्र कागज / उजुरी दर्ता';


-- Dumping structure for table ipalika.egovernance_चार_कल्ला
CREATE TABLE IF NOT EXISTS `egovernance_चार_कल्ला` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `land_ownership_certificate` varchar(200) NOT NULL DEFAULT '',
  `land_area_survey_map` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `authorized_heir_copy` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='चार किल्ला प्रमाणित';


-- Dumping structure for table ipalika.egovernance_institution_register
CREATE TABLE IF NOT EXISTS `egovernance_institution_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `copy_of_legislation_or_rules` varchar(200) NOT NULL DEFAULT '',
  `bahal_agreement_photo` varchar(200) DEFAULT '',
  `bahal_tax_information` varchar(200) DEFAULT '',
  `land_ownership_certificate` varchar(200) DEFAULT '',
  `map_pass_certificate` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='संस्था दर्ता सिफारिस';


-- Dumping structure for table ipalika.egovernance_free_healthcare
CREATE TABLE IF NOT EXISTS `egovernance_free_healthcare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `document_proving_poverty` varchar(200) NOT NULL DEFAULT '',
  `other_document_1` varchar(200) DEFAULT '',
  `other_document_2` varchar(200) DEFAULT '',
  `other_document_3` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='निःशुल्क वा सःशुल्क स्वास्थ्य उपचार सिफारिस';


-- Dumping structure for table ipalika.egovernance_land_register
CREATE TABLE IF NOT EXISTS `egovernance_land_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `ward_no` int(11) NOT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `copy_of_previous_data` varchar(200) NOT NULL DEFAULT '',
  `field_book` varchar(200) NOT NULL DEFAULT '',
  `onsite_inspection_report` varchar(200) NOT NULL DEFAULT '',
  `land_survey_map` varchar(200) NOT NULL DEFAULT '',
  `proof_document_1` varchar(200) DEFAULT '',
  `proof_document_2` varchar(200) DEFAULT '',
  `proof_document_3` varchar(200) DEFAULT '',
  `local_sarjamin_muchulka` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='जग्गा दर्ता सिफारिस';


-- Dumping structure for table ipalika.egovernance_school_operation
CREATE TABLE IF NOT EXISTS `egovernance_school_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `school_registration_certificate` varchar(200) NOT NULL DEFAULT '',
  `local_level_renewal_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',   -- In case of non-government schools
  `bahal_agreement_photo` varchar(200) DEFAULT '',
  `bahal_tax_information` varchar(200) DEFAULT '',
  `inspection_report` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='विद्यालय सञ्चालन स्वीकृत / कक्षा वृद्धि सिफारिस';


-- Dumping structure for table ipalika.egovernance_internal_residence
CREATE TABLE IF NOT EXISTS `egovernance_internal_residence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `relocating_person_citizenship_photo_1` varchar(200) DEFAULT '',
  `relocating_person_citizenship_photo_2` varchar(200) DEFAULT '',
  `relocating_person_citizenship_photo_3` varchar(200) DEFAULT '',
  `marriage_registration_certificate` varchar(200) DEFAULT '',
  `birth_registration_certificate_1` varchar(200) DEFAULT '',
  `birth_registration_certificate_2` varchar(200) DEFAULT '',
  `birth_registration_certificate_3` varchar(200) DEFAULT '',
  `land_ownership_certificate` varchar(200) DEFAULT '', -- In case of those who have house or land
  `business_proof_document` varchar(200) DEFAULT '',  -- In case of those who don't have house or land
  `residence_proof_document` varchar(200) DEFAULT '',  -- In case of those who don't have house or land
  `tax_information_photo` varchar(200) DEFAULT '',  -- In case of those who have house or land
  `bahal_agreement_photo` varchar(200) DEFAULT '',  -- In case of those who don't have house or land
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='अन्तरिक बसाई सराई सिफारिस';


-- Dumping structure for table ipalika.egovernance_court_fee
CREATE TABLE IF NOT EXISTS `egovernance_court_fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',   -- In case you have your own house
  `evidence_document_filed_in_court_1` varchar(200) DEFAULT '',
  `evidence_document_filed_in_court_2` varchar(200) DEFAULT '',
  `written_reason_for_waiving_fee` varchar(200) NOT NULL DEFAULT '',
  `local_sarjamin_muchulka` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='कोर्ट फी मिनाहा सिफारिस';


-- Dumping structure for table ipalika.egovernance_business_closure
CREATE TABLE IF NOT EXISTS `egovernance_business_closure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `business_renewal_certificate` varchar(200) NOT NULL DEFAULT '',
  `ghar_bahal_agreement` varchar(200) NOT NULL DEFAULT '',
  `onsite_report` varchar(200) NOT NULL DEFAULT '',
  `identity_document_if_foreigner` varchar(200) DEFAULT '',
  `letter_from_embassy` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='व्यवसाय बन्द सिफारिस';


-- Dumping structure for table ipalika.egovernance_angikrit_citizenship
CREATE TABLE IF NOT EXISTS `egovernance_angikrit_citizenship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `doc_renouncing_citizenship_of_former_country` varchar(200) NOT NULL DEFAULT '',
  `proof_doc_of_living_in_nepal_for_15_years` varchar(200) NOT NULL DEFAULT '',
  `marriage_registration_certificate` varchar(200) DEFAULT '',  -- Copy of Marriage Registration Certificate for Married Adopted Citizenship
  `official_certificate_of_concerned_country` varchar(200) DEFAULT '',
  `proof_document_knowing_to_read_write_in_nepali` varchar(200) NOT NULL DEFAULT '',
  `pp_photo_1` varchar(200) NOT NULL DEFAULT '',
  `pp_photo_2` varchar(200) NOT NULL DEFAULT '',
  `pp_photo_3` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) NOT NULL DEFAULT '',
  `sarjamin_muchulka` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='अंगिकृत नागरिकता सिफारिस';



-- Dumping structure for table ipalika.egovernance_citizenship_and_copy
CREATE TABLE IF NOT EXISTS `egovernance_citizenship_and_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `father_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `mother_citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `birth_certificate` varchar(200) NOT NULL DEFAULT '',
  `husband_citizenship_photo` varchar(200) DEFAULT '',  -- In case of married woman
  `student_character_certificate` varchar(200) DEFAULT '',  -- In case of student
  `marriage_registration_certificate` varchar(200) DEFAULT '',  -- In case the person is married
  `resettlement_certificate` varchar(200) DEFAULT '',  -- In case of resettlement
  `pp_photo_copy_1` varchar(200) NOT NULL DEFAULT '',
  `pp_photo_copy_2` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
  `concerned_office_letter` varchar(200) NOT NULL DEFAULT '',
  `old_citizenship_photo` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='नागरिकता र प्रतिलिपि सिफारिस';


-- Dumping structure for table ipalika.egovernance_resettlement_register
CREATE TABLE IF NOT EXISTS `egovernance_resettlement_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `citizenship_photo` varchar(200) NOT NULL DEFAULT '',
  `family_details_photo` varchar(200) NOT NULL DEFAULT '',
  `concerned_ward_office_letter` varchar(200) NOT NULL DEFAULT '',
  `lalpurja_of_going_place` varchar(200) DEFAULT '',
  `lalpurja_of_coming_place` varchar(200) DEFAULT '',
  `certificate_of_relocation` varchar(200) NOT NULL DEFAULT '',
  `relocating_person_citizenship_photo_1` varchar(200) DEFAULT '',
  `relocating_person_citizenship_photo_2` varchar(200) DEFAULT '',
  `relocating_person_citizenship_photo_3` varchar(200) DEFAULT '',
  `relocating_person_citizenship_photo_4` varchar(200) DEFAULT '',
  `birth_registration_certificate_1` varchar(200) DEFAULT '',
  `birth_registration_certificate_2` varchar(200) DEFAULT '',
  `birth_registration_certificate_3` varchar(200) DEFAULT '',
  `birth_registration_certificate_4` varchar(200) DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Resettlement (incoming/outgoing) Registration)';


-- Dumping structure for table ipalika.egovernance_business_register
CREATE TABLE IF NOT EXISTS `egovernance_business_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `business_registration_certificate` varchar(200) NOT NULL DEFAULT '',
  `tax_information_photo` varchar(200) DEFAULT '',
  `bahal_agreement_photo` varchar(200) DEFAULT '',
  `pp_photo_copy_1` varchar(200) NOT NULL DEFAULT '',
  `pp_photo_copy_2` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='व्यवसाय दर्ता सिफारिस';


-- Dumping structure for table ipalika.egovernance_quadruped
CREATE TABLE IF NOT EXISTS `egovernance_quadruped` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(20) NOT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `applicant_name` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `application_photo` varchar(200) NOT NULL DEFAULT '',
  `approval_letter` varchar(200) NOT NULL DEFAULT '',
  `giver_identification_photo` varchar(200) NOT NULL DEFAULT '',
  `receiver_identification_photo` varchar(200) NOT NULL DEFAULT '',
  `no_effect_of_rearing_on_surrounding_confirmation` varchar(200) NOT NULL DEFAULT '',
    `registrar_name_eng` varchar(80) DEFAULT NULL,
    `registrar_name_nep` varchar(80) DEFAULT NULL,
    `registrar_signature` varchar(100) DEFAULT NULL,
    `registrar_signature_date` varchar(45) DEFAULT NULL,
    `ward_secretary_name` varchar(80) DEFAULT NULL,
    `ward_secretary_signature` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `date_of_registration` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `approved` bit(1) DEFAULT b'0',
  `deleted` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='चौपाय सम्बन्धी सिफारिस';



CREATE TABLE IF NOT EXISTS status_level (
    type_id INT(11) NOT NULL AUTO_INCREMENT,
    type_name VARCHAR(20) NOT NULL DEFAULT '0',
    PRIMARY KEY (type_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='verification status level';
package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class BirthVerifyDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String wardNo;
    private String name;
    private String address;
    private String gender;
    private String applicationPhoto;
    private String citizenshipPhoto;
    private String birthCertificateMinor;
    private String resettlementCertificate;
    private String taxInformationPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfVerification;
    private String approved;
    private String status;
}

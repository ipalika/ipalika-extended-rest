package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class DeceasedCertificateDTO {

    private int id;
    private String tokenId;
    private String wardNo;
    private String name;
    private String deceasedName;
    private String relationship;
    private String applicationPhoto;
    private String citizenshipPhotoApplicant;
    private String ppPhotoApplicant;
    private String citizenshipPhotoDeceased;
    private String ppPhotoDeceased;
    private String relationshipPhoto;
    private String deathCertificate;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

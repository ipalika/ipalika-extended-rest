package com.ishanitech.ipalika.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class used for standardizing response obtained from the web service.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response<T> {
	private T data;
}

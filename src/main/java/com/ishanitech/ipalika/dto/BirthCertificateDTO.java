package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class BirthCertificateDTO {
	private int id;
	private String tokenId;
	private String registrationNo;
	private String lgProvince;
	private String lgDistrict;
	private String lgMunicipality;
	private String lgWard;
	private String tole;
	private String dorAd;
	private String dorBs;
	private String informantNameEng;
	private String informantNameNep;
	private String informantIdEng;
	private String informantIdNep;
	private String nameEng;
	private String nameNep;
	private String dobAd;
	private String dobBs;
	private String fatherNameEng;
	private String fatherNameNep;
	private String fatherIdEng;
	private String fatherIdNep;
	private String motherNameEng;
	private String motherNameNep;
	private String motherIdEng;
	private String motherIdNep;
	private String gender;
	private String permanentAddressEng;
	private String permanentAddressNep;
	private String birthPlaceEng;
	private String birthPlaceNep;
	private String birthPlaceCertificate;
	private String registrarNameEng;
	private String registrarNameNep;
	private String registrarSignature;
	private String registrarSignatureDate;
	private String wardSecretaryName;
	private String wardSecretarySignature;
	private String status;
	private String email;
	private String phone;
	private String grandfatherNameEng;
	private String grandfatherNameNep;
	private String approved;
	private String fatherIdImg;
	private String motherIdImg;
	
	private LgProvinceDTO province;
	private LgDistrictDTO district;
	private LgMunicipalityDTO municipality;
	private LgWardDTO ward;
	
	
}

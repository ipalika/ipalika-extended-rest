package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class BusinessDisclosureDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String citizenshipPhoto;
    private String businessRenewalCertificate;
    private String gharBahalAgreement;
    private String onsiteReport;
    private String identityDocumentIfForeigner;
    private String letterFromEmbassy;
    private String taxInformationPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

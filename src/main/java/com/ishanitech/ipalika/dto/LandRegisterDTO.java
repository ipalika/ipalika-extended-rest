package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class LandRegisterDTO {


    private int id;
    private String tokenId;
    private String registrationNo;
    private String wardNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String citizenshipPhoto;
    private String taxInformationPhoto;
    private String copyOfPreviousData;
    private String fieldBook;
    private String onsiteInspectionReport;
    private String landSurveyMap;
    private String proofDocument1;
    private String proofDocument2;
    private String proofDocument3;
    private String localSarjaminMuchulka;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

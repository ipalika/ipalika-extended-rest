package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class DivorceCertificateDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String dateOfMarriageAd;
    private String dateOfMarriageBs;
    private String husbandName;
    private String permanentAddressHusband;
    private String wardNoHusband;
    private String husbandCitizenshipId;
    private String husbandCitizenshipPhoto;
    private String wifeName;
    private String permanentAddressWife;
    private String wardNoWife;
    private String wifeCitizenshipId;
    private String wifeCitizenshipPhoto;
    private String applicationPhoto;
    private String dissolutionCertificate;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

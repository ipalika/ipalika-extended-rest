package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class PropertyTaxDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String landOwnershipCertificate;
    private String buildingMapApprovalCertificate;
    private String copyOfMap;
    private String registrationPassCertificate;
    private String localLevelSurveyMap;
    private String onsiteTechnicalReport;
    private String taxPaidToInlandRevenueOffice;
    private String citizenshipPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

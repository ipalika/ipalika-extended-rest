package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class InternalResidenceDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String relocatingPersonCitizenshipPhoto1;
    private String relocatingPersonCitizenshipPhoto2;
    private String relocatingPersonCitizenshipPhoto3;
    private String marriageRegistrationCertificate;
    private String birthRegistrationCertificate1;
    private String birthRegistrationCertificate2;
    private String birthRegistrationCertificate3;
    private String landOwnershipCertificate;
    private String businessProofDocument;
    private String residenceProofDocument;
    private String taxInformationPhoto;
    private String bahalAgreementPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

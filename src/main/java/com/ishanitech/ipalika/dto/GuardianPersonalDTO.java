package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class GuardianPersonalDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String guardianGiverCitizenshipPhoto;
    private String guardianGiverBirthCertificate;
    private String guardianReceiverCitizenshipPhoto;
    private String guardianReceiverBirthCertificate;
    private String taxInformationPhoto;
    private String onsiteSarjaminMuchulka;
    private String localSarjaminMuchulka;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

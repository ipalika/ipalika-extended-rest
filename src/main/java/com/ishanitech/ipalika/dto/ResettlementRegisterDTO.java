package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class ResettlementRegisterDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String citizenshipPhoto;
    private String familyDetailsPhoto;
    private String concernedWardOfficeLetter;
    private String lalpurjaOfGoingPlace;
    private String lalpurjaOfComingPlace;
    private String certificateOfRelocation;
    private String relocatingPersonCitizenshipPhoto1;
    private String relocatingPersonCitizenshipPhoto2;
    private String relocatingPersonCitizenshipPhoto3;
    private String relocatingPersonCitizenshipPhoto4;
    private String birthRegistrationCertificate1;
    private String birthRegistrationCertificate2;
    private String birthRegistrationCertificate3;
    private String birthRegistrationCertificate4;
    private String taxInformationPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

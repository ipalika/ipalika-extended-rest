package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class HouseLandTransferDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String citizenshipPhoto;
    private String relationshipCertificateOfDeceasedPetitioner;
    private String landOwnershipCertificate;
    private String sarjaminWitnessCitizenshipPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class CitizenshipAndCopyDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String fatherCitizenshipPhoto;
    private String motherCitizenshipPhoto;
    private String birthCertificate;
    private String husbandCitizenshipPhoto;
    private String studentCharacterCertificate;
    private String marriageRegistrationCertificate;
    private String resettlementCertificate;
    private String ppPhotoCopy1;
    private String ppPhotoCopy2;
    private String taxInformationPhoto;
    private String concernedOfficeLetter;
    private String oldCitizenshipPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

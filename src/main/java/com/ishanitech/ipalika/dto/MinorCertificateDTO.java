package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class MinorCertificateDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String wardNo;
    private String name;
    private String address;
    private String gender;
    private String applicationPhoto;
    private String birthCertificate;
    private String fatherCitizenshipPhoto;
    private String motherCitizenshipPhoto;
    private String ppPhoto;
    private String taxInformationPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

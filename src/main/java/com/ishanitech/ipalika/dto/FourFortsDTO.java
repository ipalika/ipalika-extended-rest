package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class FourFortsDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String wardNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String citizenshipPhoto;
    private String landOwnershipCertificate;
    private String landAreaSurveyMap;
    private String taxInformationPhoto;
    private String authorizedHeirCopy;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

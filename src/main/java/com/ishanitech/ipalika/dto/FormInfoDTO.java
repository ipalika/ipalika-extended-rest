package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class FormInfoDTO {
    private int id;
    private String form_type;
}

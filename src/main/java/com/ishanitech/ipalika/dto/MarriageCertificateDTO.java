package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class MarriageCertificateDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String marriageApplicationPhoto;
    private String husbandName;
    private String husbandCitizenshipPhoto;
    private String husbandCitizenshipId;
    private String wifeName;
    private String wifeCitizenshipPhoto;
    private String wifeCitizenshipId;
    private String brideFamilyCitizenshipPhoto;
    private String brideFamilyCitizenshipId;
    private String taxInformationPhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}


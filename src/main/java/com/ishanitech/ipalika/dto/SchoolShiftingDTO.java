package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class SchoolShiftingDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String schoolRegistrationCertificate;
    private String localLevelRenewalCertificate;
    private String taxInformationCurrentPlace;
    private String taxInformationShiftingPlace;
    private String bahalAgreementPhoto;
    private String bahalTaxInformation;
    private String permanentAccountNoCertificate;
    private String inspectionReport;
    private String wardOfficePermitLetter;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

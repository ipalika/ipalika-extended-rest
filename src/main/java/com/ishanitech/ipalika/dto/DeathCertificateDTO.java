package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class DeathCertificateDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String wardNo;
    private String name;
    private String gender;
    private String dobAd;
    private String dobBs;
    private String dateOfDemiseAd;
    private String dateOfDemiseBs;
    private String demiseCause;
    private String applicationPhoto;
    private String citizenshipPhoto;
    private String informantDocument;
    private String relationshipCertificatePhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;

}

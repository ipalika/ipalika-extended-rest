package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class KothaKholneDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String wardNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String taxInformationPhoto;
    private String bahalAgreementPhoto;
    private String districtAdminOfficeLetter;
    private String localSarjaminMuchulka;
    private String policeSarjaminMuchulka;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

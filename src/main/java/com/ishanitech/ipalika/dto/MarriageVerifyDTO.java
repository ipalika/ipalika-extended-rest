package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class MarriageVerifyDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String wardNo;
    private String husbandName;
    private String wifeName;
    private String husbandCitizenshipPhoto;
    private String husbandCitizenshipId;
    private String wifeCitizenshipPhoto;
    private String wifeCitizenshipId;
    private String migrationCertificatePhoto;
    private String taxInformationPhoto;
    private String marriageCertificatePhoto;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfVerification;
    private String approved;
    private String status;
}

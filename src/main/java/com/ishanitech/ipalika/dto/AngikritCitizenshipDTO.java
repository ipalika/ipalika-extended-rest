package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class AngikritCitizenshipDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String docRenouncingCitizenshipOfFormerCountry;
    private String proofDocOfLivingInNepalFor15Years;
    private String marriageRegistrationCertificate;
    private String officialCertificateOfConcernedCountry;
    private String proofDocumentKnowingToReadWriteInNepali;
    private String ppPhoto1;
    private String ppPhoto2;
    private String ppPhoto3;
    private String taxInformationPhoto;
    private String sarjaminMuchulka;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

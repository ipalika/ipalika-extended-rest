package com.ishanitech.ipalika.dto;

import lombok.Data;

@Data
public class ReconciliationPaperDTO {

    private int id;
    private String tokenId;
    private String registrationNo;
    private String applicantName;
    private String address;
    private String applicationPhoto;
    private String citizenshipPhotoPerson1;
    private String citizenshipPhotoPerson2;
    private String citizenshipPhotoPerson3;
    private String citizenshipPhotoPerson4;
    private String otherDocument1;
    private String otherDocument2;
    private String otherDocument3;
    private String registrarNameEng;
    private String registrarNameNep;
    private String registrarSignature;
    private String registrarSignatureDate;
    private String wardSecretaryName;
    private String wardSecretarySignature;
    private String email;
    private String phone;
    private String dateOfRegistration;
    private String approved;
    private String status;
}

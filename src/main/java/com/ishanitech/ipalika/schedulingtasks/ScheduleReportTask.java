package com.ishanitech.ipalika.schedulingtasks;

import java.util.List;

import com.ishanitech.ipalika.service.RegistrationReportService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ishanitech.ipalika.service.ReportService;
import com.ishanitech.ipalika.servicer.WardService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ScheduleReportTask {
    private final ReportService reportService;
    private final RegistrationReportService registrationReportService;
    private final WardService wardService;

    public ScheduleReportTask(ReportService reportService, WardService wardService, RegistrationReportService registrationReportService) {
        this.reportService = reportService;
        this.wardService = wardService;
        this.registrationReportService = registrationReportService;
    }

    //Run Report Generation Service Everyday at midnight
    @Scheduled(cron = "0 0 0 * * ?")
    //@Scheduled(fixedRate = 60000)
    public void generateReport() {
        log.info("Generate Report Called");
        reportService.generateReport(0);
    }


    @Scheduled(cron = "0 0 0 * * ?")
    //@Scheduled(fixedRate = 60000)
    public void generateRegistrationCount() {
        log.info("Generate Report Called");
        registrationReportService.populateRegistrationCount();
    }

    @Scheduled(cron = "0 0 0 * * ?")
    //@Scheduled(fixedRate = 60000)
    public void generateReportWard() {
        List<Integer> wardNos = wardService.getAllWardNumbers();
        wardNos.stream().forEach((wardNo) -> reportService.generateReport(wardNo));
    }
}

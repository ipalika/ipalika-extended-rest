package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.MohiLagatDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.MohiLagatService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class MohiLagatController {

    private final MohiLagatService mohiLagatService;

    private MohiLagatController(MohiLagatService mohiLagatService){this.mohiLagatService = mohiLagatService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/mohi-lagat")
    public ResponseDTO<List<MohiLagatDTO>> getMohiLagatRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<MohiLagatDTO>>(mohiLagatService.getMohiLagatRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/mohi-lagat/{tokenId}")
    public ResponseDTO<MohiLagatDTO> getMohiLagatCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<MohiLagatDTO>(mohiLagatService.getMohiLagatCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/mohi-lagat/{tokenId}")
    public void updateMohiLagatCertificateByTokenId(@RequestBody MohiLagatDTO mohiRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        mohiLagatService.updateMohiLagatCertificateByTokenId(mohiRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/mohi-lagat")
    @ApiOperation("Add Mohi Lagat Katta Registration")
    public void addMohiLagatRegistration(HttpServletResponse http, @RequestBody MohiLagatDTO mohiRegistrationInfo)
            throws CustomSqlException {
        mohiLagatService.addMohiLagatRegistration(mohiRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/mohi-lagat/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        mohiLagatService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/mohi-lagat/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        mohiLagatService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.BusinessRegisterDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.BusinessRegisterService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class BusinessRegisterController {

    private final BusinessRegisterService businessRegisterService;

    private BusinessRegisterController(BusinessRegisterService businessRegisterService){this.businessRegisterService = businessRegisterService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/business-register")
    public ResponseDTO<List<BusinessRegisterDTO>> getBusinessRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<BusinessRegisterDTO>>(businessRegisterService.getBusinessRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/business-register/{tokenId}")
    public ResponseDTO<BusinessRegisterDTO> getBusinessCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<BusinessRegisterDTO>(businessRegisterService.getBusinessCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/business-register/{tokenId}")
    public void updateBusinessCertificateByTokenId(@RequestBody BusinessRegisterDTO businessRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        businessRegisterService.updateBusinessCertificateByTokenId(businessRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/business-register")
    @ApiOperation("Add Business Registration")
    public void addBusinessRegistration(HttpServletResponse http, @RequestBody BusinessRegisterDTO businessRegistrationInfo)
            throws CustomSqlException {
        businessRegisterService.addBusinessRegistration(businessRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/business-register/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        businessRegisterService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/business-register/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        businessRegisterService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

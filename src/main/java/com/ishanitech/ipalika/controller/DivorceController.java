package com.ishanitech.ipalika.controller;


import com.ishanitech.ipalika.dto.DivorceCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.DivorceService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class DivorceController {


    private final DivorceService divorceService;

    private DivorceController(DivorceService divorceService){
        this.divorceService = divorceService;
    }



    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/divorce")
    public ResponseDTO<List<DivorceCertificateDTO>> getDivorceRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<DivorceCertificateDTO>>(divorceService.getDivorceRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/divorce/{tokenId}")
    public ResponseDTO<DivorceCertificateDTO> getDivorceCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<DivorceCertificateDTO>(divorceService.getDivorceCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/divorce/{tokenId}")
    public void updateDivorceCertificateByTokenId(@RequestBody DivorceCertificateDTO divorceRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId" + tokenId);
        divorceService.updateDivorceCertificateByTokenId(divorceRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/divorce")
    @ApiOperation("Add Divorce Registration")
    public void addDivorceRegistration(HttpServletResponse http, @RequestBody DivorceCertificateDTO divorceRegistrationInfo)
            throws CustomSqlException {
        divorceService.addDivorceRegistration(divorceRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/divorce/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        divorceService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/divorce/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        divorceService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

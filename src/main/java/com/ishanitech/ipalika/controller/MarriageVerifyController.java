package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.MarriageVerifyDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.MarriageVerifyService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class MarriageVerifyController {

    private final MarriageVerifyService marriageVerifyService;

    private MarriageVerifyController(MarriageVerifyService marriageVerifyService){
        this.marriageVerifyService = marriageVerifyService;
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/marriage-verify")
    public ResponseDTO<List<MarriageVerifyDTO>> getMarriageVerifications(HttpServletRequest request) {
        return new ResponseDTO<List<MarriageVerifyDTO>>(marriageVerifyService.getMarriageVerifications(request));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/marriage-verify")
    @ApiOperation("Add Marriage Verification")
    public void addMarriageVerification(HttpServletResponse http, @RequestBody MarriageVerifyDTO marriageVerificationInfo)
            throws CustomSqlException {
        marriageVerifyService.addMarriageVerification(marriageVerificationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/marriage-verify/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image)
            throws FileStorageException {
        marriageVerifyService.addAllImages(image);
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/marriage-verify/{tokenId}")
    public ResponseDTO<MarriageVerifyDTO> getMarriageVerificationByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<MarriageVerifyDTO>(marriageVerifyService.getMarriageVerificationByTokenId(tokenId));

    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/marriage-verify/{tokenId}")
    public void updateMarriageVerificationByTokenId(@RequestBody MarriageVerifyDTO marriageVerificationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        marriageVerifyService.updateMarriageVerificationByTokenId(marriageVerificationInfo, tokenId);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/marriage-verify/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        marriageVerifyService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

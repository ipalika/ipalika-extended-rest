package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.BirthVerifyDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.BirthVerifyService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class BirthVerifyController {

    private final BirthVerifyService birthVerifyService;

    private BirthVerifyController(BirthVerifyService birthVerifyService){this.birthVerifyService = birthVerifyService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/birth-verify")
    public ResponseDTO<List<BirthVerifyDTO>> getBirthVerifications(HttpServletRequest request) {
        return new ResponseDTO<List<BirthVerifyDTO>>(birthVerifyService.getBirthVerifications(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/birth-verify/{tokenId}")
    public ResponseDTO<BirthVerifyDTO> getBirthVerificationByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<BirthVerifyDTO>(birthVerifyService.getBirthVerificationByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/birth-verify/{tokenId}")
    public void updateBirthVerificationByTokenId(@RequestBody BirthVerifyDTO birthVerificationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        birthVerifyService.updateBirthVerificationByTokenId(birthVerificationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/birth-verify")
    @ApiOperation("Add Birth Verification")
    public void addBirthVerification(HttpServletResponse http, @RequestBody BirthVerifyDTO birthVerificationInfo)
            throws CustomSqlException {
        birthVerifyService.addBirthVerification(birthVerificationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/birth-verify/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        birthVerifyService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/birth-verify/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        birthVerifyService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

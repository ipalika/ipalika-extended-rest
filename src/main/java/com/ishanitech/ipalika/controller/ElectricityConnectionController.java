package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dao.FormStatusUpdationInfo;
import com.ishanitech.ipalika.dto.ElectricityConnectionDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.security.CustomUserDetails;
import com.ishanitech.ipalika.service.ElectricityConnectionService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class ElectricityConnectionController {

    private final ElectricityConnectionService electricityConnectionService;

    private ElectricityConnectionController(ElectricityConnectionService electricityConnectionService){this.electricityConnectionService = electricityConnectionService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/electricity")
    public ResponseDTO<List<ElectricityConnectionDTO>> getElectricityRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<ElectricityConnectionDTO>>(electricityConnectionService.getElectricityRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/electricity/{tokenId}")
    public ResponseDTO<ElectricityConnectionDTO> getElectricityCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<ElectricityConnectionDTO>(electricityConnectionService.getElectricityCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/electricity/{tokenId}")
    public void updateElectricityCertificateByTokenId(@RequestBody ElectricityConnectionDTO electricityRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        electricityConnectionService.updateElectricityCertificateByTokenId(electricityRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/electricity")
    @ApiOperation("Add Electricity Connection Registration")
    public void addElectricityRegistration(HttpServletResponse http, @RequestBody ElectricityConnectionDTO electricityRegistrationInfo)
            throws CustomSqlException {
        electricityConnectionService.addElectricityRegistration(electricityRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/electricity/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        electricityConnectionService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/electricity/status/{tokenId}/{status}/{formInfoId}/{regNo}/{pdfLocation}")
    public void updateCertificateStatusByTokenId(
                                                 @PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId,
                                                 @PathVariable("regNo") String regNo,
                                                 @PathVariable("pdfLocation") String pdfLocation,
                                                 @AuthenticationPrincipal CustomUserDetails user
                                                 )
            throws EntityNotFoundException {
        System.out.println("authenticationprincipal============================= = " + user);
        electricityConnectionService.updateCertificateStatus(tokenId, status, formInfoId, regNo, pdfLocation, user);
    }
}

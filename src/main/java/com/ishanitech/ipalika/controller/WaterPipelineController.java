package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.WaterPipelineDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.WaterPipelineService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class WaterPipelineController {

    private final WaterPipelineService waterPipelineService;

    private WaterPipelineController(WaterPipelineService waterPipelineService){this.waterPipelineService = waterPipelineService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/water-pipeline")
    public ResponseDTO<List<WaterPipelineDTO>> getPipelineRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<WaterPipelineDTO>>(waterPipelineService.getPipelineRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/water-pipeline/{tokenId}")
    public ResponseDTO<WaterPipelineDTO> getPipelineCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<WaterPipelineDTO>(waterPipelineService.getPipelineCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/water-pipeline/{tokenId}")
    public void updatePipelineCertificateByTokenId(@RequestBody WaterPipelineDTO pipelineRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        waterPipelineService.updatePipelineCertificateByTokenId(pipelineRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/water-pipeline")
    @ApiOperation("Add Water Pipeline Registration")
    public void addPipelineRegistration(HttpServletResponse http, @RequestBody WaterPipelineDTO pipelineRegistrationInfo)
            throws CustomSqlException {
        waterPipelineService.addPipelineRegistration(pipelineRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/water-pipeline/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        waterPipelineService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/water-pipeline/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        waterPipelineService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

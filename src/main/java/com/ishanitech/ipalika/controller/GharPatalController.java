package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.GharPatalDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.GharPatalService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class GharPatalController {

    private final GharPatalService gharPatalService;

    private GharPatalController(GharPatalService gharPatalService){this.gharPatalService = gharPatalService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/ghar-patal")
    public ResponseDTO<List<GharPatalDTO>> getGharPatalVerifications(HttpServletRequest request) {
        return new ResponseDTO<List<GharPatalDTO>>(gharPatalService.getGharPatalVerifications(request));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/ghar-patal")
    @ApiOperation("Add Ghar Patal Verification")
    public void addGharPatalVerification(HttpServletResponse http, @RequestBody GharPatalDTO gharVerificationInfo)
            throws CustomSqlException {
        gharPatalService.addGharPatalVerification(gharVerificationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/ghar-patal/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image)
            throws FileStorageException {
        gharPatalService.addAllImages(image);
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/ghar-patal/{tokenId}")
    public ResponseDTO<GharPatalDTO> getGharPatalVerificationByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<GharPatalDTO>(gharPatalService.getGharPatalVerificationByTokenId(tokenId));

    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/ghar-patal/{tokenId}")
    public void updateGharPatalVerificationByTokenId(@RequestBody GharPatalDTO gharVerificationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        gharPatalService.updateGharPatalVerificationByTokenId(gharVerificationInfo, tokenId);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/ghar-patal/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        gharPatalService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

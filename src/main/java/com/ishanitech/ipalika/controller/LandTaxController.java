package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.LandTaxDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.LandTaxService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class LandTaxController {

    private final LandTaxService landTaxService;

    private LandTaxController(LandTaxService landTaxService){this.landTaxService = landTaxService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-tax")
    public ResponseDTO<List<LandTaxDTO>> getLandRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<LandTaxDTO>>(landTaxService.getLandRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-tax/{tokenId}")
    public ResponseDTO<LandTaxDTO> getLandCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<LandTaxDTO>(landTaxService.getLandCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-tax/{tokenId}")
    public void updateLandCertificateByTokenId(@RequestBody LandTaxDTO landRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        landTaxService.updateLandCertificateByTokenId(landRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-tax")
    @ApiOperation("Add Land Tax Registration")
    public void addLandRegistration(HttpServletResponse http, @RequestBody LandTaxDTO landRegistrationInfo)
            throws CustomSqlException {
        landTaxService.addLandRegistration(landRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-tax/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        landTaxService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-tax/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        landTaxService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

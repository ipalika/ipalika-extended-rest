package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.RightfulPersonDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.RightfulPersonService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class RightfulPersonController {

    private final RightfulPersonService rightfulPersonService;

    private RightfulPersonController(RightfulPersonService rightfulPersonService){this.rightfulPersonService = rightfulPersonService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/rightful")
    public ResponseDTO<List<RightfulPersonDTO>> getRightfulRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<RightfulPersonDTO>>(rightfulPersonService.getRightfulRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/rightful/{tokenId}")
    public ResponseDTO<RightfulPersonDTO> getRightfulCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<RightfulPersonDTO>(rightfulPersonService.getRightfulCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/rightful/{tokenId}")
    public void updateRightfulCertificateByTokenId(@RequestBody RightfulPersonDTO rightfulRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        rightfulPersonService.updateRightfulCertificateByTokenId(rightfulRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/rightful")
    @ApiOperation("Add Rightful Person Registration")
    public void addRightfulRegistration(HttpServletResponse http, @RequestBody RightfulPersonDTO rightfulRegistrationInfo)
            throws CustomSqlException {
        rightfulPersonService.addRightfulRegistration(rightfulRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/rightful/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        rightfulPersonService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/rightful/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        rightfulPersonService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.HouseLandTransferDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.HouseLandTransferService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class HouseLandTransferController {

    private final HouseLandTransferService houseLandTransferService;

    private HouseLandTransferController(HouseLandTransferService houseLandTransferService){this.houseLandTransferService = houseLandTransferService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/hlTransfer")
    public ResponseDTO<List<HouseLandTransferDTO>> getHouseLandRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<HouseLandTransferDTO>>(houseLandTransferService.getHouseLandRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/hlTransfer/{tokenId}")
    public ResponseDTO<HouseLandTransferDTO> getHouseLandCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<HouseLandTransferDTO>(houseLandTransferService.getHouseLandCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/hlTransfer/{tokenId}")
    public void updateHouseLandCertificateByTokenId(@RequestBody HouseLandTransferDTO houseRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        houseLandTransferService.updateHouseLandCertificateByTokenId(houseRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/hlTransfer")
    @ApiOperation("Add House Land Transfer Registration")
    public void addHouseLandRegistration(HttpServletResponse http, @RequestBody HouseLandTransferDTO houseRegistrationInfo)
            throws CustomSqlException {
        houseLandTransferService.addHouseLandRegistration(houseRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/hlTransfer/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        houseLandTransferService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/hlTransfer/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        houseLandTransferService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.PermanentResidenceDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.PermanentResidenceService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class PermanentResidenceController {

    private final PermanentResidenceService permanentResidenceService;

    private PermanentResidenceController(PermanentResidenceService permanentResidenceService){this.permanentResidenceService = permanentResidenceService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/permanent-residence")
    public ResponseDTO<List<PermanentResidenceDTO>> getPermanentResidenceRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<PermanentResidenceDTO>>(permanentResidenceService.getPermanentResidenceRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/permanent-residence/{tokenId}")
    public ResponseDTO<PermanentResidenceDTO> getPermanentResidenceCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<PermanentResidenceDTO>(permanentResidenceService.getPermanentResidenceCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/permanent-residence/{tokenId}")
    public void updatePermanentResidenceCertificateByTokenId(@RequestBody PermanentResidenceDTO permanentResidenceInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        permanentResidenceService.updatePermanentResidenceCertificateByTokenId(permanentResidenceInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/permanent-residence")
    @ApiOperation("Add Permanent Residence Registration")
    public void addPermanentResidenceRegistration(HttpServletResponse http, @RequestBody PermanentResidenceDTO permanentResidenceInfo)
            throws CustomSqlException {
        permanentResidenceService.addPermanentResidenceRegistration(permanentResidenceInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/permanent-residence/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        permanentResidenceService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/permanent-residence/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        permanentResidenceService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

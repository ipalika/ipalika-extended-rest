package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.GharKayamDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.GharKayamService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class GharKayamController {

    private final GharKayamService gharKayamService;

    private GharKayamController(GharKayamService gharKayamService){this.gharKayamService = gharKayamService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/ghar-kayam")
    public ResponseDTO<List<GharKayamDTO>> getGharKayamRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<GharKayamDTO>>(gharKayamService.getGharKayamRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/ghar-kayam/{tokenId}")
    public ResponseDTO<GharKayamDTO> getGharKayamCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<GharKayamDTO>(gharKayamService.getGharKayamCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/ghar-kayam/{tokenId}")
    public void updateGharKayamCertificateByTokenId(@RequestBody GharKayamDTO gharRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        gharKayamService.updateGharKayamCertificateByTokenId(gharRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/ghar-kayam")
    @ApiOperation("Add Ghar Kayam Registration")
    public void addGharKayamRegistration(HttpServletResponse http, @RequestBody GharKayamDTO gharRegistrationInfo)
            throws CustomSqlException {
        gharKayamService.addGharKayamRegistration(gharRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/ghar-kayam/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        gharKayamService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/ghar-kayam/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        gharKayamService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.HomeRoadDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.HomeRoadService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class HomeRoadController {

    private final HomeRoadService homeRoadService;

    private HomeRoadController(HomeRoadService homeRoadService){this.homeRoadService = homeRoadService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/home-road")
    public ResponseDTO<List<HomeRoadDTO>> getHomeRoadRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<HomeRoadDTO>>(homeRoadService.getHomeRoadRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/home-road/{tokenId}")
    public ResponseDTO<HomeRoadDTO> getHomeRoadCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<HomeRoadDTO>(homeRoadService.getHomeRoadCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/home-road/{tokenId}")
    public void updateHomeRoadCertificateByTokenId(@RequestBody HomeRoadDTO homeRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        homeRoadService.updateHomeRoadCertificateByTokenId(homeRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/home-road")
    @ApiOperation("Add Home Road Registration")
    public void addHomeRoadRegistration(HttpServletResponse http, @RequestBody HomeRoadDTO homeRegistrationInfo)
            throws CustomSqlException {
        homeRoadService.addHomeRoadRegistration(homeRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/home-road/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        homeRoadService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/home-road/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        homeRoadService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

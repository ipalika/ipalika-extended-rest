package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.HouseBuildingDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.HouseBuildingService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class HouseBuildingController {

    private final HouseBuildingService houseBuildingService;

    private HouseBuildingController(HouseBuildingService houseBuildingService){this.houseBuildingService = houseBuildingService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/house-building")
    public ResponseDTO<List<HouseBuildingDTO>> getHouseBuildingRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<HouseBuildingDTO>>(houseBuildingService.getHouseBuildingRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/house-building/{tokenId}")
    public ResponseDTO<HouseBuildingDTO> getHouseBuildingCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<HouseBuildingDTO>(houseBuildingService.getHouseBuildingCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/house-building/{tokenId}")
    public void updateHouseBuildingCertificateByTokenId(@RequestBody HouseBuildingDTO houseRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        houseBuildingService.updateHouseBuildingCertificateByTokenId(houseRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/house-building")
    @ApiOperation("Add House Building Registration")
    public void addHouseBuildingRegistration(HttpServletResponse http, @RequestBody HouseBuildingDTO houseRegistrationInfo)
            throws CustomSqlException {
        houseBuildingService.addHouseBuildingRegistration(houseRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/house-building/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        houseBuildingService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/house-building/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        houseBuildingService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

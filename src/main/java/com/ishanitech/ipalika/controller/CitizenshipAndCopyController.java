package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.CitizenshipAndCopyDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.CitizenshipAndCopyService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class CitizenshipAndCopyController {

    private final CitizenshipAndCopyService citizenshipAndCopyService;

    private CitizenshipAndCopyController(CitizenshipAndCopyService citizenshipAndCopyService){this.citizenshipAndCopyService = citizenshipAndCopyService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/citizenship-copy")
    public ResponseDTO<List<CitizenshipAndCopyDTO>> getCitizenshipRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<CitizenshipAndCopyDTO>>(citizenshipAndCopyService.getCitizenshipRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/citizenship-copy/{tokenId}")
    public ResponseDTO<CitizenshipAndCopyDTO> getCitizenshipCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<CitizenshipAndCopyDTO>(citizenshipAndCopyService.getCitizenshipCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/citizenship-copy/{tokenId}")
    public void updateCitizenshipCertificateByTokenId(@RequestBody CitizenshipAndCopyDTO citizenshipRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        citizenshipAndCopyService.updateCitizenshipCertificateByTokenId(citizenshipRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/citizenship-copy")
    @ApiOperation("Add Citizenship And Copy Registration")
    public void addCitizenshipRegistration(HttpServletResponse http, @RequestBody CitizenshipAndCopyDTO citizenshipRegistrationInfo)
            throws CustomSqlException {
        citizenshipAndCopyService.addCitizenshipRegistration(citizenshipRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/citizenship-copy/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        citizenshipAndCopyService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/citizenship-copy/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        citizenshipAndCopyService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

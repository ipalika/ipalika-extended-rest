package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.IndustryShiftingDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.IndustryShiftingService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class IndustryShiftingController {

    private final IndustryShiftingService industryShiftingService;

    private IndustryShiftingController(IndustryShiftingService industryShiftingService){this.industryShiftingService = industryShiftingService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/industry-shifting")
    public ResponseDTO<List<IndustryShiftingDTO>> getIndustryRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<IndustryShiftingDTO>>(industryShiftingService.getIndustryRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/industry-shifting/{tokenId}")
    public ResponseDTO<IndustryShiftingDTO> getIndustryCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<IndustryShiftingDTO>(industryShiftingService.getIndustryCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/industry-shifting/{tokenId}")
    public void updateIndustryCertificateByTokenId(@RequestBody IndustryShiftingDTO industryRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        industryShiftingService.updateIndustryCertificateByTokenId(industryRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/industry-shifting")
    @ApiOperation("Add Industry Shifting Registration")
    public void addIndustryRegistration(HttpServletResponse http, @RequestBody IndustryShiftingDTO industryRegistrationInfo)
            throws CustomSqlException {
        industryShiftingService.addIndustryRegistration(industryRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/industry-shifting/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        industryShiftingService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/industry-shifting/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        industryShiftingService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.LandMarkingDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.LandMarkingService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class LandMarkingController {

    private final LandMarkingService landMarkingService;

    private LandMarkingController(LandMarkingService landMarkingService){this.landMarkingService = landMarkingService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/landmark")
    public ResponseDTO<List<LandMarkingDTO>> getLandMarkingRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<LandMarkingDTO>>(landMarkingService.getLandMarkingRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/landmark/{tokenId}")
    public ResponseDTO<LandMarkingDTO> getLandMarkingCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<LandMarkingDTO>(landMarkingService.getLandMarkingCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/landmark/{tokenId}")
    public void updateLandMarkingCertificateByTokenId(@RequestBody LandMarkingDTO landmarkRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        landMarkingService.updateLandMarkingCertificateByTokenId(landmarkRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/landmark")
    @ApiOperation("Add Land Marking Registration")
    public void addLandMarkingRegistration(HttpServletResponse http, @RequestBody LandMarkingDTO landmarkRegistrationInfo)
            throws CustomSqlException {
        landMarkingService.addLandMarkingRegistration(landmarkRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/landmark/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        landMarkingService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/landmark/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        landMarkingService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

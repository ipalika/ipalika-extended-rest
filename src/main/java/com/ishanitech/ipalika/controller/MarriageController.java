package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.MarriageCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.MarriageService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/e-governance")
public class MarriageController {

    private final MarriageService marriageService;

    private MarriageController(MarriageService marriageService) {
        this.marriageService = marriageService;
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/marriage")
    public ResponseDTO<List<MarriageCertificateDTO>> getMarriageRegistration(HttpServletRequest request) {
        return new ResponseDTO<List<MarriageCertificateDTO>>(marriageService.getMarriageRegistration(request));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/marriage")
    @ApiOperation("Add Marriage Registration")
    public void addMarriageRegistration(HttpServletResponse http, @RequestBody MarriageCertificateDTO marriageRegistrationInfo)
            throws CustomSqlException {
        log.info(marriageRegistrationInfo.getHusbandName());
        marriageService.addMarriageRegistration(marriageRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/marriage/image")
    public void addAllImages(@RequestParam("pictures") MultipartFile image)
            throws FileStorageException {
        marriageService.addAllImages(image);
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/marriage/{tokenId}")
    public ResponseDTO<MarriageCertificateDTO> getMarriageCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<MarriageCertificateDTO>(marriageService.getMarriageCertificateByTokenId(tokenId));

    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/marriage/{tokenId}")
    public void updateMarriageCertificateByTokenId(@RequestBody MarriageCertificateDTO marriageRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
         log.info("tokenId" + tokenId);
         marriageService.updateMarriageCertificateByTokenId(marriageRegistrationInfo, tokenId);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/marriage/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        marriageService.updateCertificateStatus(tokenId, status, formInfoId);
    }


}

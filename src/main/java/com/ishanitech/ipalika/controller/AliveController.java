package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.AliveCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.AliveService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class AliveController {

    private final AliveService aliveService;

    private AliveController(AliveService aliveService){this.aliveService = aliveService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/alive")
    public ResponseDTO<List<AliveCertificateDTO>> getAliveRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<AliveCertificateDTO>>(aliveService.getAliveRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/alive/{tokenId}")
    public ResponseDTO<AliveCertificateDTO> getAliveCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<AliveCertificateDTO>(aliveService.getAliveCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/alive/{tokenId}")
    public void updateAliveCertificateByTokenId(@RequestBody AliveCertificateDTO aliveRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        aliveService.updateAliveCertificateByTokenId(aliveRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/alive")
    @ApiOperation("Add Alive Registration")
    public void addAliveRegistration(HttpServletResponse http, @RequestBody AliveCertificateDTO aliveRegistrationInfo)
            throws CustomSqlException {
        aliveService.addAliveRegistration(aliveRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/alive/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        aliveService.addAllImages(image);
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/alive/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        aliveService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

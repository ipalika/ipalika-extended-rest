package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ReconciliationPaperDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.ReconciliationPaperService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class ReconciliationPaperController {

    private final ReconciliationPaperService reconciliationPaperService;

    private ReconciliationPaperController(ReconciliationPaperService reconciliationPaperService){this.reconciliationPaperService = reconciliationPaperService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/reconciliation")
    public ResponseDTO<List<ReconciliationPaperDTO>> getReconciliationRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<ReconciliationPaperDTO>>(reconciliationPaperService.getReconciliationRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/reconciliation/{tokenId}")
    public ResponseDTO<ReconciliationPaperDTO> getReconciliationCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<ReconciliationPaperDTO>(reconciliationPaperService.getReconciliationCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/reconciliation/{tokenId}")
    public void updateReconciliationCertificateByTokenId(@RequestBody ReconciliationPaperDTO reconciliationRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        reconciliationPaperService.updateReconciliationCertificateByTokenId(reconciliationRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/reconciliation")
    @ApiOperation("Add Reconciliation Registration")
    public void addReconciliationRegistration(HttpServletResponse http, @RequestBody ReconciliationPaperDTO reconciliationRegistrationInfo)
            throws CustomSqlException {
        reconciliationPaperService.addReconciliationRegistration(reconciliationRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/reconciliation/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        reconciliationPaperService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/reconciliation/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        reconciliationPaperService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.BusinessDisclosureDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.BusinessDisclosureService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class BusinessDisclosureController {

    private final BusinessDisclosureService businessDisclosureService;

    private BusinessDisclosureController(BusinessDisclosureService businessDisclosureService){this.businessDisclosureService = businessDisclosureService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/business-disclosure")
    public ResponseDTO<List<BusinessDisclosureDTO>> getBusinessRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<BusinessDisclosureDTO>>(businessDisclosureService.getBusinessRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/business-disclosure/{tokenId}")
    public ResponseDTO<BusinessDisclosureDTO> getBusinessCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<BusinessDisclosureDTO>(businessDisclosureService.getBusinessCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/business-disclosure/{tokenId}")
    public void updateBusinessCertificateByTokenId(@RequestBody BusinessDisclosureDTO businessRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        businessDisclosureService.updateBusinessCertificateByTokenId(businessRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/business-disclosure")
    @ApiOperation("Add Business Disclosure Registration")
    public void addBusinessRegistration(HttpServletResponse http, @RequestBody BusinessDisclosureDTO businessRegistrationInfo)
            throws CustomSqlException {
        businessDisclosureService.addBusinessRegistration(businessRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/business-disclosure/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        businessDisclosureService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/business-disclosure/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        businessDisclosureService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

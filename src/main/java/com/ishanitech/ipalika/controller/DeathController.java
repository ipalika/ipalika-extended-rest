package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.DeathCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.DeathService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class DeathController {

    private final DeathService deathService;

    private DeathController(DeathService deathService) {
        this.deathService = deathService;
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/death")
    public ResponseDTO<List<DeathCertificateDTO>> getDeathRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<DeathCertificateDTO>>(deathService.getDeathRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/death/{tokenId}")
    public ResponseDTO<DeathCertificateDTO> getDeathCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<DeathCertificateDTO>(deathService.getDeathCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/death/{tokenId}")
    public void updateDeathCertificateByTokenId(@RequestBody DeathCertificateDTO deathRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId" + tokenId);
        deathService.updateDeathCertificateByTokenId(deathRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/death")
    @ApiOperation("Add Death Registration")
    public void addDeathRegistration(HttpServletResponse http, @RequestBody DeathCertificateDTO deathRegistrationInfo)
            throws CustomSqlException {
        deathService.addDeathRegistration(deathRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/death/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        deathService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/death/status/{tokenId}/{status}/{formInfoId}")
    public void death(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        deathService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

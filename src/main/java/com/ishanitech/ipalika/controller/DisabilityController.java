package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.DisabilityCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.DisabilityService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class DisabilityController {


    private final DisabilityService disabilityService;

    private DisabilityController(DisabilityService disabilityService) {
        this.disabilityService = disabilityService;
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/disability")
    public ResponseDTO<List<DisabilityCertificateDTO>> getDisabilityRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<DisabilityCertificateDTO>>(disabilityService.getDisabilityRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/disability/{tokenId}")
    public ResponseDTO<DisabilityCertificateDTO> getDisabilityCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<DisabilityCertificateDTO>(disabilityService.getDisabilityCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/disability/{tokenId}")
    public void updateDisabilityCertificateByTokenId(@RequestBody DisabilityCertificateDTO disabilityRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId" + tokenId);
        disabilityService.updateDisabilityCertificateByTokenId(disabilityRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/disability")
    @ApiOperation("Add Disability Registration")
    public void addDisabilityRegistration(HttpServletResponse http, @RequestBody DisabilityCertificateDTO disabilityRegistrationInfo)
            throws CustomSqlException {
        log.info("################");
        log.info("disablility registration = " + disabilityRegistrationInfo);
        disabilityService.addDisabilityRegistration(disabilityRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/disability/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        disabilityService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/disability/types")
    public ResponseDTO<List<String>> getDifferentlyAbledTypes(){
        log.info("################");
        log.info(disabilityService.getDifferentlyAbledTypes().toString());
        return new ResponseDTO<>(disabilityService.getDifferentlyAbledTypes());
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/disability/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        disabilityService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

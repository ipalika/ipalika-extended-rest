package com.ishanitech.ipalika.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.ishanitech.ipalika.config.properties.FileStorageProperties;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.utils.FileUtilService;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ishanitech.ipalika.dto.AnswerDTO;
import com.ishanitech.ipalika.dto.RequestDTO;
import com.ishanitech.ipalika.dto.SurveyAnswerDTO;
import com.ishanitech.ipalika.dto.SurveyAnswerExtraInfoDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.SurveyAnswerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/file-edit")
@RestController
public class EditFileController {

    private final Path storageLocation;

    public EditFileController(FileStorageProperties storageProperties){
        this.storageLocation = Paths.get(storageProperties.getUpload().getDirectory()).toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.storageLocation);
        } catch(IOException ex) {
            throw new FileStorageException("Couldn't create directory to store your file!");
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/editImage")
    public void uploadeditedImageF(@RequestParam("picture") MultipartFile image) {
        log.info(String.format("Image name: %s", image.getOriginalFilename()));
        String fileName = StringUtils.cleanPath(image.getOriginalFilename());

        try {
            if(fileName.contains("..")) {
                throw new FileStorageException(String.format("Sorry your file %s contains invalid characters for pathname.!", fileName));
            }

            Path targetLocation = this.storageLocation.resolve(fileName);
            Files.copy(image.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

        } catch(IOException ex) {
            throw new FileStorageException(String.format("Couldn't store the file %s!", fileName));
        }

    }

}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.LandRegisterDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.LandRegisterService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class LandRegisterController {

    private final LandRegisterService landRegisterService;

    private LandRegisterController(LandRegisterService landRegisterService){this.landRegisterService = landRegisterService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-register")
    public ResponseDTO<List<LandRegisterDTO>> getLandRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<LandRegisterDTO>>(landRegisterService.getLandRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-register/{tokenId}")
    public ResponseDTO<LandRegisterDTO> getLandCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<LandRegisterDTO>(landRegisterService.getLandCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-register/{tokenId}")
    public void updateLandCertificateByTokenId(@RequestBody LandRegisterDTO landRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        landRegisterService.updateLandCertificateByTokenId(landRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-register")
    @ApiOperation("Add Land Registration Recommendation")
    public void addLandRegistration(HttpServletResponse http, @RequestBody LandRegisterDTO landRegistrationInfo)
            throws CustomSqlException {
        landRegisterService.addLandRegistration(landRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-register/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        landRegisterService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-register/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        landRegisterService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

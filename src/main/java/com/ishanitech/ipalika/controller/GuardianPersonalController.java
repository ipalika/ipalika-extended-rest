package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.GuardianPersonalDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.GuardianPersonalService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class GuardianPersonalController {

    private final GuardianPersonalService guardianPersonalService;

    private GuardianPersonalController(GuardianPersonalService guardianPersonalService){this.guardianPersonalService = guardianPersonalService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/guardian-personal")
    public ResponseDTO<List<GuardianPersonalDTO>> getGuardianRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<GuardianPersonalDTO>>(guardianPersonalService.getGuardianRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/guardian-personal/{tokenId}")
    public ResponseDTO<GuardianPersonalDTO> getGuardianCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<GuardianPersonalDTO>(guardianPersonalService.getGuardianCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/guardian-personal/{tokenId}")
    public void updateGuardianCertificateByTokenId(@RequestBody GuardianPersonalDTO guardianRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        guardianPersonalService.updateGuardianCertificateByTokenId(guardianRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/guardian-personal")
    @ApiOperation("Add Guardian Recommendation (Personal) Registration")
    public void addGuardianRegistration(HttpServletResponse http, @RequestBody GuardianPersonalDTO guardianRegistrationInfo)
            throws CustomSqlException {
        guardianPersonalService.addGuardianRegistration(guardianRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/guardian-personal/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        guardianPersonalService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/guardian-personal/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        guardianPersonalService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

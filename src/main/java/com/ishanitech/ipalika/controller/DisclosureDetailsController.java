package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.DisclosureDetailsDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.DisclosureDetailsService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class DisclosureDetailsController {

    private final DisclosureDetailsService disclosureDetailsService;

    private DisclosureDetailsController(DisclosureDetailsService disclosureDetailsService){this.disclosureDetailsService = disclosureDetailsService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/disclosure")
    public ResponseDTO<List<DisclosureDetailsDTO>> getDisclosureRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<DisclosureDetailsDTO>>(disclosureDetailsService.getDisclosureRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/disclosure/{tokenId}")
    public ResponseDTO<DisclosureDetailsDTO> getDisclosureCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<DisclosureDetailsDTO>(disclosureDetailsService.getDisclosureCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/disclosure/{tokenId}")
    public void updateDisclosureCertificateByTokenId(@RequestBody DisclosureDetailsDTO disclosureRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        disclosureDetailsService.updateDisclosureCertificateByTokenId(disclosureRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/disclosure")
    @ApiOperation("Add Disclosure Details Registration")
    public void addDisclosureRegistration(HttpServletResponse http, @RequestBody DisclosureDetailsDTO disclosureRegistrationInfo)
            throws CustomSqlException {
        disclosureDetailsService.addDisclosureRegistration(disclosureRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/disclosure/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        disclosureDetailsService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/disclosure/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        disclosureDetailsService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.RegistrationReport;
import com.ishanitech.ipalika.service.RegistrationReportService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RequestMapping("/registration-report")
@RestController
public class RegistrationReportController {
    private final RegistrationReportService registrationReportService;

    public RegistrationReportController(RegistrationReportService registrationReportService) {
        this.registrationReportService = registrationReportService;
    }


    @PostMapping("/generate")
    public void populateRegistrationCount() {
    	registrationReportService.populateRegistrationCount();
    }
    
    
    //just for testing
    @GetMapping("/test")
    public void getCounts() {
        registrationReportService.getCounts();
    }

    //actual implementation here
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public ResponseDTO<List<RegistrationReport>> getRegistrationReport(HttpServletRequest request) {
//    	registrationReportService.populateRegistrationCount();
        return new ResponseDTO<List<RegistrationReport>>(registrationReportService.getRegistrationReports());
    }

}

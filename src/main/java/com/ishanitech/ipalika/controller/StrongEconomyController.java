package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.StrongEconomyDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.StrongEconomyService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class StrongEconomyController {

    private final StrongEconomyService strongEconomyService;

    private StrongEconomyController(StrongEconomyService strongEconomyService){this.strongEconomyService = strongEconomyService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/strong-economy")
    public ResponseDTO<List<StrongEconomyDTO>> getStrongEconomyRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<StrongEconomyDTO>>(strongEconomyService.getStrongEconomyRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/strong-economy/{tokenId}")
    public ResponseDTO<StrongEconomyDTO> getStrongEconomyCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<StrongEconomyDTO>(strongEconomyService.getStrongEconomyCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/strong-economy/{tokenId}")
    public void updateStrongEconomyCertificateByTokenId(@RequestBody StrongEconomyDTO economyRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        strongEconomyService.updateStrongEconomyCertificateByTokenId(economyRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/strong-economy")
    @ApiOperation("Add Strong Economic Condition Registration")
    public void addStrongEconomyRegistration(HttpServletResponse http, @RequestBody StrongEconomyDTO economyRegistrationInfo)
            throws CustomSqlException {
        strongEconomyService.addStrongEconomyRegistration(economyRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/strong-economy/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        strongEconomyService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/strong-economy/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        strongEconomyService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

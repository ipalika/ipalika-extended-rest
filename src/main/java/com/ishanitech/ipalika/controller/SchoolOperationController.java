package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.SchoolOperationDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.SchoolOperationService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class SchoolOperationController {

    private final SchoolOperationService schoolOperationService;

    private SchoolOperationController(SchoolOperationService schoolOperationService){this.schoolOperationService =schoolOperationService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/school-operation")
    public ResponseDTO<List<SchoolOperationDTO>> getSchoolRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<SchoolOperationDTO>>(schoolOperationService.getSchoolRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/school-operation/{tokenId}")
    public ResponseDTO<SchoolOperationDTO> getSchoolCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<SchoolOperationDTO>(schoolOperationService.getSchoolCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/school-operation/{tokenId}")
    public void updateSchoolCertificateByTokenId(@RequestBody SchoolOperationDTO schoolRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        schoolOperationService.updateSchoolCertificateByTokenId(schoolRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/school-operation")
    @ApiOperation("Add School Operation Registration")
    public void addSchoolRegistration(HttpServletResponse http, @RequestBody SchoolOperationDTO schoolRegistrationInfo)
            throws CustomSqlException {
        schoolOperationService.addSchoolRegistration(schoolRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/school-operation/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        schoolOperationService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/school-operation/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        schoolOperationService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.LandPurjaLostDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.LandPurjaLostService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class LandPurjaLostController {

    private final LandPurjaLostService landPurjaLostService;

    private LandPurjaLostController(LandPurjaLostService landPurjaLostService){this.landPurjaLostService = landPurjaLostService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-purja")
    public ResponseDTO<List<LandPurjaLostDTO>> getLandRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<LandPurjaLostDTO>>(landPurjaLostService.getLandRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-purja/{tokenId}")
    public ResponseDTO<LandPurjaLostDTO> getLandCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<LandPurjaLostDTO>(landPurjaLostService.getLandCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-purja/{tokenId}")
    public void updateLandCertificateByTokenId(@RequestBody LandPurjaLostDTO landRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        landPurjaLostService.updateLandCertificateByTokenId(landRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-purja")
    @ApiOperation("Add Land Dhani-Purja Lost Registration")
    public void addLandRegistration(HttpServletResponse http, @RequestBody LandPurjaLostDTO landRegistrationInfo)
            throws CustomSqlException {
        landPurjaLostService.addLandRegistration(landRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-purja/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        landPurjaLostService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-purja/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        landPurjaLostService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

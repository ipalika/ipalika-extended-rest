package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.InternalResidenceDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.InternalResidenceService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class InternalResidenceController {

    private final InternalResidenceService internalResidenceService;

    private InternalResidenceController(InternalResidenceService internalResidenceService){this.internalResidenceService = internalResidenceService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/internal-residence")
    public ResponseDTO<List<InternalResidenceDTO>> getResidenceRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<InternalResidenceDTO>>(internalResidenceService.getResidenceRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/internal-residence/{tokenId}")
    public ResponseDTO<InternalResidenceDTO> getResidenceCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<InternalResidenceDTO>(internalResidenceService.getResidenceCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/internal-residence/{tokenId}")
    public void updateResidenceCertificateByTokenId(@RequestBody InternalResidenceDTO internalResidenceInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        internalResidenceService.updateResidenceCertificateByTokenId(internalResidenceInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/internal-residence")
    @ApiOperation("Add Internal Residence Registration")
    public void addResidenceRegistration(HttpServletResponse http, @RequestBody InternalResidenceDTO internalResidenceInfo)
            throws CustomSqlException {
        internalResidenceService.addResidenceRegistration(internalResidenceInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/internal-residence/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        internalResidenceService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/internal-residence/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        internalResidenceService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.CourtFeeDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.CourtFeeService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class CourtFeeController {

    private final CourtFeeService courtFeeService;

    private CourtFeeController(CourtFeeService courtFeeService){this.courtFeeService = courtFeeService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/court")
    public ResponseDTO<List<CourtFeeDTO>> getCourtRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<CourtFeeDTO>>(courtFeeService.getCourtRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/court/{tokenId}")
    public ResponseDTO<CourtFeeDTO> getCourtCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<CourtFeeDTO>(courtFeeService.getCourtCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/court/{tokenId}")
    public void updateCourtCertificateByTokenId(@RequestBody CourtFeeDTO courtRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        courtFeeService.updateCourtCertificateByTokenId(courtRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/court")
    @ApiOperation("Add Court Fee Deduction Recommendation Registration")
    public void addCourtRegistration(HttpServletResponse http, @RequestBody CourtFeeDTO courtRegistrationInfo)
            throws CustomSqlException {
        courtFeeService.addCourtRegistration(courtRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/court/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        courtFeeService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/court/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        courtFeeService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

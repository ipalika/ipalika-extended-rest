package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.MinorCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.MinorService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class MinorController {

    private final MinorService minorService;

    private MinorController(MinorService minorService){this.minorService = minorService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/minor")
    public ResponseDTO<List<MinorCertificateDTO>> getMinorRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<MinorCertificateDTO>>(minorService.getMinorRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/minor/{tokenId}")
    public ResponseDTO<MinorCertificateDTO> getMinorCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<MinorCertificateDTO>(minorService.getMinorCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/minor/{tokenId}")
    public void updateMinorCertificateByTokenId(@RequestBody MinorCertificateDTO minorRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        minorService.updateMinorCertificateByTokenId(minorRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/minor")
    @ApiOperation("Add Minor Registration")
    public void addMinorRegistration(HttpServletResponse http, @RequestBody MinorCertificateDTO minorRegistrationInfo)
            throws CustomSqlException {
        minorService.addMinorRegistration(minorRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/minor/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        minorService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/minor/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        minorService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.QuadrupedDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.QuadrupedService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class QuadrupedController {

    private final QuadrupedService quadrupedService;

    private QuadrupedController(QuadrupedService quadrupedService){this.quadrupedService = quadrupedService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/quadruped")
    public ResponseDTO<List<QuadrupedDTO>> getQuadrupedRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<QuadrupedDTO>>(quadrupedService.getQuadrupedRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/quadruped/{tokenId}")
    public ResponseDTO<QuadrupedDTO> getQuadrupedCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<QuadrupedDTO>(quadrupedService.getQuadrupedCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/quadruped/{tokenId}")
    public void updateQuadrupedCertificateByTokenId(@RequestBody QuadrupedDTO quadrupedRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        quadrupedService.updateQuadrupedCertificateByTokenId(quadrupedRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/quadruped")
    @ApiOperation("Add Quadruped Registration")
    public void addQuadrupedRegistration(HttpServletResponse http, @RequestBody QuadrupedDTO quadrupedRegistrationInfo)
            throws CustomSqlException {
        quadrupedService.addQuadrupedRegistration(quadrupedRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/quadruped/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        quadrupedService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/quadruped/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        quadrupedService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.BusinessNotOperatingDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.BusinessNotOperatingService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class BusinessNotOperatingController {

    private final BusinessNotOperatingService businessNotOperatingService;

    private BusinessNotOperatingController(BusinessNotOperatingService businessNotOperatingService){this.businessNotOperatingService = businessNotOperatingService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/business")
    public ResponseDTO<List<BusinessNotOperatingDTO>> getBusinessRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<BusinessNotOperatingDTO>>(businessNotOperatingService.getBusinessRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/business/{tokenId}")
    public ResponseDTO<BusinessNotOperatingDTO> getBusinessCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<BusinessNotOperatingDTO>(businessNotOperatingService.getBusinessCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/business/{tokenId}")
    public void updateBusinessCertificateByTokenId(@RequestBody BusinessNotOperatingDTO businessRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        businessNotOperatingService.updateBusinessCertificateByTokenId(businessRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/business")
    @ApiOperation("Add Business Not Operating Registration")
    public void addBusinessRegistration(HttpServletResponse http, @RequestBody BusinessNotOperatingDTO businessRegistrationInfo)
            throws CustomSqlException {
        businessNotOperatingService.addBusinessRegistration(businessRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/business/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        businessNotOperatingService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/business/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        businessNotOperatingService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;


import com.ishanitech.ipalika.dto.DeceasedCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.DeceasedService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class RelationDeceasedController {


    private final DeceasedService deceasedService;

    private RelationDeceasedController(DeceasedService deceasedService){this.deceasedService = deceasedService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/deceased")
    public ResponseDTO<List<DeceasedCertificateDTO>> getRelationRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<DeceasedCertificateDTO>>(deceasedService.getRelationRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/deceased/{tokenId}")
    public ResponseDTO<DeceasedCertificateDTO> getRelationCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<DeceasedCertificateDTO>(deceasedService.getRelationCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/deceased/{tokenId}")
    public void updateRelationCertificateByTokenId(@RequestBody DeceasedCertificateDTO relationRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId" + tokenId);
        deceasedService.updateRelationCertificateByTokenId(relationRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/deceased")
    @ApiOperation("Add Relation Registration")
    public void addRelationRegistration(HttpServletResponse http, @RequestBody DeceasedCertificateDTO relationRegistrationInfo)
            throws CustomSqlException {
        deceasedService.addRelationRegistration(relationRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/deceased/image")
    public void uploadRelationCertificatePhoto(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        deceasedService.uploadRelationCertificate(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/deceased/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        deceasedService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

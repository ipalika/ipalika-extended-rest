package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.BahalTaxDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.BahalTaxService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class BahalTaxController {

    private final BahalTaxService bahalTaxService;

    private BahalTaxController(BahalTaxService bahalTaxService){this.bahalTaxService = bahalTaxService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/bahal-tax")
    public ResponseDTO<List<BahalTaxDTO>> getBahalTaxRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<BahalTaxDTO>>(bahalTaxService.getBahalTaxRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/bahal-tax/{tokenId}")
    public ResponseDTO<BahalTaxDTO> getBahalTaxCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<BahalTaxDTO>(bahalTaxService.getBahalTaxCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/bahal-tax/{tokenId}")
    public void updateBahalTaxCertificateByTokenId(@RequestBody BahalTaxDTO bahalRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        bahalTaxService.updateBahalTaxCertificateByTokenId(bahalRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/bahal-tax")
    @ApiOperation("Add Bahal-Tax Registration")
    public void addBahalTaxRegistration(HttpServletResponse http, @RequestBody BahalTaxDTO bahalRegistrationInfo)
            throws CustomSqlException {
        bahalTaxService.addBahalTaxRegistration(bahalRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/bahal-tax/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        bahalTaxService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/bahal-tax/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        bahalTaxService.updateCertificateStatus(tokenId, status, formInfoId);
    }

}

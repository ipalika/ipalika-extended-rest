package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.KothaKholneDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.KothaKholneService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class KothaKholneController {

    private final KothaKholneService kothaKholneService;

    private KothaKholneController(KothaKholneService kothaKholneService){this.kothaKholneService = kothaKholneService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/kotha")
    public ResponseDTO<List<KothaKholneDTO>> getKothaRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<KothaKholneDTO>>(kothaKholneService.getKothaRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/kotha/{tokenId}")
    public ResponseDTO<KothaKholneDTO> getKothaCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<KothaKholneDTO>(kothaKholneService.getKothaCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/kotha/{tokenId}")
    public void updateKothaCertificateByTokenId(@RequestBody KothaKholneDTO kothaRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        kothaKholneService.updateKothaCertificateByTokenId(kothaRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/kotha")
    @ApiOperation("Add Kotha Kholne Karya Registration")
    public void addKothaRegistration(HttpServletResponse http, @RequestBody KothaKholneDTO kothaRegistrationInfo)
            throws CustomSqlException {
        kothaKholneService.addKothaRegistration(kothaRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/kotha/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        kothaKholneService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/kotha/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        kothaKholneService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.GuardianInstitutionalDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.GuardianInstitutionalService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class GuardianInstitutionalController {

    private final GuardianInstitutionalService guardianInstitutionalService;

    private GuardianInstitutionalController(GuardianInstitutionalService guardianInstitutionalService){this.guardianInstitutionalService = guardianInstitutionalService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/guardian-institutional")
    public ResponseDTO<List<GuardianInstitutionalDTO>> getGuardianRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<GuardianInstitutionalDTO>>(guardianInstitutionalService.getGuardianRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/guardian-institutional/{tokenId}")
    public ResponseDTO<GuardianInstitutionalDTO> getGuardianCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<GuardianInstitutionalDTO>(guardianInstitutionalService.getGuardianCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/guardian-institutional/{tokenId}")
    public void updateGuardianCertificateByTokenId(@RequestBody GuardianInstitutionalDTO guardianRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        guardianInstitutionalService.updateGuardianCertificateByTokenId(guardianRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/guardian-institutional")
    @ApiOperation("Add Guardian Recommendation (Institutional) Registration")
    public void addGuardianRegistration(HttpServletResponse http, @RequestBody GuardianInstitutionalDTO guardianRegistrationInfo)
            throws CustomSqlException {
        guardianInstitutionalService.addGuardianRegistration(guardianRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/guardian-institutional/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        guardianInstitutionalService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/guardian-institutional/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        guardianInstitutionalService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

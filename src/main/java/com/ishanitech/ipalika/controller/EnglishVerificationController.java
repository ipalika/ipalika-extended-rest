package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.EnglishVerificationDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.EnglishVerificationService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class EnglishVerificationController {

    private final EnglishVerificationService englishVerificationService;

    private EnglishVerificationController(EnglishVerificationService englishVerificationService){this.englishVerificationService = englishVerificationService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/english-verify")
    public ResponseDTO<List<EnglishVerificationDTO>> getEnglishVerifications(HttpServletRequest request) {
        return new ResponseDTO<List<EnglishVerificationDTO>>(englishVerificationService.getEnglishVerifications(request));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/english-verify")
    @ApiOperation("Add English Verification")
    public void addEnglishVerification(HttpServletResponse http, @RequestBody EnglishVerificationDTO englishVerificationInfo)
            throws CustomSqlException {
        englishVerificationService.addEnglishVerification(englishVerificationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/english-verify/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image)
            throws FileStorageException {
        englishVerificationService.addAllImages(image);
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/english-verify/{tokenId}")
    public ResponseDTO<EnglishVerificationDTO> getEnglishVerificationByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<EnglishVerificationDTO>(englishVerificationService.getEnglishVerificationByTokenId(tokenId));

    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/english-verify/{tokenId}")
    public void updateEnglishVerificationByTokenId(@RequestBody EnglishVerificationDTO englishVerificationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        englishVerificationService.updateEnglishVerificationByTokenId(englishVerificationInfo, tokenId);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/english-verify/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        englishVerificationService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.PropertyTaxDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.PropertyTaxService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class PropertyTaxController {

    private final PropertyTaxService propertyTaxService;

    private PropertyTaxController(PropertyTaxService propertyTaxService){this.propertyTaxService = propertyTaxService;}

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/property-tax")
    public ResponseDTO<List<PropertyTaxDTO>> getPropertyTaxRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<PropertyTaxDTO>>(propertyTaxService.getPropertyTaxRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/property-tax/{tokenId}")
    public ResponseDTO<PropertyTaxDTO> getPropertyTaxCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<PropertyTaxDTO>(propertyTaxService.getPropertyTaxCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/property-tax/{tokenId}")
    public void updatePropertyTaxCertificateByTokenId(@RequestBody PropertyTaxDTO propertyRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        propertyTaxService.updatePropertyTaxCertificateByTokenId(propertyRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/property-tax")
    @ApiOperation("Add Property Tax Registration")
    public void addPropertyTaxRegistration(HttpServletResponse http, @RequestBody PropertyTaxDTO propertyRegistrationInfo)
            throws CustomSqlException {
        propertyTaxService.addPropertyTaxRegistration(propertyRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/property-tax/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        propertyTaxService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/property-tax/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        propertyTaxService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

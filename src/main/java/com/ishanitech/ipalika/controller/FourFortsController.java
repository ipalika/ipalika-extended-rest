package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.FourFortsDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.FourFortsService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class FourFortsController {

    private final FourFortsService fourFortsService;

    private FourFortsController(FourFortsService fourFortsService){this.fourFortsService = fourFortsService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/four-forts")
    public ResponseDTO<List<FourFortsDTO>> getFourFortsRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<FourFortsDTO>>(fourFortsService.getFourFortsRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/four-forts/{tokenId}")
    public ResponseDTO<FourFortsDTO> getFourFortsCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<FourFortsDTO>(fourFortsService.getFourFortsCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/four-forts/{tokenId}")
    public void updateFourFortsCertificateByTokenId(@RequestBody FourFortsDTO fortRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        fourFortsService.updateFourFortsCertificateByTokenId(fortRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/four-forts")
    @ApiOperation("Add Four Forts Registration")
    public void addFourFortsRegistration(HttpServletResponse http, @RequestBody FourFortsDTO fortRegistrationInfo)
            throws CustomSqlException {
        fourFortsService.addFourFortsRegistration(fortRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/four-forts/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        fourFortsService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/four-forts/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        fourFortsService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

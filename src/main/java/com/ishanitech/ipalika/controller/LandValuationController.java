package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.LandValuationDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.LandValuationService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class LandValuationController {

    private final LandValuationService landValuationService;

    private LandValuationController(LandValuationService landValuationService){this.landValuationService = landValuationService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-valuate")
    public ResponseDTO<List<LandValuationDTO>> getLandRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<LandValuationDTO>>(landValuationService.getLandRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/land-valuate/{tokenId}")
    public ResponseDTO<LandValuationDTO> getLandCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<LandValuationDTO>(landValuationService.getLandCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-valuate/{tokenId}")
    public void updateLandCertificateByTokenId(@RequestBody LandValuationDTO landRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        landValuationService.updateLandCertificateByTokenId(landRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-valuate")
    @ApiOperation("Add Land Valuation Registration")
    public void addLandRegistration(HttpServletResponse http, @RequestBody LandValuationDTO landRegistrationInfo)
            throws CustomSqlException {
        landValuationService.addLandRegistration(landRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/land-valuate/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        landValuationService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/land-valuate/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        landValuationService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

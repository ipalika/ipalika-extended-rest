package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.FreeHealthDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.FreeHealthService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class FreeHealthController {

    private final FreeHealthService freeHealthService;

    private FreeHealthController(FreeHealthService freeHealthService){this.freeHealthService = freeHealthService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/free-health")
    public ResponseDTO<List<FreeHealthDTO>> getFreeHealthRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<FreeHealthDTO>>(freeHealthService.getFreeHealthRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/free-health/{tokenId}")
    public ResponseDTO<FreeHealthDTO> getFreeHealthCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<FreeHealthDTO>(freeHealthService.getFreeHealthCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/free-health/{tokenId}")
    public void updateFreeHealthCertificateByTokenId(@RequestBody FreeHealthDTO healthRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        freeHealthService.updateFreeHealthCertificateByTokenId(healthRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/free-health")
    @ApiOperation("Add Free HealthCare Registration")
    public void addFreeHealthRegistration(HttpServletResponse http, @RequestBody FreeHealthDTO healthRegistrationInfo)
            throws CustomSqlException {
        freeHealthService.addFreeHealthRegistration(healthRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/free-health/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        freeHealthService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/free-health/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        freeHealthService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

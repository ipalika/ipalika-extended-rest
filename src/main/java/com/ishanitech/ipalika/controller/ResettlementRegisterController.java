package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResettlementRegisterDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.ResettlementRegisterService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class ResettlementRegisterController {

    private ResettlementRegisterService resettlementRegisterService;

    private ResettlementRegisterController(ResettlementRegisterService resettlementRegisterService){this.resettlementRegisterService = resettlementRegisterService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/resettlement")
    public ResponseDTO<List<ResettlementRegisterDTO>> getResettlementRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<ResettlementRegisterDTO>>(resettlementRegisterService.getResettlementRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/resettlement/{tokenId}")
    public ResponseDTO<ResettlementRegisterDTO> getResettlementCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<ResettlementRegisterDTO>(resettlementRegisterService.getResettlementCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/resettlement/{tokenId}")
    public void updateResettlementCertificateByTokenId(@RequestBody ResettlementRegisterDTO resettlementRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        resettlementRegisterService.updateResettlementCertificateByTokenId(resettlementRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/resettlement")
    @ApiOperation("Add Resettlement (Incoming/Outgoing) Registration")
    public void addResettlementRegistration(HttpServletResponse http, @RequestBody ResettlementRegisterDTO resettlementRegistrationInfo)
            throws CustomSqlException {
        resettlementRegisterService.addResettlementRegistration(resettlementRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/resettlement/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        resettlementRegisterService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/resettlement/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        resettlementRegisterService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ManjurinamaVerificationDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.ManjurinamaVerificationService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class ManjurinamaVerificationController {

    private final ManjurinamaVerificationService manjurinamaVerificationService;

    private ManjurinamaVerificationController(ManjurinamaVerificationService manjurinamaVerificationService){this.manjurinamaVerificationService = manjurinamaVerificationService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/manjurinama-verify")
    public ResponseDTO<List<ManjurinamaVerificationDTO>> getManjurinamaVerifications(HttpServletRequest request) {
        return new ResponseDTO<List<ManjurinamaVerificationDTO>>(manjurinamaVerificationService.getManjurinamaVerifications(request));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/manjurinama-verify")
    @ApiOperation("Add Manjurinama Verification")
    public void addManjurinamaVerification(HttpServletResponse http, @RequestBody ManjurinamaVerificationDTO manjurinamaVerificationInfo)
            throws CustomSqlException {
        manjurinamaVerificationService.addManjurinamaVerification(manjurinamaVerificationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/manjurinama-verify/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image)
            throws FileStorageException {
        manjurinamaVerificationService.addAllImages(image);
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/manjurinama-verify/{tokenId}")
    public ResponseDTO<ManjurinamaVerificationDTO> getManjurinamaVerificationByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<ManjurinamaVerificationDTO>(manjurinamaVerificationService.getManjurinamaVerificationByTokenId(tokenId));

    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/manjurinama-verify/{tokenId}")
    public void updateManjurinamaVerificationByTokenId(@RequestBody ManjurinamaVerificationDTO manjurinamaVerificationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        manjurinamaVerificationService.updateManjurinamaVerificationByTokenId(manjurinamaVerificationInfo, tokenId);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/manjurinama-verify/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        manjurinamaVerificationService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

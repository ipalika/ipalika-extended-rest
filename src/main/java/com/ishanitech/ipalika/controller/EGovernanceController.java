package com.ishanitech.ipalika.controller;

import 	java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.RelationCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.EGovernanceService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class EGovernanceController {

	private final EGovernanceService eGovernanceService;

	private EGovernanceController(EGovernanceService eGovernanceService) {
		this.eGovernanceService = eGovernanceService;
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/birth")
	public ResponseDTO<List<BirthCertificateDTO>> getBirthRegistrations(HttpServletRequest request) {
		return new ResponseDTO<List<BirthCertificateDTO>>(eGovernanceService.getBirthRegistrations(request));
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/birth")
	@ApiOperation("Add Birth Registration")
	public void addBirthRegistration(HttpServletResponse http, @RequestBody BirthCertificateDTO birthRegistrationInfo)
			throws CustomSqlException {
		eGovernanceService.addBirthRegistration(birthRegistrationInfo);
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/birth/{tokenId}")
	public ResponseDTO<BirthCertificateDTO> getBirthCertificateByTokenId(@PathVariable("tokenId") String tokenId)
			throws EntityNotFoundException {
//		log.info("incoming tokenId" + tokenId);
		return new ResponseDTO<BirthCertificateDTO>(eGovernanceService.getBirthCertificateByTokenId(tokenId));
	}
	
	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/birth/status/{tokenId}/{status}/{formInfoId}")
	public void updateBirthCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
													  @PathVariable("status") Integer status,
													  @PathVariable("formInfoId") Integer formInfoId)
			throws EntityNotFoundException {
		eGovernanceService.updateBirthCertificateStatus(tokenId, status, formInfoId);
	}

	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/birth/{tokenId}")
	public void updateBirthCertificateByTokenId(@RequestBody BirthCertificateDTO birthRegistrationInfo, @PathVariable("tokenId") String tokenId)
			throws CustomSqlException {
		log.info("tokenId" + tokenId);
		eGovernanceService.updateBirthCertificateByTokenId(birthRegistrationInfo, tokenId);
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/birth/image")
	public void uploadBirthCertificatePhoto(@RequestParam("picture") MultipartFile image) throws FileStorageException {
		log.info(image.toString());
		eGovernanceService.addBirthCertificateImage(image);
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/relation")
	public ResponseDTO<List<RelationCertificateDTO>> getRelationRegistrations(HttpServletRequest request) {
		return new ResponseDTO<List<RelationCertificateDTO>>(eGovernanceService.getRelationRegistrations(request));
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/relation")
	@ApiOperation("Add RELATIon Registration")
	public void addRelationRegistration(HttpServletResponse http, @RequestBody RelationCertificateDTO relationRegistrationInfo)
			throws CustomSqlException {
		eGovernanceService.addRelationRegistration(relationRegistrationInfo);
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/relation/{tokenId}")
	public ResponseDTO<RelationCertificateDTO> getRelationCertificateByTokenId(@PathVariable("tokenId") String tokenId)
			throws EntityNotFoundException {
//		log.info("incoming tokenId" + tokenId);
		return new ResponseDTO<RelationCertificateDTO>(eGovernanceService.getRelationCertificateByTokenId(tokenId));
	}

	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/relation/status/{tokenId}/{status}/{formInfoId}")
	public void updateRelationCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
													  @PathVariable("status") Integer status,
													  @PathVariable("formInfoId") Integer formInfoId)
			throws EntityNotFoundException {
		eGovernanceService.updateRelationCertificateStatus(tokenId, status, formInfoId);
	}
	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/relation/{tokenId}")
	public void updateRelationCertificateByTokenId(@RequestBody RelationCertificateDTO relationRegistrationInfo, @PathVariable("tokenId") String tokenId)
			throws CustomSqlException {
		log.info("tokenId" + tokenId);
		eGovernanceService.updateRelationCertificateByTokenId(relationRegistrationInfo, tokenId);
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/relation/image")
	public void uploadRelationCertificatePhoto(@RequestParam("picture") MultipartFile image) throws FileStorageException {
		eGovernanceService.uploadRelationCertificate(image);
	}

}

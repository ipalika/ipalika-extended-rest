package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.PersonalDetailsDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.PersonalDetailsService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class PersonalDetailsController {

    private final PersonalDetailsService personalDetailsService;

    private PersonalDetailsController(PersonalDetailsService personalDetailsService){this.personalDetailsService = personalDetailsService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/personal-details")
    public ResponseDTO<List<PersonalDetailsDTO>> getPersonalDetailsRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<PersonalDetailsDTO>>(personalDetailsService.getPersonalDetailsRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/personal-details/{tokenId}")
    public ResponseDTO<PersonalDetailsDTO> getPersonalDetailsCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<PersonalDetailsDTO>(personalDetailsService.getPersonalDetailsCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/personal-details/{tokenId}")
    public void updatePersonalDetailsCertificateByTokenId(@RequestBody PersonalDetailsDTO personalRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        personalDetailsService.updatePersonalDetailsCertificateByTokenId(personalRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/personal-details")
    @ApiOperation("Add Personal Details Registration")
    public void addPersonalDetailsRegistration(HttpServletResponse http, @RequestBody PersonalDetailsDTO personalRegistrationInfo)
            throws CustomSqlException {
        personalDetailsService.addPersonalDetailsRegistration(personalRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/personal-details/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        personalDetailsService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/personal-details/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        personalDetailsService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

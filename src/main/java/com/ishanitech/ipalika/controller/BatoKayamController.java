package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.BatoKayamDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.BatoKayamService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class BatoKayamController {

    private final BatoKayamService batoKayamService;

    private BatoKayamController(BatoKayamService batoKayamService){this.batoKayamService = batoKayamService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/bato-kayam")
    public ResponseDTO<List<BatoKayamDTO>> getBatoKayamRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<BatoKayamDTO>>(batoKayamService.getBatoKayamRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/bato-kayam/{tokenId}")
    public ResponseDTO<BatoKayamDTO> getBatoKayamCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<BatoKayamDTO>(batoKayamService.getBatoKayamCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/bato-kayam/{tokenId}")
    public void updateBatoKayamCertificateByTokenId(@RequestBody BatoKayamDTO batoRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        batoKayamService.updateBatoKayamCertificateByTokenId(batoRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/bato-kayam")
    @ApiOperation("Add Bato Kayam Registration")
    public void addBatoKayamRegistration(HttpServletResponse http, @RequestBody BatoKayamDTO batoRegistrationInfo)
            throws CustomSqlException {
        batoKayamService.addBatoKayamRegistration(batoRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/bato-kayam/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        batoKayamService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/bato-kayam/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        batoKayamService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

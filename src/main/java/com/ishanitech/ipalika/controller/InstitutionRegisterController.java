package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.InstitutionRegisterDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.InstitutionRegisterService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class InstitutionRegisterController {

    private final InstitutionRegisterService institutionRegisterService;

    private InstitutionRegisterController(InstitutionRegisterService institutionRegisterService){this.institutionRegisterService = institutionRegisterService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/institution-register")
    public ResponseDTO<List<InstitutionRegisterDTO>> getInstitutionRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<InstitutionRegisterDTO>>(institutionRegisterService.getInstitutionRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/institution-register/{tokenId}")
    public ResponseDTO<InstitutionRegisterDTO> getInstitutionCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<InstitutionRegisterDTO>(institutionRegisterService.getInstitutionCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/institution-register/{tokenId}")
    public void updateInstitutionCertificateByTokenId(@RequestBody InstitutionRegisterDTO institutionRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        institutionRegisterService.updateInstitutionCertificateByTokenId(institutionRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/institution-register")
    @ApiOperation("Add Institution Registration")
    public void addInstitutionRegistration(HttpServletResponse http, @RequestBody InstitutionRegisterDTO institutionRegistrationInfo)
            throws CustomSqlException {
        institutionRegisterService.addInstitutionRegistration(institutionRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/institution-register/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        institutionRegisterService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/institution-register/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        institutionRegisterService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

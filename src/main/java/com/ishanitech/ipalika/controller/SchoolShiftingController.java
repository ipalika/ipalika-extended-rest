package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.SchoolShiftingDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.SchoolShiftingService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class SchoolShiftingController {

    private final SchoolShiftingService schoolShiftingService;

    private SchoolShiftingController(SchoolShiftingService schoolShiftingService){this.schoolShiftingService = schoolShiftingService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/school-shifting")
    public ResponseDTO<List<SchoolShiftingDTO>> getSchoolRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<SchoolShiftingDTO>>(schoolShiftingService.getSchoolRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/school-shifting/{tokenId}")
    public ResponseDTO<SchoolShiftingDTO> getSchoolCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<SchoolShiftingDTO>(schoolShiftingService.getSchoolCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/school-shifting/{tokenId}")
    public void updateSchoolCertificateByTokenId(@RequestBody SchoolShiftingDTO schoolRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        schoolShiftingService.updateSchoolCertificateByTokenId(schoolRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/school-shifting")
    @ApiOperation("Add School Shifting Registration")
    public void addSchoolRegistration(HttpServletResponse http, @RequestBody SchoolShiftingDTO schoolRegistrationInfo)
            throws CustomSqlException {
        schoolShiftingService.addSchoolRegistration(schoolRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/school-shifting/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        schoolShiftingService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/school-shifting/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        schoolShiftingService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

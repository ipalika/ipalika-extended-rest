package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.PoorEconomyDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.PoorEconomyService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class PoorEconomyController {

    private final PoorEconomyService poorEconomyService;

    private PoorEconomyController(PoorEconomyService poorEconomyService){this.poorEconomyService = poorEconomyService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/poor-economy")
    public ResponseDTO<List<PoorEconomyDTO>> getPoorEconomyRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<PoorEconomyDTO>>(poorEconomyService.getPoorEconomyRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/poor-economy/{tokenId}")
    public ResponseDTO<PoorEconomyDTO> getPoorEconomyCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<PoorEconomyDTO>(poorEconomyService.getPoorEconomyCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/poor-economy/{tokenId}")
    public void updatePoorEconomyCertificateByTokenId(@RequestBody PoorEconomyDTO economyRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        poorEconomyService.updatePoorEconomyCertificateByTokenId(economyRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/poor-economy")
    @ApiOperation("Add Poor Economy Condition Registration")
    public void addPoorEconomyRegistration(HttpServletResponse http, @RequestBody PoorEconomyDTO economyRegistrationInfo)
            throws CustomSqlException {
        poorEconomyService.addPoorEconomyRegistration(economyRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/poor-economy/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        poorEconomyService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/poor-economy/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        poorEconomyService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

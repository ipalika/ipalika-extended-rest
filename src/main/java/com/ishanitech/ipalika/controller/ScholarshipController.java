package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.ScholarshipCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.ScholarshipService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class ScholarshipController {

    private final ScholarshipService scholarshipService;

    private ScholarshipController(ScholarshipService scholarshipService){this.scholarshipService = scholarshipService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/scholarship")
    public ResponseDTO<List<ScholarshipCertificateDTO>> getScholarshipRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<ScholarshipCertificateDTO>>(scholarshipService.getScholarshipRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/scholarship/{tokenId}")
    public ResponseDTO<ScholarshipCertificateDTO> getScholarshipCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<ScholarshipCertificateDTO>(scholarshipService.getScholarshipCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/scholarship/{tokenId}")
    public void updateScholarshipCertificateByTokenId(@RequestBody ScholarshipCertificateDTO scholarshipRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        scholarshipService.updateScholarshipCertificateByTokenId(scholarshipRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/scholarship")
    @ApiOperation("Add Scholarship Registration")
    public void addScholarshipRegistration(HttpServletResponse http, @RequestBody ScholarshipCertificateDTO scholarshipRegistrationInfo)
            throws CustomSqlException {
        scholarshipService.addScholarshipRegistration(scholarshipRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/scholarship/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        scholarshipService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/scholarship/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        scholarshipService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

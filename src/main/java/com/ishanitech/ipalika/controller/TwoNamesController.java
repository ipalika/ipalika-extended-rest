package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.dto.TwoNamesDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.TwoNamesService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class TwoNamesController {

    private final TwoNamesService twoNamesService;

    private TwoNamesController(TwoNamesService twoNamesService){this.twoNamesService = twoNamesService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/two-names")
    public ResponseDTO<List<TwoNamesDTO>> getTwoNamesRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<TwoNamesDTO>>(twoNamesService.getTwoNamesRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/two-names/{tokenId}")
    public ResponseDTO<TwoNamesDTO> getTwoNamesCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<TwoNamesDTO>(twoNamesService.getTwoNamesCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/two-names/{tokenId}")
    public void updateTwoNamesCertificateByTokenId(@RequestBody TwoNamesDTO namesRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        twoNamesService.updateTwoNamesCertificateByTokenId(namesRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/two-names")
    @ApiOperation("Add Two Named Person Registration")
    public void addTwoNamesRegistration(HttpServletResponse http, @RequestBody TwoNamesDTO namesRegistrationInfo)
            throws CustomSqlException {
        twoNamesService.addTwoNamesRegistration(namesRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/two-names/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        twoNamesService.addAllImages(image);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/two-names/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        twoNamesService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

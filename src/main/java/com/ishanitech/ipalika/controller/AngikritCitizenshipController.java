package com.ishanitech.ipalika.controller;

import com.ishanitech.ipalika.dto.AngikritCitizenshipDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.exception.EntityNotFoundException;
import com.ishanitech.ipalika.exception.FileStorageException;
import com.ishanitech.ipalika.service.AngikritCitizenshipService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RequestMapping("/e-governance")
@RestController
public class AngikritCitizenshipController {

    private final AngikritCitizenshipService angikritCitizenshipService;

    private AngikritCitizenshipController(AngikritCitizenshipService angikritCitizenshipService){this.angikritCitizenshipService = angikritCitizenshipService;}


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/angikrit")
    public ResponseDTO<List<AngikritCitizenshipDTO>> getCitizenshipRegistrations(HttpServletRequest request) {
        return new ResponseDTO<List<AngikritCitizenshipDTO>>(angikritCitizenshipService.getCitizenshipRegistrations(request));
    }


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/angikrit/{tokenId}")
    public ResponseDTO<AngikritCitizenshipDTO> getCitizenshipCertificateByTokenId(@PathVariable("tokenId") String tokenId)
            throws EntityNotFoundException {
        return new ResponseDTO<AngikritCitizenshipDTO>(angikritCitizenshipService.getCitizenshipCertificateByTokenId(tokenId));
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/angikrit/{tokenId}")
    public void updateCitizenshipCertificateByTokenId(@RequestBody AngikritCitizenshipDTO citizenshipRegistrationInfo, @PathVariable("tokenId") String tokenId)
            throws CustomSqlException {
        log.info("tokenId " + tokenId);
        angikritCitizenshipService.updateCitizenshipCertificateByTokenId(citizenshipRegistrationInfo, tokenId);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/angikrit")
    @ApiOperation("Add Angikrit Citizenship Registration")
    public void addCitizenshipRegistration(HttpServletResponse http, @RequestBody AngikritCitizenshipDTO citizenshipRegistrationInfo)
            throws CustomSqlException {
        angikritCitizenshipService.addCitizenshipRegistration(citizenshipRegistrationInfo);
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/angikrit/image")
    public void addAllImages(@RequestParam("picture") MultipartFile image) throws FileStorageException {
        angikritCitizenshipService.addAllImages(image);
    }


    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/angikrit/status/{tokenId}/{status}/{formInfoId}")
    public void updateCertificateStatusByTokenId(@PathVariable("tokenId") String tokenId,
                                                 @PathVariable("status") Integer status,
                                                 @PathVariable("formInfoId") Integer formInfoId)
            throws EntityNotFoundException {
        angikritCitizenshipService.updateCertificateStatus(tokenId, status, formInfoId);
    }
}

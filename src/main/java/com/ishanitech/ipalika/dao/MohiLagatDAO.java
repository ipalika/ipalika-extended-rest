package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.MohiLagatDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface MohiLagatDAO extends CommonDAO {

    @SqlQuery("SELECT * FROM egovernance_mohi_lagat")
    @RegisterBeanMapper(MohiLagatDTO.class)
    List<MohiLagatDTO> getMohiLagatRegistrations();

    @SqlUpdate("INSERT INTO egovernance_mohi_lagat (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, land_ownership_certificate, land_survey_map, tax_information_photo, landlord_proof_photo, field_book, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :landSurveyMap, :taxInformationPhoto, :landlordProofPhoto, :fieldBook, :email, :phone, :dateOfRegistration)")
    void addMohiLagatRegistration(@BindBean MohiLagatDTO mohiRegistrationInfo);
    @Transaction
    default void addNewEntity(MohiLagatDTO mohiRegistrationInfo) {
        addMohiLagatRegistration(mohiRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.MOHI_LAGAT.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_mohi_lagat WHERE token_id = :tokenId")
    @RegisterBeanMapper(MohiLagatDTO.class)
    MohiLagatDTO getMohiLagatCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_mohi_lagat SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateMohiLagatRegistrationByTokenId(@BindBean MohiLagatDTO mohiRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_mohi_lagat SET approved = 1 WHERE token_id =:tokenId")
    void approveMohiLagatRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_mohi_lagat SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.FourFortsDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface FourFortsDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_चार_कल्ला")
    @RegisterBeanMapper(FourFortsDTO.class)
    List<FourFortsDTO> getFourFortsRegistrations();

    @SqlUpdate("INSERT INTO egovernance_चार_कल्ला (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, land_ownership_certificate, land_area_survey_map, tax_information_photo, authorized_heir_copy, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :landAreaSurveyMap, :taxInformationPhoto, :authorizedHeirCopy, :email, :phone, :dateOfRegistration)")
    void addFourFortsRegistration(@BindBean FourFortsDTO fortRegistrationInfo);
    @Transaction
    default void addNewEntity(FourFortsDTO fortRegistrationInfo) {
        addFourFortsRegistration(fortRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.CHAR_KILLA.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_चार_कल्ला WHERE token_id = :tokenId")
    @RegisterBeanMapper(FourFortsDTO.class)
    FourFortsDTO getFourFortsCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_चार_कल्ला SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateFourFortsRegistrationByTokenId(@BindBean FourFortsDTO fortRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_चार_कल्ला SET approved = 1 WHERE token_id =:tokenId")
    void approveFourFortsRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_चार_कल्ला SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

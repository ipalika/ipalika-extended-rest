package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.SchoolOperationDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface SchoolOperationDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_school_operation")
    @RegisterBeanMapper(SchoolOperationDTO.class)
    List<SchoolOperationDTO> getSchoolRegistrations();

    @SqlUpdate("INSERT INTO egovernance_school_operation (token_id, registration_no, applicant_name, address, application_photo, school_registration_certificate, local_level_renewal_certificate, tax_information_photo, bahal_agreement_photo, bahal_tax_information, inspection_report, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :schoolRegistrationCertificate, :localLevelRenewalCertificate, :taxInformationPhoto, :bahalAgreementPhoto, :bahalTaxInformation, :inspectionReport, :email, :phone, :dateOfRegistration)")
    void addSchoolRegistration(@BindBean SchoolOperationDTO schoolRegistrationInfo);
    @Transaction
    default void addNewEntity(SchoolOperationDTO schoolRegistrationInfo) {
        addSchoolRegistration(schoolRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.SCHOOL_OPERATION.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_school_operation WHERE token_id = :tokenId")
    @RegisterBeanMapper(SchoolOperationDTO.class)
    SchoolOperationDTO getSchoolCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_school_operation SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateSchoolRegistrationByTokenId(@BindBean SchoolOperationDTO schoolRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_school_operation SET approved = 1 WHERE token_id =:tokenId")
    void approveSchoolRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_school_operation SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.ReconciliationPaperDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface ReconciliationPaperDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_reconciliation_paper")
    @RegisterBeanMapper(ReconciliationPaperDTO.class)
    List<ReconciliationPaperDTO> getReconciliationRegistrations();

    @SqlUpdate("INSERT INTO egovernance_reconciliation_paper (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo_person_1, citizenship_photo_person_2, citizenship_photo_person_3, citizenship_photo_person_4, other_document_1, other_document_2, other_document_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhotoPerson1, :citizenshipPhotoPerson2, :citizenshipPhotoPerson3, :citizenshipPhotoPerson4, :otherDocument1, :otherDocument2, :otherDocument3, :email, :phone, :dateOfRegistration)")
    void addReconciliationRegistration(@BindBean ReconciliationPaperDTO reconciliationRegistrationInfo);
    @Transaction
    default void addNewEntity(ReconciliationPaperDTO reconciliationRegistrationInfo) {
        addReconciliationRegistration(reconciliationRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.RECONCILIATION_PAPER.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_reconciliation_paper WHERE token_id = :tokenId")
    @RegisterBeanMapper(ReconciliationPaperDTO.class)
    ReconciliationPaperDTO getReconciliationCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_reconciliation_paper SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateReconciliationRegistrationByTokenId(@BindBean ReconciliationPaperDTO reconciliationRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_reconciliation_paper SET approved = 1 WHERE token_id =:tokenId")
    void approveReconciliationRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_reconciliation_paper SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.AdvertisementTaxDTO;
import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface AdvertisementTaxDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_advertisement_tax")
    @RegisterBeanMapper(AdvertisementTaxDTO.class)
    List<AdvertisementTaxDTO> getAdvertisementRegistrations();

    @SqlUpdate("INSERT INTO egovernance_advertisement_tax (token_id, registration_no, applicant_name, address, application_photo, organization_certified_document_1, organization_certified_document_2, local_level_business_tax, other_tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :organizationCertifiedDocument1, :organizationCertifiedDocument2, :localLevelBusinessTax, :otherTaxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addAdvertisementRegistration(@BindBean AdvertisementTaxDTO advertisementRegistrationInfo);
    @Transaction
    default void addNewEntity(AdvertisementTaxDTO advertisementRegistrationInfo) {
        addAdvertisementRegistration(advertisementRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.ADVERTISEMENT_TAX.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_advertisement_tax WHERE token_id = :tokenId")
    @RegisterBeanMapper(AdvertisementTaxDTO.class)
    AdvertisementTaxDTO getAdvertisementCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_advertisement_tax SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateAdvertisementRegistrationByTokenId(@BindBean AdvertisementTaxDTO advertisementRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_advertisement_tax SET approved = 1 WHERE token_id =:tokenId")
    void approveAdvertisementRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_advertisement_tax SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

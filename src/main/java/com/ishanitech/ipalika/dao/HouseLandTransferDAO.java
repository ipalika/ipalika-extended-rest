package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.HouseLandTransferDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface HouseLandTransferDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_house_land_transfer")
    @RegisterBeanMapper(HouseLandTransferDTO.class)
    List<HouseLandTransferDTO> getHouseLandRegistrations();

    @SqlUpdate("INSERT INTO egovernance_house_land_transfer (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, relationship_certificate_of_deceased_petitioner, land_ownership_certificate, sarjamin_witness_citizenship_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :relationshipCertificateOfDeceasedPetitioner, :landOwnershipCertificate, :sarjaminWitnessCitizenshipPhoto, :email, :phone, :dateOfRegistration)")
    void addHouseLandRegistration(@BindBean HouseLandTransferDTO houseRegistrationInfo);
    @Transaction
    default void addNewEntity(HouseLandTransferDTO houseRegistrationInfo) {
        addHouseLandRegistration(houseRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.HOUSE_LAND_TRANSFER.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_house_land_transfer WHERE token_id = :tokenId")
    @RegisterBeanMapper(HouseLandTransferDTO.class)
    HouseLandTransferDTO getHouseLandCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_house_land_transfer SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateHouseLandRegistrationByTokenId(@BindBean HouseLandTransferDTO houseRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_house_land_transfer SET approved = 1 WHERE token_id =:tokenId")
    void approveHouseLandRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_house_land_transfer SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

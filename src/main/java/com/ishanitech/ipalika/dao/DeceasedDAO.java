package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.DeceasedCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface DeceasedDAO extends CommonDAO{


    @SqlUpdate("INSERT INTO egovernance_relationship_deceased (token_id, ward_no, name, deceased_name, relationship, application_photo, citizenship_photo_applicant, pp_photo_applicant, citizenship_photo_deceased, pp_photo_deceased, relationship_photo, death_certificate, email, phone, date_of_registration) VALUES(:tokenId, :wardNo, :name, :deceasedName, :relationship, :applicationPhoto, :citizenshipPhotoApplicant, :ppPhotoApplicant, :citizenshipPhotoDeceased, :ppPhotoDeceased, :relationshipPhoto, :deathCertificate, :email, :phone, :dateOfRegistration)")
    void addRelationRegistration(@BindBean DeceasedCertificateDTO relationRegistrationInfo);
    @Transaction
    default void addNewEntity(DeceasedCertificateDTO relationRegistrationInfo) {
        addRelationRegistration(relationRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.RELATIONSHIP_DECEASED.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_relationship_deceased")
    @RegisterBeanMapper(DeceasedCertificateDTO.class)
    List<DeceasedCertificateDTO> getRelationRegistrations();

    @SqlQuery("SELECT * FROM egovernance_relationship_deceased WHERE token_id = :tokenId")
    @RegisterBeanMapper(DeceasedCertificateDTO.class)
    DeceasedCertificateDTO getRelationRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_relationship_deceased SET ward_no =:wardNo, name =:name, deceased_name =:deceasedName, email =:email, phone=:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId")
    void updateRelationRegistrationByTokenId(@BindBean DeceasedCertificateDTO relationRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_relationship_deceased SET approved = 1 WHERE token_id =:tokenId")
    void approveRelationRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_relationship_deceased SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import java.util.List;

import com.ishanitech.ipalika.dto.FormInfoDTO;
import com.ishanitech.ipalika.model.Answer;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.RelationCertificateDTO;
import org.jdbi.v3.sqlobject.transaction.Transaction;

public interface EGovernanceDAO extends CommonDAO{



	@SqlQuery("SELECT id FROM egovernance_birth_certificate")
	List<String> getAllFilledIds();
	
	@SqlQuery("SELECT * FROM egovernance_birth_certificate")
	@RegisterBeanMapper(BirthCertificateDTO.class)
	List<BirthCertificateDTO> getBirthRegistrations();

	@SqlUpdate("INSERT INTO egovernance_birth_certificate (token_id, lg_province, lg_district, lg_municipality, lg_ward, tole, name_eng, name_nep, gender, dob_ad, dob_bs, informant_name_eng, informant_name_nep, informant_id_eng, informant_id_nep, dor_ad, dor_bs, father_name_eng, father_name_nep, father_id_eng, father_id_nep, mother_name_eng, mother_name_nep, mother_id_eng, mother_id_nep, permanent_address_eng, permanent_address_nep, birth_place_eng, birth_place_nep, birth_place_certificate, registrar_name_eng, registrar_name_nep, registrar_signature, registrar_signature_date, ward_secretary_name, ward_secretary_signature, status, email, phone, grandfather_name_eng, grandfather_name_nep, father_id_img, mother_id_img) VALUES(:tokenId, :lgProvince, :lgDistrict, :lgMunicipality, :lgWard, :tole, :nameEng, :nameNep, :gender, :dobAd, :dobBs, :informantNameEng, :informantNameNep, :informantIdEng, :informantIdNep, :dorAd, :dorBs, :fatherNameEng, :fatherNameNep, :fatherIdEng, :fatherIdNep, :motherNameEng, :motherNameNep, :motherIdEng, :motherIdNep, :permanentAddressEng, :permanentAddressNep, :birthPlaceEng, :birthPlaceNep, :birthPlaceCertificate, :registrarNameEng, :registrarNameNep, :registrarSignature, :registrarSignatureDate, :wardSecretaryName, :wardSecretarySignature,'1', :email, :phone, :grandfatherNameEng, :grandfatherNameNep, :fatherIdImg, :motherIdImg)")
	void addBirthRegistration(@BindBean BirthCertificateDTO birthRegistrationInfo);

	@SqlUpdate("UPDATE registration_report SET unverified = unverified+1 WHERE form_info_id = :id ")
	void updateRegistrationReport(int id);
	@Transaction
	default void addNewBirth(BirthCertificateDTO birthRegistrationInfo) {
		addBirthRegistration(birthRegistrationInfo);
		updateRegistrationReportForm(1, FormInfoUtil.BIRTH_CERTIFICATE.getFormId());
	}

	@SqlQuery("SELECT * FROM egovernance_birth_certificate WHERE token_id = :tokenId")
	@RegisterBeanMapper(BirthCertificateDTO.class)
	BirthCertificateDTO getBirthRegistrationByTokenId(@Bind("tokenId") String tokenId);

	@SqlUpdate("UPDATE egovernance_birth_certificate SET lg_province =:lgProvince, lg_district =:lgDistrict, lg_municipality =:lgMunicipality, lg_ward =:lgWard, tole =:tole, name_eng =:nameEng, name_nep =:nameNep, gender =:gender, dob_ad =:dobAd, dob_bs =:dobBs, informant_name_eng=:informantNameEng, informant_name_nep =:informantNameNep, informant_id_eng =:informantIdEng, informant_id_nep =:informantIdNep, dor_ad =:dorAd , dor_bs =:dorBs, father_name_eng =:fatherNameEng, father_name_nep =:fatherNameNep, father_id_eng =:fatherIdEng, father_id_nep =:fatherIdNep, mother_name_eng =:motherNameEng, mother_name_nep =:motherNameNep, mother_id_eng =:motherIdEng, mother_id_nep =:motherIdNep, permanent_address_eng =:permanentAddressEng, permanent_address_nep =:permanentAddressNep, birth_place_eng =:birthPlaceEng, birth_place_nep =:birthPlaceNep, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature, email =:email, phone =:phone, grandfather_name_eng =:grandfatherNameEng, grandfather_name_nep =:grandfatherNameNep WHERE token_id =:tokenId" )
	void updateBirthRegistrationByTokenId(@BindBean BirthCertificateDTO birthRegistrationInfo, @Bind("tokenId") String tokenId);

	@SqlUpdate("UPDATE egovernance_birth_certificate SET approved = 1 WHERE token_id =:tokenId")
	void approveBirthRegistration(@Bind("tokenId") String tokenId);

	
	@SqlQuery("SELECT * FROM egovernance_relationship_living")
	@RegisterBeanMapper(RelationCertificateDTO.class)
	List<RelationCertificateDTO> getRelationRegistrations();

	@SqlUpdate("INSERT INTO egovernance_relationship_living (token_id, ward_no, name, relative_name, relationship, application_photo, citizenship_photo_applicant, pp_photo_applicant, citizenship_photo_relative, pp_photo_relative, relationship_photo, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :wardNo, :name, :relativeName, :relationship, :applicationPhoto, :citizenshipPhotoApplicant, :ppPhotoApplicant, :citizenshipPhotoRelative, :ppPhotoRelative, :relationshipPhoto, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
	void addRelationRegistration(@BindBean RelationCertificateDTO relationRegistrationInfo);
	@Transaction
	default void addNewRelation(RelationCertificateDTO relationRegistrationInfo) {
		addRelationRegistration(relationRegistrationInfo);
		updateRegistrationReportForm(1, FormInfoUtil.RELATIONSHIP_LIVING.getFormId());
	}
	@SqlQuery("SELECT * FROM egovernance_relationship_living WHERE token_id = :tokenId")
	@RegisterBeanMapper(RelationCertificateDTO.class)
	RelationCertificateDTO getRelationRegistrationByTokenId(@Bind("tokenId") String tokenId);

//	@SqlUpdate("UPDATE egovernance_relationship_living SET ward_no =:wardNo, name =:name, relative_name =:relativeName, application_photo=:applicationPhoto, citizenship_photo_applicant =:citizenshipPhotoApplicant, pp_photo_applicant =:ppPhotoApplicant, citizenship_photo_relative =:citizenshipPhotoRelative, pp_photo_relative =:ppPhotoRelative, relationship_photo =:relationshipPhoto, tax_information_photo =:taxInformationPhoto, email =:email, phone=:phone WHERE token_id =:tokenId")
	@SqlUpdate("UPDATE egovernance_relationship_living SET ward_no =:wardNo, name =:name, relative_name =:relativeName, email =:email, phone=:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId")
	void updateRelationRegistrationByTokenId(@BindBean RelationCertificateDTO relationRegistrationInfo, @Bind("tokenId") String tokenId);

	@SqlUpdate("UPDATE egovernance_relationship_living SET approved = 1 WHERE token_id =:tokenId")
	void approveRelationRegistration(@Bind("tokenId") String tokenId);

	@SqlUpdate("UPDATE egovernance_birth_certificate SET status =:status WHERE token_id =:tokenId")
	void updateBirthRegistrationStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

	@SqlUpdate("UPDATE egovernance_relationship_living SET status =:status WHERE token_id =:tokenId")
	void updateRelationRegistrationStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

	@Transaction
	default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
		updateBirthRegistrationStatus(tokenId, status);
		updateRegistrationReportForm(status, formInfoId) ;
	}

	@SqlQuery("SELECT * FROM form_info")
	@RegisterBeanMapper(FormInfoDTO.class)
	List<FormInfoDTO> getAllFormsInfoData();

	@Transaction
	default	void updateRelationStatus(String tokenId, Integer status, Integer formInfoId){
		updateRelationRegistrationStatus(tokenId, status);
		updateRegistrationReportForm(status, formInfoId) ;
	}

}

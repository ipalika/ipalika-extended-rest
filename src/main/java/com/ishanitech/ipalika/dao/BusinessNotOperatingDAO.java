package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.BusinessNotOperatingDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface BusinessNotOperatingDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_business_not_operating")
    @RegisterBeanMapper(BusinessNotOperatingDTO.class)
    List<BusinessNotOperatingDTO> getBusinessRegistrations();

    @SqlUpdate("INSERT INTO egovernance_business_not_operating (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, business_registration_certificate, tax_information_photo, onsite_report, ghar_bahal_agreement, sarjamin_muchulka, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :businessRegistrationCertificate, :taxInformationPhoto, :onsiteReport, :gharBahalAgreement, :sarjaminMuchulka, :email, :phone, :dateOfRegistration)")
    void addBusinessRegistration(@BindBean BusinessNotOperatingDTO businessRegistrationInfo);
    @Transaction
    default void addNewEntity(BusinessNotOperatingDTO businessRegistrationInfo) {
        addBusinessRegistration(businessRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.BUSINESS_NOT_OPERATING.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_business_not_operating WHERE token_id = :tokenId")
    @RegisterBeanMapper(BusinessNotOperatingDTO.class)
    BusinessNotOperatingDTO getBusinessCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_not_operating SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateBusinessRegistrationByTokenId(@BindBean BusinessNotOperatingDTO businessRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_not_operating SET approved = 1 WHERE token_id =:tokenId")
    void approveBusinessRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_not_operating SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

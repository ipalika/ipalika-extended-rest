package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BatoKayamDTO;
import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface BatoKayamDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_बाटो_कायम")
    @RegisterBeanMapper(BatoKayamDTO.class)
    List<BatoKayamDTO> getBatoKayamRegistrations();

    @SqlUpdate("INSERT INTO egovernance_बाटो_कायम (token_id, registration_no, ward_no, applicant_name, address, application_photo, land_ownership_certificate, survey_map, tax_information_photo, land_owner_approval_letter, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :landOwnershipCertificate, :surveyMap, :taxInformationPhoto, :landOwnerApprovalLetter, :email, :phone, :dateOfRegistration)")
    void addBatoKayamRegistration(@BindBean BatoKayamDTO batoRegistrationInfo);
    @Transaction
    default void addNewEntity(BatoKayamDTO batoRegistrationInfo) {
        addBatoKayamRegistration(batoRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.BATO_KAYAM.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_बाटो_कायम WHERE token_id = :tokenId")
    @RegisterBeanMapper(BatoKayamDTO.class)
    BatoKayamDTO getBatoKayamCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_बाटो_कायम SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateBatoKayamRegistrationByTokenId(@BindBean BatoKayamDTO batoRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_बाटो_कायम SET approved = 1 WHERE token_id =:tokenId")
    void approveBatoKayamRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_बाटो_कायम SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.WaterPipelineDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface WaterPipelineDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_pipeline_connection")
    @RegisterBeanMapper(WaterPipelineDTO.class)
    List<WaterPipelineDTO> getPipelineRegistrations();

    @SqlUpdate("INSERT INTO egovernance_pipeline_connection (token_id, registration_no, ward_no, name, address, gender, application_photo, citizenship_photo, land_ownership_certificate, proof_of_map_pass, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :gender, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :proofOfMapPass, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addPipelineRegistration(@BindBean WaterPipelineDTO pipelineRegistrationInfo);
    @Transaction
    default void addNewEntity(WaterPipelineDTO pipelineRegistrationInfo) {
        addPipelineRegistration(pipelineRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.PIPELINE_CONNECTION.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_pipeline_connection WHERE token_id = :tokenId")
    @RegisterBeanMapper(WaterPipelineDTO.class)
    WaterPipelineDTO getPipelineRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_pipeline_connection SET ward_no =:wardNo, name =:name, address =:address, gender =:gender, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updatePipelineRegistrationByTokenId(@BindBean WaterPipelineDTO pipelineRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_pipeline_connection SET approved = 1 WHERE token_id =:tokenId")
    void approvePipelineRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_pipeline_connection SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

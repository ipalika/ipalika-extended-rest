package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.MarriageVerifyDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface MarriageVerifyDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_marriage_verification")
    @RegisterBeanMapper(MarriageVerifyDTO.class)
    List<MarriageVerifyDTO> getMarriageVerifications();

    @SqlUpdate("INSERT INTO egovernance_marriage_verification (token_id, registration_no, ward_no, husband_name, wife_name, husband_citizenship_photo, husband_citizenship_id, wife_citizenship_photo, wife_citizenship_id, migration_certificate_photo, tax_information_photo, marriage_certificate_photo, email, phone, date_of_verification) VALUES(:tokenId, :registrationNo, :wardNo, :husbandName, :wifeName, :husbandCitizenshipPhoto, :husbandCitizenshipId, :wifeCitizenshipPhoto, :wifeCitizenshipId, :migrationCertificatePhoto, :taxInformationPhoto, :marriageCertificatePhoto, :email, :phone, :dateOfVerification)")
    void addMarriageVerification(@BindBean MarriageVerifyDTO marriageVerificationInfo);
    @Transaction
    default void addNewEntity(MarriageVerifyDTO marriageVerificationInfo) {
        addMarriageVerification(marriageVerificationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.MARRIAGE_VERIFICATION.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_marriage_verification WHERE token_id = :tokenId")
    @RegisterBeanMapper(MarriageVerifyDTO.class)
    MarriageVerifyDTO getMarriageVerificationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_marriage_verification SET ward_no =:wardNo, husband_name =:husbandName, wife_name =:wifeName, husband_citizenship_id =:husbandCitizenshipId, wife_citizenship_id =:wifeCitizenshipId, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId")
    void updateMarriageVerificationByTokenId(@BindBean MarriageVerifyDTO marriageVerificationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_marriage_verification SET approved = 1 WHERE token_id =:tokenId")
    void approveMarriageVerification(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_marriage_verification SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

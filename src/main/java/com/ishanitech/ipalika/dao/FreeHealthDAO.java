package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.FreeHealthDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface FreeHealthDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_free_healthcare")
    @RegisterBeanMapper(FreeHealthDTO.class)
    List<FreeHealthDTO> getFreeHealthRegistrations();

    @SqlUpdate("INSERT INTO egovernance_free_healthcare (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, document_proving_poverty, other_document_1, other_document_2, other_document_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :documentProvingPoverty, :otherDocument1, :otherDocument2, :otherDocument3, :email, :phone, :dateOfRegistration)")
    void addFreeHealthRegistration(@BindBean FreeHealthDTO healthRegistrationInfo);
    @Transaction
    default void addNewEntity(FreeHealthDTO healthRegistrationInfo) {
        addFreeHealthRegistration(healthRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.FREE_HEALTHCARE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_free_healthcare WHERE token_id = :tokenId")
    @RegisterBeanMapper(FreeHealthDTO.class)
    FreeHealthDTO getFreeHealthCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_free_healthcare SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateFreeHealthRegistrationByTokenId(@BindBean FreeHealthDTO healthRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_free_healthcare SET approved = 1 WHERE token_id =:tokenId")
    void approveFreeHealthRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_free_healthcare SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

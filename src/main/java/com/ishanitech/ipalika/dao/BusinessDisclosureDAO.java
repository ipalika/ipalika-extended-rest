package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.BusinessDisclosureDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface BusinessDisclosureDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_business_closure")
    @RegisterBeanMapper(BusinessDisclosureDTO.class)
    List<BusinessDisclosureDTO> getBusinessRegistrations();

    @SqlUpdate("INSERT INTO egovernance_business_closure (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, business_renewal_certificate, ghar_bahal_agreement, onsite_report, identity_document_if_foreigner, letter_from_embassy, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :businessRenewalCertificate, :gharBahalAgreement, :onsiteReport, :identityDocumentIfForeigner, :letterFromEmbassy, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addBusinessRegistration(@BindBean BusinessDisclosureDTO businessRegistrationInfo);
    @Transaction
    default void addNewEntity(BusinessDisclosureDTO businessRegistrationInfo) {
        addBusinessRegistration(businessRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.BUSINESS_CLOSURE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_business_closure WHERE token_id = :tokenId")
    @RegisterBeanMapper(BusinessDisclosureDTO.class)
    BusinessDisclosureDTO getBusinessCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_closure SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateBusinessRegistrationByTokenId(@BindBean BusinessDisclosureDTO businessRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_closure SET approved = 1 WHERE token_id =:tokenId")
    void approveBusinessRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_closure SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

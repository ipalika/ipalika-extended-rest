package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.RightfulPersonDTO;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface RightfulPersonDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_rightful_person")
    @RegisterBeanMapper(RightfulPersonDTO.class)
    List<RightfulPersonDTO> getRightfulRegistrations();

    @SqlUpdate("INSERT INTO egovernance_rightful_person (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, relationship_proof_certificate, onsite_sarjamin, tax_information_photo, sarjamin_muchulka, proof_document_1, proof_document_2, proof_document_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :relationshipProofCertificate, :onsiteSarjamin, :taxInformationPhoto, :sarjaminMuchulka, :proofDocument1, :proofDocument2, :proofDocument3, :email, :phone, :dateOfRegistration)")
    void addRightfulRegistration(@BindBean RightfulPersonDTO rightfulRegistrationInfo);
    @Transaction
    default void addNewEntity(RightfulPersonDTO rightfulRegistrationInfo) {
        addRightfulRegistration(rightfulRegistrationInfo);
        updateRegistrationReportForm(1,1);
    }
    @SqlQuery("SELECT * FROM egovernance_rightful_person WHERE token_id = :tokenId")
    @RegisterBeanMapper(RightfulPersonDTO.class)
    RightfulPersonDTO getRightfulCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_rightful_person SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateRightfulRegistrationByTokenId(@BindBean RightfulPersonDTO rightfulRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_rightful_person SET approved = 1 WHERE token_id =:tokenId")
    void approveRightfulRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_rightful_person SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.ScholarshipCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface ScholarshipDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_scholarship_certificate")
    @RegisterBeanMapper(ScholarshipCertificateDTO.class)
    List<ScholarshipCertificateDTO> getScholarshipRegistrations();

    @SqlUpdate("INSERT INTO egovernance_scholarship_certificate (token_id, registration_no, ward_no, name, address, gender, application_photo, citizenship_photo, edu_qualification_certificate, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :gender, :applicationPhoto, :citizenshipPhoto, :eduQualificationCertificate, :email, :phone, :dateOfRegistration)")
    void addScholarshipRegistration(@BindBean ScholarshipCertificateDTO scholarshipRegistrationInfo);
    @Transaction
    default void addNewEntity(ScholarshipCertificateDTO scholarshipRegistrationInfo) {
        addScholarshipRegistration(scholarshipRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.SCHOLARSHIP_CERTIFICATE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_scholarship_certificate WHERE token_id = :tokenId")
    @RegisterBeanMapper(ScholarshipCertificateDTO.class)
    ScholarshipCertificateDTO getScholarshipRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_scholarship_certificate SET ward_no =:wardNo, name =:name, address =:address, gender =:gender, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateScholarshipRegistrationByTokenId(@BindBean ScholarshipCertificateDTO scholarshipRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_scholarship_certificate SET approved = 1 WHERE token_id =:tokenId")
    void approveScholarshipRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_scholarship_certificate SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

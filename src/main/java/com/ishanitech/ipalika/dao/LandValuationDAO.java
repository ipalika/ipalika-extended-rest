package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.LandValuationDTO;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface LandValuationDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_land_valuation")
    @RegisterBeanMapper(LandValuationDTO.class)
    List<LandValuationDTO> getLandRegistrations();

    @SqlUpdate("INSERT INTO egovernance_land_valuation (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, land_ownership_certificate, current_land_price, sarjamin_muchulka, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :currentLandPrice, :sarjaminMuchulka, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addLandRegistration(@BindBean LandValuationDTO landRegistrationInfo);
    @Transaction
    default void addNewEntity(LandValuationDTO landRegistrationInfo) {
        addLandRegistration(landRegistrationInfo);
        updateRegistrationReportForm(1,1);
    }
    @SqlQuery("SELECT * FROM egovernance_land_valuation WHERE token_id = :tokenId")
    @RegisterBeanMapper(LandValuationDTO.class)
    LandValuationDTO getLandCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_valuation SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, current_land_price =:currentLandPrice, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateLandRegistrationByTokenId(@BindBean LandValuationDTO landRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_valuation SET approved = 1 WHERE token_id =:tokenId")
    void approveLandRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_valuation SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

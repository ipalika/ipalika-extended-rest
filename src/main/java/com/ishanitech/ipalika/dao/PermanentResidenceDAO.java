package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.PermanentResidenceDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PermanentResidenceDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_permanent_residence")
    @RegisterBeanMapper(PermanentResidenceDTO.class)
    List<PermanentResidenceDTO> getPermanentResidenceRegistrations();

    @SqlUpdate("INSERT INTO egovernance_permanent_residence (token_id, registration_no, ward_no, name, address, gender, application_photo, citizenship_photo, land_ownership_certificate, resettlement_certificate, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :gender, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :resettlementCertificate, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addPermanentResidenceRegistration(@BindBean PermanentResidenceDTO permanentResidenceInfo);
    @Transaction
    default void addNewEntity(PermanentResidenceDTO permanentResidenceInfo) {
        addPermanentResidenceRegistration(permanentResidenceInfo);
        updateRegistrationReportForm(1, FormInfoUtil.PERMANENT_RESIDENCE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_permanent_residence WHERE token_id = :tokenId")
    @RegisterBeanMapper(PermanentResidenceDTO.class)
    PermanentResidenceDTO getPermanentResidenceCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_permanent_residence SET ward_no =:wardNo, name =:name, address =:address, gender =:gender, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updatePermanentResidenceCertificateByTokenId(@BindBean PermanentResidenceDTO permanentResidenceInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_permanent_residence SET approved = 1 WHERE token_id =:tokenId")
    void approvePermanentResidenceCertificate(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_permanent_residence SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

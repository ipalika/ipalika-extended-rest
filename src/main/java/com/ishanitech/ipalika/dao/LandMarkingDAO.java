package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.LandMarkingDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface LandMarkingDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_land_marking")
    @RegisterBeanMapper(LandMarkingDTO.class)
    List<LandMarkingDTO> getLandMarkingRegistrations();

    @SqlUpdate("INSERT INTO egovernance_land_marking (token_id, registration_no, applicant_name, address, application_photo, concerned_office_letter, technical_report, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :concernedOfficeLetter, :technicalReport, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addLandMarkingRegistration(@BindBean LandMarkingDTO landmarkRegistrationInfo);
    @Transaction
    default void addNewEntity(LandMarkingDTO landmarkRegistrationInfo) {
        addLandMarkingRegistration(landmarkRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.LAND_MARKING.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_land_marking WHERE token_id = :tokenId")
    @RegisterBeanMapper(LandMarkingDTO.class)
    LandMarkingDTO getLandMarkingCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_marking SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateLandMarkingRegistrationByTokenId(@BindBean LandMarkingDTO landmarkRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_marking SET approved = 1 WHERE token_id =:tokenId")
    void approveLandMarkingRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_marking SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

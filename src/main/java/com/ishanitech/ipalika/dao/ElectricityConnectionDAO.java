package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.ElectricityConnectionDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.Define;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface ElectricityConnectionDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_electricity_connection")
    @RegisterBeanMapper(ElectricityConnectionDTO.class)
    List<ElectricityConnectionDTO> getElectricityRegistrations();

    @SqlUpdate("INSERT INTO egovernance_electricity_connection (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, land_ownership_certificate, source_of_right_document, proof_of_map_pass, tax_information_photo, other_document_1, other_document_2, other_document_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :sourceOfRightDocument, :proofOfMapPass, :taxInformationPhoto, :otherDocument1, :otherDocument2, :otherDocument3, :email, :phone, :dateOfRegistration)")
    void addElectricityRegistration(@BindBean ElectricityConnectionDTO electricityRegistrationInfo);
    @Transaction
    default void addNewEntity(ElectricityConnectionDTO electricityRegistrationInfo) {
        addElectricityRegistration(electricityRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.ELECTRICITY_CONNECTION.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_electricity_connection WHERE token_id = :tokenId")
    @RegisterBeanMapper(ElectricityConnectionDTO.class)
    ElectricityConnectionDTO getElectricityCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_electricity_connection SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, application_photo =:applicationPhoto, citizenship_photo =:citizenshipPhoto, land_ownership_certificate =:landOwnershipCertificate, source_of_right_document =:sourceOfRightDocument, proof_of_map_pass =:proofOfMapPass, tax_information_photo =:taxInformationPhoto, other_document_1 =:otherDocument1, other_document_2 =:otherDocument2, other_document_3 =:otherDocument3 WHERE token_id =:tokenId" )
    void updateElectricityRegistrationByTokenId(@BindBean ElectricityConnectionDTO electricityRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_electricity_connection SET approved = 1 WHERE token_id =:tokenId")
    int approveElectricityRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_electricity_connection SET registration_no =:regNo WHERE token_id =:tokenId")
    void updateRegistrationNumber(@Bind("regNo") String regNo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_electricity_connection SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId, String regNo){
        System.out.println("regNo=====================> " + regNo);
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
        if(status == 3){
            updateRegistrationNumber(regNo, tokenId);
        }
    }

    @SqlUpdate("UPDATE egovernance_electricity_connection SET <column> = :value  ")

    void updateAuthorityColumn(@Define String column, String value);

//    void updateFormMetaData(String f);
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.StrongEconomyDTO;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface StrongEconomyDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_strong_economy")
    @RegisterBeanMapper(StrongEconomyDTO.class)
    List<StrongEconomyDTO> getStrongEconomyRegistrations();

    @SqlUpdate("INSERT INTO egovernance_strong_economy (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, land_ownership_certificate, income_source_document, tax_information_photo, sarjamin_muchulka, other_document_1, other_document_2, other_document_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :incomeSourceDocument, :taxInformationPhoto, :sarjaminMuchulka, :otherDocument1, :otherDocument2, :otherDocument3, :email, :phone, :dateOfRegistration)")
    void addStrongEconomyRegistration(@BindBean StrongEconomyDTO economyRegistrationInfo);
    @Transaction
    default void addNewEntity(StrongEconomyDTO economyRegistrationInfo) {
        addStrongEconomyRegistration(economyRegistrationInfo);
        updateRegistrationReportForm(1,1);
    }
    @SqlQuery("SELECT * FROM egovernance_strong_economy WHERE token_id = :tokenId")
    @RegisterBeanMapper(StrongEconomyDTO.class)
    StrongEconomyDTO getStrongEconomyCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_strong_economy SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateStrongEconomyRegistrationByTokenId(@BindBean StrongEconomyDTO economyRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_strong_economy SET approved = 1 WHERE token_id =:tokenId")
    void approveStrongEconomyRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_strong_economy SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.InstitutionRegisterDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface InstitutionRegisterDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_institution_register")
    @RegisterBeanMapper(InstitutionRegisterDTO.class)
    List<InstitutionRegisterDTO> getInstitutionRegistrations();

    @SqlUpdate("INSERT INTO egovernance_institution_register (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, copy_of_legislation_or_rules, bahal_agreement_photo, bahal_tax_information, land_ownership_certificate, map_pass_certificate, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :copyOfLegislationOrRules, :bahalAgreementPhoto, :bahalTaxInformation, :landOwnershipCertificate, :mapPassCertificate, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addInstitutionRegistration(@BindBean InstitutionRegisterDTO institutionRegistrationInfo);
    @Transaction
    default void addNewEntity(InstitutionRegisterDTO institutionRegistrationInfo) {
        addInstitutionRegistration(institutionRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.INSTITUTION_REGISTER.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_institution_register WHERE token_id = :tokenId")
    @RegisterBeanMapper(InstitutionRegisterDTO.class)
    InstitutionRegisterDTO getInstitutionCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_institution_register SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateInstitutionRegistrationByTokenId(@BindBean InstitutionRegisterDTO institutionRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_institution_register SET approved = 1 WHERE token_id =:tokenId")
    void approveInstitutionRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_institution_register SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.MinorCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface MinorDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_minor_certificate")
    @RegisterBeanMapper(MinorCertificateDTO.class)
    List<MinorCertificateDTO> getMinorRegistrations();

    @SqlUpdate("INSERT INTO egovernance_minor_certificate (token_id, registration_no, ward_no, name, address, gender, application_photo, birth_certificate, father_citizenship_photo, mother_citizenship_photo, pp_photo, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :gender, :applicationPhoto, :birthCertificate, :fatherCitizenshipPhoto, :motherCitizenshipPhoto, :ppPhoto, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addMinorRegistration(@BindBean MinorCertificateDTO minorRegistrationInfo);
    @Transaction
    default void addNewEntity(MinorCertificateDTO minorRegistrationInfo) {
        addMinorRegistration(minorRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.MINOR_CERTIFICATE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_minor_certificate WHERE token_id = :tokenId")
    @RegisterBeanMapper(MinorCertificateDTO.class)
    MinorCertificateDTO getMinorRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_minor_certificate SET ward_no =:wardNo, name =:name, address =:address, gender =:gender, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateMinorRegistrationByTokenId(@BindBean MinorCertificateDTO minorRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_minor_certificate SET approved = 1 WHERE token_id =:tokenId")
    void approveMinorRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_minor_certificate SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

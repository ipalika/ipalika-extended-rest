package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.MarriageCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface MarriageDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_marriage_certificate")
    @RegisterBeanMapper(MarriageCertificateDTO.class)
    List<MarriageCertificateDTO> getMarriageRegistration();


    @SqlUpdate("INSERT INTO egovernance_marriage_certificate (token_id, marriage_application_photo, husband_name, husband_citizenship_photo, husband_citizenship_id, wife_name, wife_citizenship_photo, wife_citizenship_id, bride_family_citizenship_photo, bride_family_citizenship_id, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :marriageApplicationPhoto, :husbandName, :husbandCitizenshipPhoto, :husbandCitizenshipId, :wifeName, :wifeCitizenshipPhoto, :wifeCitizenshipId, :brideFamilyCitizenshipPhoto, :brideFamilyCitizenshipId, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addMarriageRegistration(@BindBean MarriageCertificateDTO marriageRegistrationInfo);
    @Transaction
    default void addNewEntity(MarriageCertificateDTO marriageRegistrationInfo) {
        addMarriageRegistration(marriageRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.MARRIAGE_CERTIFICATE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_marriage_certificate WHERE token_id = :tokenId")
    @RegisterBeanMapper(MarriageCertificateDTO.class)
    MarriageCertificateDTO getMarriageRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_marriage_certificate SET husband_name =:husbandName, husband_citizenship_id =:husbandCitizenshipId, wife_name =:wifeName, wife_citizenship_id =:wifeCitizenshipId, bride_family_citizenship_id =:brideFamilyCitizenshipId, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId")
    void updateMarriageRegistrationByTokenId(@BindBean MarriageCertificateDTO marriageRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_marriage_certificate SET approved = 1 WHERE token_id =:tokenId")
    void approveMarriageRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_marriage_certificate SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }

}

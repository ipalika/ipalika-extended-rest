package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.HouseBuildingDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface HouseBuildingDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_house_building")
    @RegisterBeanMapper(HouseBuildingDTO.class)
    List<HouseBuildingDTO> getHouseBuildingRegistrations();

    @SqlUpdate("INSERT INTO egovernance_house_building (token_id, registration_no, ward_no, name, address, application_photo, citizenship_photo, building_map_pass, construction_completion_certificate, land_ownership_certificate, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :applicationPhoto, :citizenshipPhoto, :buildingMapPass, :constructionCompletionCertificate, :landOwnershipCertificate, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addHouseBuildingRegistration(@BindBean HouseBuildingDTO houseRegistrationInfo);
    @Transaction
    default void addNewEntity(HouseBuildingDTO houseRegistrationInfo) {
        addHouseBuildingRegistration(houseRegistrationInfo);
        updateRegistrationReportForm(1, 1);
    }
    @SqlQuery("SELECT * FROM egovernance_house_building WHERE token_id = :tokenId")
    @RegisterBeanMapper(HouseBuildingDTO.class)
    HouseBuildingDTO getHouseBuildingCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_house_building SET ward_no =:wardNo, name =:name, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateHouseBuildingRegistrationByTokenId(@BindBean HouseBuildingDTO houseRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_house_building SET approved = 1 WHERE token_id =:tokenId")
    void approveHouseBuildingRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_house_building SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.DisabilityCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface DisabilityDAO extends CommonDAO{


    @SqlQuery("SELECT * FROM egovernance_disabled_certificate")
    @RegisterBeanMapper(DisabilityCertificateDTO.class)
    List<DisabilityCertificateDTO> getDisabilityRegistrations();

    @SqlUpdate("INSERT INTO egovernance_disabled_certificate (token_id, registration_no, ward_no, name, address, gender, application_photo, disability_type, disability_certificate_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :gender, :applicationPhoto, :disabilityType, :disabilityCertificatePhoto, :email, :phone, :dateOfRegistration)")
    void addDisabilityRegistration(@BindBean DisabilityCertificateDTO disabilityRegistrationInfo);
    @Transaction
    default void addNewEntity(DisabilityCertificateDTO disabilityRegistrationInfo) {
        addDisabilityRegistration(disabilityRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.DISABLED_CERTIFICATE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_disabled_certificate WHERE token_id = :tokenId")
    @RegisterBeanMapper(DisabilityCertificateDTO.class)
    DisabilityCertificateDTO getDisabilityRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_disabled_certificate SET ward_no =:wardNo, name =:name, address =:address, gender =:gender, disability_type =:disabilityType, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateDisabilityRegistrationByTokenId(@BindBean DisabilityCertificateDTO disabilityRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_disabled_certificate SET approved = 1 WHERE token_id =:tokenId")
    void approveDisabilityRegistration(@Bind("tokenId") String tokenId);

    @SqlQuery("SELECT differently_abled_nep FROM differently_abled")
    List<String> getListofDifferentlyAbled();

    @SqlUpdate("UPDATE egovernance_disabled_certificate SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

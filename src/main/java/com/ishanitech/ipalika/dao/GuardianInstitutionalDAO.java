package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.GuardianInstitutionalDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface GuardianInstitutionalDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_संरक्षक_संस्थागत")
    @RegisterBeanMapper(GuardianInstitutionalDTO.class)
    List<GuardianInstitutionalDTO> getGuardianRegistrations();

    @SqlUpdate("INSERT INTO egovernance_संरक्षक_संस्थागत (token_id, registration_no, applicant_name, address, application_photo, institution_renewal_certificate, copy_of_legislation, copy_of_rules, tax_information_photo, bahal_agreement_photo, bahal_tax_information, sarjamin_muchulka, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :institutionRenewalCertificate, :copyOfLegislation, :copyOfRules, :taxInformationPhoto, :bahalAgreementPhoto, :bahalTaxInformation, :sarjaminMuchulka, :email, :phone, :dateOfRegistration)")
    void addGuardianRegistration(@BindBean GuardianInstitutionalDTO guardianRegistrationInfo);
    @Transaction
    default void addNewEntity(GuardianInstitutionalDTO guardianRegistrationInfo) {
        addGuardianRegistration(guardianRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.GUARDIAN_INSTITUTIONAL.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_संरक्षक_संस्थागत WHERE token_id = :tokenId")
    @RegisterBeanMapper(GuardianInstitutionalDTO.class)
    GuardianInstitutionalDTO getGuardianCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_संरक्षक_संस्थागत SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateGuardianRegistrationByTokenId(@BindBean GuardianInstitutionalDTO guardianRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_संरक्षक_संस्थागत SET approved = 1 WHERE token_id =:tokenId")
    void approveGuardianRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_संरक्षक_संस्थागत SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.GharPatalDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface GharPatalDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_ghar_patal")
    @RegisterBeanMapper(GharPatalDTO.class)
    List<GharPatalDTO> getGharPatalVerifications();

    @SqlUpdate("INSERT INTO egovernance_ghar_patal (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, house_map, map_pass_certificate, onsite_inspection_report, sarjamin_muchulka, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :houseMap, :mapPassCertificate, :onsiteInspectionReport, :sarjaminMuchulka, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addGharPatalVerification(@BindBean GharPatalDTO gharVerificationInfo);
    @Transaction
    default void addNewEntity(GharPatalDTO gharVerificationInfo) {
        addGharPatalVerification(gharVerificationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.GHAR_PATAL.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_ghar_patal WHERE token_id = :tokenId")
    @RegisterBeanMapper(GharPatalDTO.class)
    GharPatalDTO getGharPatalVerificationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_ghar_patal SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateGharPatalVerificationByTokenId(@BindBean GharPatalDTO gharVerificationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_ghar_patal SET approved = 1 WHERE token_id =:tokenId")
    void approveGharPatalVerification(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_ghar_patal SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

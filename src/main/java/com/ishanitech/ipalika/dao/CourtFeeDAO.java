package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.CourtFeeDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface CourtFeeDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_court_fee")
    @RegisterBeanMapper(CourtFeeDTO.class)
    List<CourtFeeDTO> getCourtRegistrations();

    @SqlUpdate("INSERT INTO egovernance_court_fee (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, tax_information_photo, evidence_document_filed_in_court_1, evidence_document_filed_in_court_2, written_reason_for_waiving_fee, local_sarjamin_muchulka, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :taxInformationPhoto, :evidenceDocumentFiledInCourt1, :evidenceDocumentFiledInCourt2, :writtenReasonForWaivingFee, :localSarjaminMuchulka, :email, :phone, :dateOfRegistration)")
    void addCourtRegistration(@BindBean CourtFeeDTO courtRegistrationInfo);
    @Transaction
    default void addNewEntity(CourtFeeDTO courtRegistrationInfo) {
        addCourtRegistration(courtRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.COURT_FEE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_court_fee WHERE token_id = :tokenId")
    @RegisterBeanMapper(CourtFeeDTO.class)
    CourtFeeDTO getCourtCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_court_fee SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateCourtRegistrationByTokenId(@BindBean CourtFeeDTO courtRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_court_fee SET approved = 1 WHERE token_id =:tokenId")
    void approveCourtRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_court_fee SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

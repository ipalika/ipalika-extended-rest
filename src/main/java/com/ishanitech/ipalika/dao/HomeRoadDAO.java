package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.HomeRoadDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface HomeRoadDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_home_road")
    @RegisterBeanMapper(HomeRoadDTO.class)
    List<HomeRoadDTO> getHomeRoadRegistrations();

    @SqlUpdate("INSERT INTO egovernance_home_road (token_id, registration_no, ward_no, applicant_name, address, application_photo, land_ownership_certificate, gross_survey_map_of_land, tax_information_photo, giver_citizenship_photo, receiver_citizenship_photo, onsite_inspection_report, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :landOwnershipCertificate, :grossSurveyMapOfLand, :taxInformationPhoto, :giverCitizenshipPhoto, :receiverCitizenshipPhoto, :onsiteInspectionReport, :email, :phone, :dateOfRegistration)")
    void addHomeRoadRegistration(@BindBean HomeRoadDTO homeRegistrationInfo);
    @Transaction
    default void addNewEntity(HomeRoadDTO homeRegistrationInfo) {
        addHomeRoadRegistration(homeRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.HOME_ROAD.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_home_road WHERE token_id = :tokenId")
    @RegisterBeanMapper(HomeRoadDTO.class)
    HomeRoadDTO getHomeRoadCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_home_road SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateHomeRoadRegistrationByTokenId(@BindBean HomeRoadDTO homeRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_home_road SET approved = 1 WHERE token_id =:tokenId")
    void approveHomeRoadRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_home_road SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

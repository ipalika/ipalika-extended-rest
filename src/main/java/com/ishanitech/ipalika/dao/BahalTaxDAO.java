package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BahalTaxDTO;
import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface BahalTaxDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_bahal_tax")
    @RegisterBeanMapper(BahalTaxDTO.class)
    List<BahalTaxDTO> getBahalTaxRegistrations();

    @SqlUpdate("INSERT INTO egovernance_bahal_tax (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, bahal_agreement_photo, registration_certificate, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :bahalAgreementPhoto, :registrationCertificate, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addBahalTaxRegistration(@BindBean BahalTaxDTO bahalRegistrationInfo);
    @Transaction
    default void addNewEntity(BahalTaxDTO bahalRegistrationInfo) {
        addBahalTaxRegistration(bahalRegistrationInfo);
        updateRegistrationReportForm(1,1);
    }
    @SqlQuery("SELECT * FROM egovernance_bahal_tax WHERE token_id = :tokenId")
    @RegisterBeanMapper(BahalTaxDTO.class)
    BahalTaxDTO getBahalTaxCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_bahal_tax SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateBahalTaxRegistrationByTokenId(@BindBean BahalTaxDTO bahalRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_bahal_tax SET approved = 1 WHERE token_id =:tokenId")
    void approveBahalTaxRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_bahal_tax SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.LandRegisterDTO;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface LandRegisterDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_land_register")
    @RegisterBeanMapper(LandRegisterDTO.class)
    List<LandRegisterDTO> getLandRegistrations();

    @SqlUpdate("INSERT INTO egovernance_land_register (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, tax_information_photo, copy_of_previous_data, field_book, onsite_inspection_report, land_survey_map, proof_document_1, proof_document_2, proof_document_3, local_sarjamin_muchulka, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :taxInformationPhoto, :copyOfPreviousData, :fieldBook, :onsiteInspectionReport, :landSurveyMap, :proofDocument1, :proofDocument2, :proofDocument3, :localSarjaminMuchulka, :email, :phone, :dateOfRegistration)")
    void addLandRegistration(@BindBean LandRegisterDTO landRegistrationInfo);
    @Transaction
    default void addNewEntity(LandRegisterDTO landRegistrationInfo) {
        addLandRegistration(landRegistrationInfo);
        updateRegistrationReportForm(1,1);
    }
    @SqlQuery("SELECT * FROM egovernance_land_register WHERE token_id = :tokenId")
    @RegisterBeanMapper(LandRegisterDTO.class)
    LandRegisterDTO getLandCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_register SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateLandRegistrationByTokenId(@BindBean LandRegisterDTO landRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_register SET approved = 1 WHERE token_id =:tokenId")
    void approveLandRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_register SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

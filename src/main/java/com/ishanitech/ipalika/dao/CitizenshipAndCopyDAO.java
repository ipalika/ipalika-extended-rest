package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.CitizenshipAndCopyDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface CitizenshipAndCopyDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_citizenship_and_copy")
    @RegisterBeanMapper(CitizenshipAndCopyDTO.class)
    List<CitizenshipAndCopyDTO> getCitizenshipRegistrations();

    @SqlUpdate("INSERT INTO egovernance_citizenship_and_copy (token_id, registration_no, applicant_name, address, application_photo, father_citizenship_photo, mother_citizenship_photo, birth_certificate, husband_citizenship_photo, student_character_certificate, marriage_registration_certificate, resettlement_certificate, pp_photo_copy_1, pp_photo_copy_2, tax_information_photo, concerned_office_letter, old_citizenship_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :fatherCitizenshipPhoto, :motherCitizenshipPhoto, :birthCertificate, :husbandCitizenshipPhoto, :studentCharacterCertificate, :marriageRegistrationCertificate, :resettlementCertificate, :ppPhotoCopy1, :ppPhotoCopy2, :taxInformationPhoto, :concernedOfficeLetter, :oldCitizenshipPhoto, :email, :phone, :dateOfRegistration)")
    void addCitizenshipRegistration(@BindBean CitizenshipAndCopyDTO citizenshipRegistrationInfo);
    @Transaction
    default void addNewEntity(CitizenshipAndCopyDTO citizenshipRegistrationInfo) {
        addCitizenshipRegistration(citizenshipRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.CITIZENSHIP_AND_COPY.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_citizenship_and_copy WHERE token_id = :tokenId")
    @RegisterBeanMapper(CitizenshipAndCopyDTO.class)
    CitizenshipAndCopyDTO getCitizenshipCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_citizenship_and_copy SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateCitizenshipRegistrationByTokenId(@BindBean CitizenshipAndCopyDTO citizenshipRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_citizenship_and_copy SET approved = 1 WHERE token_id =:tokenId")
    void approveCitizenshipRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_citizenship_and_copy SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

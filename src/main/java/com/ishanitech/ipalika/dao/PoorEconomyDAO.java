package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.PoorEconomyDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PoorEconomyDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_poor_economy")
    @RegisterBeanMapper(PoorEconomyDTO.class)
    List<PoorEconomyDTO> getPoorEconomyRegistrations();

    @SqlUpdate("INSERT INTO egovernance_poor_economy (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, document_confirming_poor_1, document_confirming_poor_2, document_confirming_poor_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :documentConfirmingPoor1, :documentConfirmingPoor2, :documentConfirmingPoor3, :email, :phone, :dateOfRegistration)")
    void addPoorEconomyRegistration(@BindBean PoorEconomyDTO economyRegistrationInfo);
    @Transaction
    default void addNewEntity(PoorEconomyDTO economyRegistrationInfo) {
        addPoorEconomyRegistration(economyRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.POOR_ECONOMY.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_poor_economy WHERE token_id = :tokenId")
    @RegisterBeanMapper(PoorEconomyDTO.class)
    PoorEconomyDTO getPoorEconomyCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_poor_economy SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updatePoorEconomyRegistrationByTokenId(@BindBean PoorEconomyDTO economyRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_poor_economy SET approved = 1 WHERE token_id =:tokenId")
    void approvePoorEconomyRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_poor_economy SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

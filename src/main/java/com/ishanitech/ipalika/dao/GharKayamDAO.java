package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.GharKayamDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface GharKayamDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_ghar_kayam")
    @RegisterBeanMapper(GharKayamDTO.class)
    List<GharKayamDTO> getGharKayamRegistrations();

    @SqlUpdate("INSERT INTO egovernance_ghar_kayam (token_id, registration_no, ward_no, name, address, application_photo, citizenship_photo, land_lalpurja, onsite_report, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :applicationPhoto, :citizenshipPhoto, :landLalpurja, :onsiteReport, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addGharKayamRegistration(@BindBean GharKayamDTO gharRegistrationInfo);
    @Transaction
    default void addNewEntity(GharKayamDTO gharRegistrationInfo) {
        addGharKayamRegistration(gharRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.GHAR_KAYAM.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_ghar_kayam WHERE token_id = :tokenId")
    @RegisterBeanMapper(GharKayamDTO.class)
    GharKayamDTO getGharKayamCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_ghar_kayam SET ward_no =:wardNo, name =:name, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateGharKayamRegistrationByTokenId(@BindBean GharKayamDTO gharRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_ghar_kayam SET approved = 1 WHERE token_id =:tokenId")
    void approveGharKayamRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_ghar_kayam SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

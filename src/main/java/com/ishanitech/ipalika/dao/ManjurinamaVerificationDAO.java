package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.ManjurinamaVerificationDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface ManjurinamaVerificationDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_manjurinama_verification")
    @RegisterBeanMapper(ManjurinamaVerificationDTO.class)
    List<ManjurinamaVerificationDTO> getManjurinamaVerifications();

    @SqlUpdate("INSERT INTO egovernance_manjurinama_verification (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, proof_document_1, proof_document_2, proof_document_3, tax_information_photo , email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :proofDocument1, :proofDocument2, :proofDocument3, :taxInformationPhoto , :email, :phone, :dateOfRegistration)")
    void addManjurinamaVerification(@BindBean ManjurinamaVerificationDTO manjurinamaVerificationInfo);
    @Transaction
    default void addNewEntity(ManjurinamaVerificationDTO manjurinamaVerificationInfo) {
        addManjurinamaVerification(manjurinamaVerificationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.MANJURINAMA_VERIFICATION.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_manjurinama_verification WHERE token_id = :tokenId")
    @RegisterBeanMapper(ManjurinamaVerificationDTO.class)
    ManjurinamaVerificationDTO getManjurinamaVerificationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_manjurinama_verification SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateManjurinamaVerificationByTokenId(@BindBean ManjurinamaVerificationDTO manjurinamaVerificationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_manjurinama_verification SET approved = 1 WHERE token_id =:tokenId")
    void approveManjurinamaVerification(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_manjurinama_verification SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.AliveCertificateDTO;
import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface AliveDAO extends CommonDAO{


    @SqlQuery("SELECT * FROM egovernance_alive_certificate")
    @RegisterBeanMapper(AliveCertificateDTO.class)
    List<AliveCertificateDTO> getAliveRegistrations();

    @SqlUpdate("INSERT INTO egovernance_alive_certificate (token_id, registration_no, ward_no, name, address, gender, application_photo, citizenship_photo, pp_photo, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :gender, :applicationPhoto, :citizenshipPhoto, :ppPhoto, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addAliveRegistration(@BindBean AliveCertificateDTO aliveRegistrationInfo);
    @Transaction
    default void addNewEntity(AliveCertificateDTO aliveRegistrationInfo) {
        addAliveRegistration(aliveRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.ALIVE_CERTIFICATE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_alive_certificate WHERE token_id = :tokenId")
    @RegisterBeanMapper(AliveCertificateDTO.class)
    AliveCertificateDTO getAliveRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_alive_certificate SET ward_no =:wardNo, name =:name, address =:address, gender =:gender, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateAliveRegistrationByTokenId(@BindBean AliveCertificateDTO aliveRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_alive_certificate SET approved = 1 WHERE token_id =:tokenId")
    void approveAliveRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_alive_certificate SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

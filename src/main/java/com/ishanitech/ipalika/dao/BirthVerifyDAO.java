package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.BirthVerifyDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface BirthVerifyDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_birth_verification")
    @RegisterBeanMapper(BirthVerifyDTO.class)
    List<BirthVerifyDTO> getBirthVerifications();

    @SqlUpdate("INSERT INTO egovernance_birth_verification (token_id, registration_no, ward_no, name, address, gender, application_photo, citizenship_photo, birth_certificate_minor, resettlement_certificate, tax_information_photo, email, phone, date_of_verification) VALUES(:tokenId, :registrationNo, :wardNo, :name, :address, :gender, :applicationPhoto, :citizenshipPhoto, :birthCertificateMinor, :resettlementCertificate, :taxInformationPhoto, :email, :phone, :dateOfVerification)")
    void addBirthVerification(@BindBean BirthVerifyDTO birthVerificationInfo);
    @Transaction
    default void addNewEntity(BirthVerifyDTO birthVerificationInfo) {
        addBirthVerification(birthVerificationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.BIRTH_VERIFICATION.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_birth_verification WHERE token_id = :tokenId")
    @RegisterBeanMapper(BirthVerifyDTO.class)
    BirthVerifyDTO getBirthVerificationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_birth_verification SET ward_no =:wardNo, name =:name, address =:address, gender =:gender, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId")
    void updateBirthVerificationByTokenId(@BindBean BirthVerifyDTO birthVerificationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_birth_verification SET approved = 1 WHERE token_id =:tokenId")
    void approveBirthVerification(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_birth_verification SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.DeathCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface DeathDAO extends CommonDAO{


    @SqlQuery("SELECT * FROM egovernance_death_certificate")
    @RegisterBeanMapper(DeathCertificateDTO.class)
    List<DeathCertificateDTO> getDeathRegistrations();

    @SqlUpdate("INSERT INTO egovernance_death_certificate (token_id, registration_no, ward_no, name, gender, dob_Ad, dob_Bs, date_of_demise_ad, date_of_demise_bs, demise_cause, application_photo, citizenship_photo, informant_document, relationship_certificate_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :name, :gender, :dobAd, :dobBs, :dateOfDemiseAd, :dateOfDemiseBs, :demiseCause, :applicationPhoto, :citizenshipPhoto, :informantDocument, :relationshipCertificatePhoto, :email, :phone, :dateOfRegistration)")
    void addDeathRegistration(@BindBean DeathCertificateDTO deathRegistrationInfo);
    @Transaction
    default void addNewEntity(DeathCertificateDTO deathRegistrationInfo) {
        addDeathRegistration(deathRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.DEATH_CERTIFICATE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_death_certificate WHERE token_id = :tokenId")
    @RegisterBeanMapper(DeathCertificateDTO.class)
    DeathCertificateDTO getDeathRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_death_certificate SET ward_no =:wardNo, name =:name, gender =:gender, dob_Ad =:dobAd, dob_Bs =:dobBs, date_of_demise_ad =:dateOfDemiseAd, date_of_demise_bs =:dateOfDemiseBs, demise_cause =:demiseCause, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateDeathRegistrationByTokenId(@BindBean DeathCertificateDTO deathRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_death_certificate SET approved = 1 WHERE token_id =:tokenId")
    void approveDeathRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_death_certificate SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

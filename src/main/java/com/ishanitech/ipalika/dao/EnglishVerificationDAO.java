package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.EnglishVerificationDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface EnglishVerificationDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_english_verification")
    @RegisterBeanMapper(EnglishVerificationDTO.class)
    List<EnglishVerificationDTO> getEnglishVerifications();

    @SqlUpdate("INSERT INTO egovernance_english_verification (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, proof_document_1, proof_document_2, proof_document_3, tax_information_photo , email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :proofDocument1, :proofDocument2, :proofDocument3, :taxInformationPhoto , :email, :phone, :dateOfRegistration)")
    void addEnglishVerification(@BindBean EnglishVerificationDTO englishVerificationInfo);
    @Transaction
    default void addNewEntity(EnglishVerificationDTO englishVerificationInfo) {
        addEnglishVerification(englishVerificationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.ENGLISH_VERIFICATION.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_english_verification WHERE token_id = :tokenId")
    @RegisterBeanMapper(EnglishVerificationDTO.class)
    EnglishVerificationDTO getEnglishVerificationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_english_verification SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateEnglishVerificationByTokenId(@BindBean EnglishVerificationDTO englishVerificationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_english_verification SET approved = 1 WHERE token_id =:tokenId")
    void approveEnglishVerification(@Bind("tokenId") String tokenId);
    @SqlUpdate("UPDATE egovernance_english_verification SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.SchoolShiftingDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface SchoolShiftingDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_विद्यालय_ठाउँसारी")
    @RegisterBeanMapper(SchoolShiftingDTO.class)
    List<SchoolShiftingDTO> getSchoolRegistrations();

    @SqlUpdate("INSERT INTO egovernance_विद्यालय_ठाउँसारी (token_id, registration_no, applicant_name, address, application_photo, school_registration_certificate, local_level_renewal_certificate, tax_information_current_place, tax_information_shifting_place, bahal_agreement_photo, bahal_tax_information, permanent_account_no_certificate, inspection_report, ward_office_permit_letter, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :schoolRegistrationCertificate, :localLevelRenewalCertificate, :taxInformationCurrentPlace, :taxInformationShiftingPlace, :bahalAgreementPhoto, :bahalTaxInformation, :permanentAccountNoCertificate, :inspectionReport, :wardOfficePermitLetter, :email, :phone, :dateOfRegistration)")
    void addSchoolRegistration(@BindBean SchoolShiftingDTO schoolRegistrationInfo);
    @Transaction
    default void addNewEntity(SchoolShiftingDTO schoolRegistrationInfo) {
        addSchoolRegistration(schoolRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.SCHOOL_TRANSFER.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_विद्यालय_ठाउँसारी WHERE token_id = :tokenId")
    @RegisterBeanMapper(SchoolShiftingDTO.class)
    SchoolShiftingDTO getSchoolCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_विद्यालय_ठाउँसारी SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateSchoolRegistrationByTokenId(@BindBean SchoolShiftingDTO schoolRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_विद्यालय_ठाउँसारी SET approved = 1 WHERE token_id =:tokenId")
    void approveSchoolRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_विद्यालय_ठाउँसारी SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.InternalResidenceDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface InternalResidenceDAO extends CommonDAO{


    @SqlQuery("SELECT * FROM egovernance_internal_residence")
    @RegisterBeanMapper(InternalResidenceDTO.class)
    List<InternalResidenceDTO> getResidenceRegistrations();

    @SqlUpdate("INSERT INTO egovernance_internal_residence (token_id, registration_no, applicant_name, address, application_photo, relocating_person_citizenship_photo_1, relocating_person_citizenship_photo_2, relocating_person_citizenship_photo_3, marriage_registration_certificate, birth_registration_certificate_1, birth_registration_certificate_2, birth_registration_certificate_3, land_ownership_certificate, business_proof_document, residence_proof_document, tax_information_photo, bahal_agreement_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :relocatingPersonCitizenshipPhoto1, :relocatingPersonCitizenshipPhoto2, :relocatingPersonCitizenshipPhoto3, :marriageRegistrationCertificate, :birthRegistrationCertificate1, :birthRegistrationCertificate2, :birthRegistrationCertificate3, :landOwnershipCertificate, :businessProofDocument, :residenceProofDocument, :taxInformationPhoto, :bahalAgreementPhoto, :email, :phone, :dateOfRegistration)")
    void addResidenceRegistration(@BindBean InternalResidenceDTO internalResidenceInfo);
    @Transaction
    default void addNewEntity(InternalResidenceDTO internalResidenceInfo) {
        addResidenceRegistration(internalResidenceInfo);
        updateRegistrationReportForm(1, FormInfoUtil.INTERNAL_RESIDENCE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_internal_residence WHERE token_id = :tokenId")
    @RegisterBeanMapper(InternalResidenceDTO.class)
    InternalResidenceDTO getResidenceCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_internal_residence SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateResidenceCertificateByTokenId(@BindBean InternalResidenceDTO internalResidenceInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_internal_residence SET approved = 1 WHERE token_id =:tokenId")
    void approveResidenceCertificate(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_internal_residence SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormStatusUpdationInfo {

    private String wardAdminNameEng;
    private String registrarNameEng;
    private String wardSecretaryName;


}

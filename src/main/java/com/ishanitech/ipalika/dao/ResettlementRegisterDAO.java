package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.ResettlementRegisterDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface ResettlementRegisterDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_resettlement_register")
    @RegisterBeanMapper(ResettlementRegisterDTO.class)
    List<ResettlementRegisterDTO> getResettlementRegistrations();

    @SqlUpdate("INSERT INTO egovernance_resettlement_register (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, family_details_photo, concerned_ward_office_letter, lalpurja_of_going_place, lalpurja_of_coming_place, certificate_of_relocation, relocating_person_citizenship_photo_1, relocating_person_citizenship_photo_2, relocating_person_citizenship_photo_3, relocating_person_citizenship_photo_4, birth_registration_certificate_1, birth_registration_certificate_2, birth_registration_certificate_3, birth_registration_certificate_4, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :familyDetailsPhoto, :concernedWardOfficeLetter, :lalpurjaOfGoingPlace, :lalpurjaOfComingPlace, :certificateOfRelocation, :relocatingPersonCitizenshipPhoto1, :relocatingPersonCitizenshipPhoto2, :relocatingPersonCitizenshipPhoto3, :relocatingPersonCitizenshipPhoto4, :birthRegistrationCertificate1, :birthRegistrationCertificate2, :birthRegistrationCertificate3, :birthRegistrationCertificate4, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addResettlementRegistration(@BindBean ResettlementRegisterDTO resettlementRegistrationInfo);
    @Transaction
    default void addNewEntity(ResettlementRegisterDTO resettlementRegistrationInfo) {
        addResettlementRegistration(resettlementRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.RESETTLEMENT_REGISTER.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_resettlement_register WHERE token_id = :tokenId")
    @RegisterBeanMapper(ResettlementRegisterDTO.class)
    ResettlementRegisterDTO getResettlementCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_resettlement_register SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateResettlementCertificateByTokenId(@BindBean ResettlementRegisterDTO resettlementRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_resettlement_register SET approved = 1 WHERE token_id =:tokenId")
    void approveResettlementCertificate(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_resettlement_register SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.PersonalDetailsDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PersonalDetailsDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_व्यक्तिगत_विवरण")
    @RegisterBeanMapper(PersonalDetailsDTO.class)
    List<PersonalDetailsDTO> getPersonalDetailsRegistrations();

    @SqlUpdate("INSERT INTO egovernance_व्यक्तिगत_विवरण (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, tax_information_photo, proof_document_1, proof_document_2, proof_document_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :taxInformationPhoto, :proofDocument1, :proofDocument2, :proofDocument3, :email, :phone, :dateOfRegistration)")
    void addPersonalDetailsRegistration(@BindBean PersonalDetailsDTO personalRegistrationInfo);
    @Transaction
    default void addNewEntity(PersonalDetailsDTO personalRegistrationInfo) {
        addPersonalDetailsRegistration(personalRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.PERSONAL_DETAILS.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_व्यक्तिगत_विवरण WHERE token_id = :tokenId")
    @RegisterBeanMapper(PersonalDetailsDTO.class)
    PersonalDetailsDTO getPersonalDetailsCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_व्यक्तिगत_विवरण SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updatePersonalDetailsRegistrationByTokenId(@BindBean PersonalDetailsDTO personalRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_व्यक्तिगत_विवरण SET approved = 1 WHERE token_id =:tokenId")
    void approvePersonalDetailsRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_व्यक्तिगत_विवरण SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

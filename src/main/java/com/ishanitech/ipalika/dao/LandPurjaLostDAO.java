package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.LandPurjaLostDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface LandPurjaLostDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_land_dhanipurja")
    @RegisterBeanMapper(LandPurjaLostDTO.class)
    List<LandPurjaLostDTO> getLandRegistrations();

    @SqlUpdate("INSERT INTO egovernance_land_dhanipurja (token_id, registration_no, ward_no, applicant_name, address, application_photo, citizenship_photo, land_ownership_certificate, sarjamin_muchulka, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :wardNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :landOwnershipCertificate, :sarjaminMuchulka, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addLandRegistration(@BindBean LandPurjaLostDTO landRegistrationInfo);
    @Transaction
    default void addNewEntity(LandPurjaLostDTO landRegistrationInfo) {
        addLandRegistration(landRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.LAND_DHANIPURJA.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_land_dhanipurja WHERE token_id = :tokenId")
    @RegisterBeanMapper(LandPurjaLostDTO.class)
    LandPurjaLostDTO getLandCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_dhanipurja SET ward_no =:wardNo, applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateLandRegistrationByTokenId(@BindBean LandPurjaLostDTO landRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_dhanipurja SET approved = 1 WHERE token_id =:tokenId")
    void approveLandRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_land_dhanipurja SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

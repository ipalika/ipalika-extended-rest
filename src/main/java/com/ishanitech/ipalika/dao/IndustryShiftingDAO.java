package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.IndustryShiftingDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface IndustryShiftingDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_उद्योग_ठाउँसारी")
    @RegisterBeanMapper(IndustryShiftingDTO.class)
    List<IndustryShiftingDTO> getIndustryRegistrations();

    @SqlUpdate("INSERT INTO egovernance_उद्योग_ठाउँसारी (token_id, registration_no, applicant_name, address, application_photo, industry_registration_certificate, local_level_renewal_certificate, tax_information_photo, bahal_agreement_photo, bahal_tax_information, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :industryRegistrationCertificate, :localLevelRenewalCertificate, :taxInformationPhoto, :bahalAgreementPhoto, :bahalTaxInformation, :email, :phone, :dateOfRegistration)")
    void addIndustryRegistration(@BindBean IndustryShiftingDTO industryRegistrationInfo);
    @Transaction
    default void addNewEntity(IndustryShiftingDTO industryRegistrationInfo) {
        addIndustryRegistration(industryRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.INDUSTRY_TRANSFER.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_उद्योग_ठाउँसारी WHERE token_id = :tokenId")
    @RegisterBeanMapper(IndustryShiftingDTO.class)
    IndustryShiftingDTO getIndustryCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_उद्योग_ठाउँसारी SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateIndustryRegistrationByTokenId(@BindBean IndustryShiftingDTO industryRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_उद्योग_ठाउँसारी SET approved = 1 WHERE token_id =:tokenId")
    void approveIndustryRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_उद्योग_ठाउँसारी SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

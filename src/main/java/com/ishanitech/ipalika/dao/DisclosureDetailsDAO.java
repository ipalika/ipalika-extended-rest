package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.DisclosureDetailsDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface DisclosureDetailsDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_details_disclosure")
    @RegisterBeanMapper(DisclosureDetailsDTO.class)
    List<DisclosureDetailsDTO> getDisclosureRegistrations();

    @SqlUpdate("INSERT INTO egovernance_details_disclosure (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, office_letter, other_document_1, other_document_2, other_document_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :officeLetter, :otherDocument1, :otherDocument2, :otherDocument3, :email, :phone, :dateOfRegistration)")
    void addDisclosureRegistration(@BindBean DisclosureDetailsDTO disclosureRegistrationInfo);
    @Transaction
    default void addNewEntity(DisclosureDetailsDTO disclosureRegistrationInfo) {
        addDisclosureRegistration(disclosureRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.DETAILS_DISCLOSURE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_details_disclosure WHERE token_id = :tokenId")
    @RegisterBeanMapper(DisclosureDetailsDTO.class)
    DisclosureDetailsDTO getDisclosureCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_details_disclosure SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateDisclosureRegistrationByTokenId(@BindBean DisclosureDetailsDTO disclosureRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_details_disclosure SET approved = 1 WHERE token_id =:tokenId")
    void approveDisclosureRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_details_disclosure SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

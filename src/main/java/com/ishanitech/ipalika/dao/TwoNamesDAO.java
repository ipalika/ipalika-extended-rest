package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.TwoNamesDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface TwoNamesDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_two_names")
    @RegisterBeanMapper(TwoNamesDTO.class)
    List<TwoNamesDTO> getTwoNamesRegistrations();

    @SqlUpdate("INSERT INTO egovernance_two_names (token_id, registration_no, applicant_name, address, application_photo, citizenship_photo, tax_information_photo, sarjamin_muchulka, doc_confirming_name_change_1, doc_confirming_name_change_2, doc_confirming_name_change_3, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :citizenshipPhoto, :taxInformationPhoto, :sarjaminMuchulka, :docConfirmingNameChange1, :docConfirmingNameChange2, :docConfirmingNameChange3, :email, :phone, :dateOfRegistration)")
    void addTwoNamesRegistration(@BindBean TwoNamesDTO namesRegistrationInfo);
    @Transaction
    default void addNewEntity(TwoNamesDTO namesRegistrationInfo) {
        addTwoNamesRegistration(namesRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.TWO_NAMES.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_two_names WHERE token_id = :tokenId")
    @RegisterBeanMapper(TwoNamesDTO.class)
    TwoNamesDTO getTwoNamesCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_two_names SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateTwoNamesRegistrationByTokenId(@BindBean TwoNamesDTO namesRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_two_names SET approved = 1 WHERE token_id =:tokenId")
    void approveTwoNamesRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_two_names SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

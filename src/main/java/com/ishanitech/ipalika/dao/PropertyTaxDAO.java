package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.PropertyTaxDTO;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface PropertyTaxDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_property_tax")
    @RegisterBeanMapper(PropertyTaxDTO.class)
    List<PropertyTaxDTO> getPropertyTaxRegistrations();

    @SqlUpdate("INSERT INTO egovernance_property_tax (token_id, registration_no, applicant_name, address, application_photo, land_ownership_certificate, building_map_approval_certificate, copy_of_map, registration_pass_certificate, local_level_survey_map, onsite_technical_report, tax_paid_to_inland_revenue_office, citizenship_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :landOwnershipCertificate, :buildingMapApprovalCertificate, :copyOfMap, :registrationPassCertificate, :localLevelSurveyMap, :onsiteTechnicalReport, :taxPaidToInlandRevenueOffice, :citizenshipPhoto, :email, :phone, :dateOfRegistration)")
    void addPropertyTaxRegistration(@BindBean PropertyTaxDTO propertyRegistrationInfo);
    @Transaction
    default void addNewEntity(PropertyTaxDTO propertyRegistrationInfo) {
        addPropertyTaxRegistration(propertyRegistrationInfo);
        updateRegistrationReportForm(1,1);
    }
    @SqlQuery("SELECT * FROM egovernance_property_tax WHERE token_id = :tokenId")
    @RegisterBeanMapper(PropertyTaxDTO.class)
    PropertyTaxDTO getPropertyTaxCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_property_tax SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updatePropertyTaxRegistrationByTokenId(@BindBean PropertyTaxDTO propertyRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_property_tax SET approved = 1 WHERE token_id =:tokenId")
    void approvePropertyTaxRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_property_tax SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

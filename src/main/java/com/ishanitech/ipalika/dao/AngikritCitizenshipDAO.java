package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.AngikritCitizenshipDTO;
import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface AngikritCitizenshipDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_angikrit_citizenship")
    @RegisterBeanMapper(AngikritCitizenshipDTO.class)
    List<AngikritCitizenshipDTO> getCitizenshipRegistrations();

    @SqlUpdate("INSERT INTO egovernance_angikrit_citizenship (token_id, registration_no, applicant_name, address, application_photo, doc_renouncing_citizenship_of_former_country, proof_doc_of_living_in_nepal_for_15_years, marriage_registration_certificate, official_certificate_of_concerned_country, proof_document_knowing_to_read_write_in_nepali, pp_photo_1, pp_photo_2, pp_photo_3, tax_information_photo, sarjamin_muchulka, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :docRenouncingCitizenshipOfFormerCountry, :proofDocOfLivingInNepalFor15Years, :marriageRegistrationCertificate, :officialCertificateOfConcernedCountry, :proofDocumentKnowingToReadWriteInNepali, :ppPhoto1, :ppPhoto2, :ppPhoto3, :taxInformationPhoto, :sarjaminMuchulka, :email, :phone, :dateOfRegistration)")
    void addCitizenshipRegistration(@BindBean AngikritCitizenshipDTO citizenshipRegistrationInfo);
    @Transaction
    default void addNewEntity(AngikritCitizenshipDTO citizenshipRegistrationInfo) {
        addCitizenshipRegistration(citizenshipRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.ANGIKRIT_CITIZENSHIP.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_angikrit_citizenship WHERE token_id = :tokenId")
    @RegisterBeanMapper(AngikritCitizenshipDTO.class)
    AngikritCitizenshipDTO getCitizenshipCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_angikrit_citizenship SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateCitizenshipRegistrationByTokenId(@BindBean AngikritCitizenshipDTO citizenshipRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_angikrit_citizenship SET approved = 1 WHERE token_id =:tokenId")
    void approveCitizenshipRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_angikrit_citizenship SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

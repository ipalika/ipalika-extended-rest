package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.DivorceCertificateDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface DivorceDAO extends CommonDAO{


    @SqlQuery("SELECT * FROM egovernance_divorce_certificate")
    @RegisterBeanMapper(DivorceCertificateDTO.class)
    List<DivorceCertificateDTO> getDivorceRegistrations();

    @SqlUpdate("INSERT INTO egovernance_divorce_certificate (token_id, registration_no, date_of_marriage_ad, date_of_marriage_bs, husband_name, permanent_address_husband, ward_no_husband, husband_citizenship_id, husband_citizenship_photo, wife_name, permanent_address_wife, ward_no_wife, wife_citizenship_id, wife_citizenship_photo, application_photo, dissolution_certificate, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :dateOfMarriageAd, :dateOfMarriageBs, :husbandName, :permanentAddressHusband, :wardNoHusband, :husbandCitizenshipId, :husbandCitizenshipPhoto, :wifeName, :permanentAddressWife, :wardNoWife, :wifeCitizenshipId, :wifeCitizenshipPhoto, :applicationPhoto, :dissolutionCertificate, :email, :phone, :dateOfRegistration)")
    void addDivorceRegistration(@BindBean DivorceCertificateDTO divorceRegistrationInfo);
    @Transaction
    default void addNewEntity(DivorceCertificateDTO divorceRegistrationInfo) {
        addDivorceRegistration(divorceRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.DIVORCE_CERTIFICATE.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_divorce_certificate WHERE token_id = :tokenId")
    @RegisterBeanMapper(DivorceCertificateDTO.class)
    DivorceCertificateDTO getDivorceRegistrationByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_divorce_certificate SET date_of_marriage_ad =:dateOfMarriageAd, date_of_marriage_bs =:dateOfMarriageBs, husband_name =:husbandName, permanent_address_husband =:permanentAddressHusband, ward_no_husband =:wardNoHusband, husband_citizenship_id =:husbandCitizenshipId, wife_name =:wifeName, permanent_address_wife =:permanentAddressWife, ward_no_wife =:wardNoWife, wife_citizenship_id =:wifeCitizenshipId, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId")
    void updateDivorceRegistrationByTokenId(@BindBean DivorceCertificateDTO divorceRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_divorce_certificate SET approved = 1 WHERE token_id =:tokenId")
    void approveDivorceRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_divorce_certificate SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.GuardianPersonalDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface GuardianPersonalDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_संरक्षक_व्यक्तिगत")
    @RegisterBeanMapper(GuardianPersonalDTO.class)
    List<GuardianPersonalDTO> getGuardianRegistrations();

    @SqlUpdate("INSERT INTO egovernance_संरक्षक_व्यक्तिगत (token_id, registration_no, applicant_name, address, application_photo, guardian_giver_citizenship_photo, guardian_giver_birth_certificate, guardian_receiver_citizenship_photo, guardian_receiver_birth_certificate, tax_information_photo, onsite_sarjamin_muchulka, local_sarjamin_muchulka, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :guardianGiverCitizenshipPhoto, :guardianGiverBirthCertificate, :guardianReceiverCitizenshipPhoto, :guardianReceiverBirthCertificate, :taxInformationPhoto, :onsiteSarjaminMuchulka, :localSarjaminMuchulka, :email, :phone, :dateOfRegistration)")
    void addGuardianRegistration(@BindBean GuardianPersonalDTO guardianRegistrationInfo);
    @Transaction
    default void addNewEntity(GuardianPersonalDTO guardianRegistrationInfo) {
        addGuardianRegistration(guardianRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.GUARDIAN_PERSONAL.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_संरक्षक_व्यक्तिगत WHERE token_id = :tokenId")
    @RegisterBeanMapper(GuardianPersonalDTO.class)
    GuardianPersonalDTO getGuardianCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_संरक्षक_व्यक्तिगत SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateGuardianRegistrationByTokenId(@BindBean GuardianPersonalDTO guardianRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_संरक्षक_व्यक्तिगत SET approved = 1 WHERE token_id =:tokenId")
    void approveGuardianRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_संरक्षक_व्यक्तिगत SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

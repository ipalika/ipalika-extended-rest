package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.LandTaxDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface LandTaxDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_मालपोत_भूमिकर")
    @RegisterBeanMapper(LandTaxDTO.class)
    List<LandTaxDTO> getLandRegistrations();

    @SqlUpdate("INSERT INTO egovernance_मालपोत_भूमिकर (token_id, registration_no, applicant_name, address, application_photo, land_ownership_certificate_for_first_year, land_tax_receipt_for_renewal, land_revenue_renewal_book, tax_information_photo, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :landOwnershipCertificateForFirstYear, :landTaxReceiptForRenewal, :landRevenueRenewalBook, :taxInformationPhoto, :email, :phone, :dateOfRegistration)")
    void addLandRegistration(@BindBean LandTaxDTO landRegistrationInfo);
    @Transaction
    default void addNewEntity(LandTaxDTO landRegistrationInfo) {
        addLandRegistration(landRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.MALPOT_BHUMIKAR.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_मालपोत_भूमिकर WHERE token_id = :tokenId")
    @RegisterBeanMapper(LandTaxDTO.class)
    LandTaxDTO getLandCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_मालपोत_भूमिकर SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateLandRegistrationByTokenId(@BindBean LandTaxDTO landRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_मालपोत_भूमिकर SET approved = 1 WHERE token_id =:tokenId")
    void approveLandRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_मालपोत_भूमिकर SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

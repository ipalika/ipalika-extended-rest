package com.ishanitech.ipalika.dao;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.BusinessRegisterDTO;
import com.ishanitech.ipalika.utils.FormInfoUtil;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface BusinessRegisterDAO extends CommonDAO{

    @SqlQuery("SELECT * FROM egovernance_business_register")
    @RegisterBeanMapper(BusinessRegisterDTO.class)
    List<BusinessRegisterDTO> getBusinessRegistrations();

    @SqlUpdate("INSERT INTO egovernance_business_register (token_id, registration_no, applicant_name, address, application_photo, business_registration_certificate, tax_information_photo, bahal_agreement_photo, pp_photo_copy_1, pp_photo_copy_2, email, phone, date_of_registration) VALUES(:tokenId, :registrationNo, :applicantName, :address, :applicationPhoto, :businessRegistrationCertificate, :taxInformationPhoto, :bahalAgreementPhoto, :ppPhotoCopy1, :ppPhotoCopy2, :email, :phone, :dateOfRegistration)")
    void addBusinessRegistration(@BindBean BusinessRegisterDTO businessRegistrationInfo);
    @Transaction
    default void addNewEntity(BusinessRegisterDTO businessRegistrationInfo) {
        addBusinessRegistration(businessRegistrationInfo);
        updateRegistrationReportForm(1, FormInfoUtil.BUSINESS_REGISTER.getFormId());
    }
    @SqlQuery("SELECT * FROM egovernance_business_register WHERE token_id = :tokenId")
    @RegisterBeanMapper(BusinessRegisterDTO.class)
    BusinessRegisterDTO getBusinessCertificateByTokenId(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_register SET applicant_name =:applicantName, address =:address, email =:email, phone =:phone, registrar_name_eng =:registrarNameEng, registrar_name_nep =:registrarNameNep, registrar_signature =:registrarSignature, registrar_signature_date =:registrarSignatureDate, ward_secretary_name =:wardSecretaryName, ward_secretary_signature =:wardSecretarySignature WHERE token_id =:tokenId" )
    void updateBusinessRegistrationByTokenId(@BindBean BusinessRegisterDTO businessRegistrationInfo, @Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_register SET approved = 1 WHERE token_id =:tokenId")
    void approveBusinessRegistration(@Bind("tokenId") String tokenId);

    @SqlUpdate("UPDATE egovernance_business_register SET status =:status WHERE token_id =:tokenId")
    void updateEntityStatus(@Bind("tokenId") String tokenId, @Bind("status") Integer status);

    @Transaction
    default	void updateStatus(String tokenId, Integer status, Integer formInfoId){
        updateEntityStatus(tokenId, status);
        updateRegistrationReportForm(status, formInfoId) ;
    }
}

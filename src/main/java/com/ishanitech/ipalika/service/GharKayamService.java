package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.GharKayamDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface GharKayamService {

    void addGharKayamRegistration(GharKayamDTO gharRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<GharKayamDTO> getGharKayamRegistrations(HttpServletRequest request);

    GharKayamDTO getGharKayamCertificateByTokenId(String tokenId);

    void updateGharKayamCertificateByTokenId(GharKayamDTO gharRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

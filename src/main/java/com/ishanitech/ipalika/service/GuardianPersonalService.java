package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.GuardianPersonalDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface GuardianPersonalService {

    void addGuardianRegistration(GuardianPersonalDTO guardianRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<GuardianPersonalDTO> getGuardianRegistrations(HttpServletRequest request);

    GuardianPersonalDTO getGuardianCertificateByTokenId(String tokenId);

    void updateGuardianCertificateByTokenId(GuardianPersonalDTO guardianRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

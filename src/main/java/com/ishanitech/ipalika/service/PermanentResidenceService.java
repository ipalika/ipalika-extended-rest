package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.PermanentResidenceDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PermanentResidenceService {


    void addPermanentResidenceRegistration(PermanentResidenceDTO permanentResidenceInfo);

    public void addAllImages(MultipartFile image);

    List<PermanentResidenceDTO> getPermanentResidenceRegistrations(HttpServletRequest request);

    PermanentResidenceDTO getPermanentResidenceCertificateByTokenId(String tokenId);

    void updatePermanentResidenceCertificateByTokenId(PermanentResidenceDTO permanentResidenceInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

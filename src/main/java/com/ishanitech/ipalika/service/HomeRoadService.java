package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.HomeRoadDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface HomeRoadService {

    void addHomeRoadRegistration(HomeRoadDTO homeRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<HomeRoadDTO> getHomeRoadRegistrations(HttpServletRequest request);

    HomeRoadDTO getHomeRoadCertificateByTokenId(String tokenId);

    void updateHomeRoadCertificateByTokenId(HomeRoadDTO homeRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

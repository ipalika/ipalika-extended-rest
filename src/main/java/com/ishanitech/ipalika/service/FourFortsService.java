package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.FourFortsDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FourFortsService {

    void addFourFortsRegistration(FourFortsDTO fortRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<FourFortsDTO> getFourFortsRegistrations(HttpServletRequest request);

    FourFortsDTO getFourFortsCertificateByTokenId(String tokenId);

    void updateFourFortsCertificateByTokenId(FourFortsDTO fortRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

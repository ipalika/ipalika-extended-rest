package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.LandPurjaLostDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LandPurjaLostService {

    void addLandRegistration(LandPurjaLostDTO landRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<LandPurjaLostDTO> getLandRegistrations(HttpServletRequest request);

    LandPurjaLostDTO getLandCertificateByTokenId(String tokenId);

    void updateLandCertificateByTokenId(LandPurjaLostDTO landRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

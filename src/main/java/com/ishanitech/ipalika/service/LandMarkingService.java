package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.LandMarkingDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LandMarkingService {

    void addLandMarkingRegistration(LandMarkingDTO landmarkRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<LandMarkingDTO> getLandMarkingRegistrations(HttpServletRequest request);

    LandMarkingDTO getLandMarkingCertificateByTokenId(String tokenId);

    void updateLandMarkingCertificateByTokenId(LandMarkingDTO landmarkRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

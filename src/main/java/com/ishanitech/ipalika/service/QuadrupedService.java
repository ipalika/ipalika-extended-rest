package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.QuadrupedDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface QuadrupedService {

    void addQuadrupedRegistration(QuadrupedDTO quadrupedRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<QuadrupedDTO> getQuadrupedRegistrations(HttpServletRequest request);

    QuadrupedDTO getQuadrupedCertificateByTokenId(String tokenId);

    void updateQuadrupedCertificateByTokenId(QuadrupedDTO quadrupedRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

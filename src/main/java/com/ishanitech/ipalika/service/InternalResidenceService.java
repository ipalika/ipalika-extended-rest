package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.InternalResidenceDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface InternalResidenceService {


    void addResidenceRegistration(InternalResidenceDTO internalResidenceInfo);

    public void addAllImages(MultipartFile image);

    List<InternalResidenceDTO> getResidenceRegistrations(HttpServletRequest request);

    InternalResidenceDTO getResidenceCertificateByTokenId(String tokenId);

    void updateResidenceCertificateByTokenId(InternalResidenceDTO internalResidenceInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

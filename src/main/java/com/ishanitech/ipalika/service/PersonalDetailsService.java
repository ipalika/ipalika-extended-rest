package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.PersonalDetailsDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PersonalDetailsService {

    void addPersonalDetailsRegistration(PersonalDetailsDTO personalRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<PersonalDetailsDTO> getPersonalDetailsRegistrations(HttpServletRequest request);

    PersonalDetailsDTO getPersonalDetailsCertificateByTokenId(String tokenId);

    void updatePersonalDetailsCertificateByTokenId(PersonalDetailsDTO personalRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

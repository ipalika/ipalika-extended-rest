package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.ScholarshipCertificateDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ScholarshipService {

    void addScholarshipRegistration(ScholarshipCertificateDTO scholarshipRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<ScholarshipCertificateDTO> getScholarshipRegistrations(HttpServletRequest request);

    ScholarshipCertificateDTO getScholarshipCertificateByTokenId(String tokenId);

    void updateScholarshipCertificateByTokenId(ScholarshipCertificateDTO scholarshipRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

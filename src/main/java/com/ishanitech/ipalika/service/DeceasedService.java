package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.DeceasedCertificateDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface DeceasedService {



    void addRelationRegistration(DeceasedCertificateDTO relationRegistrationInfo);

    void uploadRelationCertificate(MultipartFile image);

    List<DeceasedCertificateDTO> getRelationRegistrations(HttpServletRequest request);

    DeceasedCertificateDTO getRelationCertificateByTokenId(String tokenId);

    void updateRelationCertificateByTokenId(DeceasedCertificateDTO relationRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);

}

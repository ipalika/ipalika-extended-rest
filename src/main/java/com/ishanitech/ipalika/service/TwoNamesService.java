package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.TwoNamesDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface TwoNamesService {

    void addTwoNamesRegistration(TwoNamesDTO namesRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<TwoNamesDTO> getTwoNamesRegistrations(HttpServletRequest request);

    TwoNamesDTO getTwoNamesCertificateByTokenId(String tokenId);

    void updateTwoNamesCertificateByTokenId(TwoNamesDTO namesRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;


import com.ishanitech.ipalika.dto.BusinessNotOperatingDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BusinessNotOperatingService {

    void addBusinessRegistration(BusinessNotOperatingDTO businessRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<BusinessNotOperatingDTO> getBusinessRegistrations(HttpServletRequest request);

    BusinessNotOperatingDTO getBusinessCertificateByTokenId(String tokenId);

    void updateBusinessCertificateByTokenId(BusinessNotOperatingDTO businessRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

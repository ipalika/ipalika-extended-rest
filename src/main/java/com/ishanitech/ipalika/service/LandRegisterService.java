package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.LandRegisterDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LandRegisterService {

    void addLandRegistration(LandRegisterDTO landRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<LandRegisterDTO> getLandRegistrations(HttpServletRequest request);

    LandRegisterDTO getLandCertificateByTokenId(String tokenId);

    void updateLandCertificateByTokenId(LandRegisterDTO landRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.DeathCertificateDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface DeathService {

    void addDeathRegistration(DeathCertificateDTO deathRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<DeathCertificateDTO> getDeathRegistrations(HttpServletRequest request);

    DeathCertificateDTO getDeathCertificateByTokenId(String tokenId);

    void updateDeathCertificateByTokenId(DeathCertificateDTO deathRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

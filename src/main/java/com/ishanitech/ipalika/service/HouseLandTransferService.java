package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.HouseLandTransferDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface HouseLandTransferService {

    void addHouseLandRegistration(HouseLandTransferDTO houseRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<HouseLandTransferDTO> getHouseLandRegistrations(HttpServletRequest request);

    HouseLandTransferDTO getHouseLandCertificateByTokenId(String tokenId);

    void updateHouseLandCertificateByTokenId(HouseLandTransferDTO houseRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

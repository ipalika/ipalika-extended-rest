package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.RegistrationReport;

import java.util.List;

public interface RegistrationReportService {
    void getCounts();
    List<RegistrationReport> getRegistrationReports();
    void populateRegistrationCount();

}

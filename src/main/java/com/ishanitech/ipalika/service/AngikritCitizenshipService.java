package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.AngikritCitizenshipDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AngikritCitizenshipService {

    void addCitizenshipRegistration(AngikritCitizenshipDTO citizenshipRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<AngikritCitizenshipDTO> getCitizenshipRegistrations(HttpServletRequest request);

    AngikritCitizenshipDTO getCitizenshipCertificateByTokenId(String tokenId);

    void updateCitizenshipCertificateByTokenId(AngikritCitizenshipDTO citizenshipRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

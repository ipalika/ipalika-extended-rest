package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.AliveCertificateDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AliveService {

    void addAliveRegistration(AliveCertificateDTO aliveRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<AliveCertificateDTO> getAliveRegistrations(HttpServletRequest request);

    AliveCertificateDTO getAliveCertificateByTokenId(String tokenId);

    void updateAliveCertificateByTokenId(AliveCertificateDTO aliveRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.SchoolShiftingDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface SchoolShiftingService {

    void addSchoolRegistration(SchoolShiftingDTO schoolRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<SchoolShiftingDTO> getSchoolRegistrations(HttpServletRequest request);

    SchoolShiftingDTO getSchoolCertificateByTokenId(String tokenId);

    void updateSchoolCertificateByTokenId(SchoolShiftingDTO schoolRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

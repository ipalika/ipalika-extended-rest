package com.ishanitech.ipalika.service;

import java.util.List;

import com.ishanitech.ipalika.dto.LgDistrictDTO;
import com.ishanitech.ipalika.dto.LgMunicipalityDTO;
import com.ishanitech.ipalika.dto.LgProvinceDTO;
import com.ishanitech.ipalika.dto.LgWardDTO;

public interface LocalGovernmentService {

	List<LgProvinceDTO> getAllProvinces();

	List<LgDistrictDTO> getAllDistricts();

	List<LgMunicipalityDTO> getAllMunicipalities();

	List<LgWardDTO> getAllWards();

	List<LgDistrictDTO> getDistrictsByProvinceId(int provinceId);

	List<LgMunicipalityDTO> getMunicipalitiesByDistrictId(int districtId);

	List<LgWardDTO> getWardsByMunicipalityId(int municipalityId);

}

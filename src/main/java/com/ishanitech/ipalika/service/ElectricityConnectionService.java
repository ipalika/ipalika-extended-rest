package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dao.FormStatusUpdationInfo;
import com.ishanitech.ipalika.dto.ElectricityConnectionDTO;
import com.ishanitech.ipalika.security.CustomUserDetails;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ElectricityConnectionService {

    void addElectricityRegistration(ElectricityConnectionDTO electricityRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<ElectricityConnectionDTO> getElectricityRegistrations(HttpServletRequest request);

    ElectricityConnectionDTO getElectricityCertificateByTokenId(String tokenId);

    void updateElectricityCertificateByTokenId(ElectricityConnectionDTO electricityRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId, String regNo, String pdfLocation, @AuthenticationPrincipal CustomUserDetails user);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.MarriageCertificateDTO;
import com.ishanitech.ipalika.dto.ResponseDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface MarriageService {

    void addMarriageRegistration(MarriageCertificateDTO marriageRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<MarriageCertificateDTO> getMarriageRegistration(HttpServletRequest request);

    MarriageCertificateDTO getMarriageCertificateByTokenId(String tokenId);

    void updateMarriageCertificateByTokenId(MarriageCertificateDTO marriageRegistrationInfo,String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);

}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.CitizenshipAndCopyDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CitizenshipAndCopyService {

    void addCitizenshipRegistration(CitizenshipAndCopyDTO citizenshipRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<CitizenshipAndCopyDTO> getCitizenshipRegistrations(HttpServletRequest request);

    CitizenshipAndCopyDTO getCitizenshipCertificateByTokenId(String tokenId);

    void updateCitizenshipCertificateByTokenId(CitizenshipAndCopyDTO citizenshipRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

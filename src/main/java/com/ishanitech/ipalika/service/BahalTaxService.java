package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.BahalTaxDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BahalTaxService {

    void addBahalTaxRegistration(BahalTaxDTO bahalRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<BahalTaxDTO> getBahalTaxRegistrations(HttpServletRequest request);

    BahalTaxDTO getBahalTaxCertificateByTokenId(String tokenId);

    void updateBahalTaxCertificateByTokenId(BahalTaxDTO bahalRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

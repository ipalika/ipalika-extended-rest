package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.GharPatalDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface GharPatalService {

    void addGharPatalVerification(GharPatalDTO gharVerificationInfo);

    public void addAllImages(MultipartFile image);

    List<GharPatalDTO> getGharPatalVerifications(HttpServletRequest request);

    GharPatalDTO getGharPatalVerificationByTokenId(String tokenId);

    void updateGharPatalVerificationByTokenId(GharPatalDTO gharVerificationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.MinorCertificateDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface MinorService {

    void addMinorRegistration(MinorCertificateDTO minorRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<MinorCertificateDTO> getMinorRegistrations(HttpServletRequest request);

    MinorCertificateDTO getMinorCertificateByTokenId(String tokenId);

    void updateMinorCertificateByTokenId(MinorCertificateDTO minorRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

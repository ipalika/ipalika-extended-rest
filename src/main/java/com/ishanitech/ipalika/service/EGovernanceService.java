package com.ishanitech.ipalika.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.RelationCertificateDTO;

public interface EGovernanceService {

	List<BirthCertificateDTO> getBirthRegistrations(HttpServletRequest request);

	void addBirthRegistration(BirthCertificateDTO birthRegistrationInfo);

	BirthCertificateDTO getBirthCertificateByTokenId(String tokenId);

	void updateBirthCertificateByTokenId(BirthCertificateDTO birthRegistrationInfo, String tokenId);

	public void addBirthCertificateImage(MultipartFile image);

	List<RelationCertificateDTO> getRelationRegistrations(HttpServletRequest request);

	void addRelationRegistration(RelationCertificateDTO relationRegistrationInfo);

	RelationCertificateDTO getRelationCertificateByTokenId(String tokenId);

	void updateRelationCertificateByTokenId(RelationCertificateDTO relationRegistrationInfo, String tokenId);

	void uploadRelationCertificate(MultipartFile image);

	void updateBirthCertificateStatus(String tokenId, Integer status, Integer formInfoId);

	void updateRelationCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.PoorEconomyDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PoorEconomyService {

    void addPoorEconomyRegistration(PoorEconomyDTO economyRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<PoorEconomyDTO> getPoorEconomyRegistrations(HttpServletRequest request);

    PoorEconomyDTO getPoorEconomyCertificateByTokenId(String tokenId);

    void updatePoorEconomyCertificateByTokenId(PoorEconomyDTO economyRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

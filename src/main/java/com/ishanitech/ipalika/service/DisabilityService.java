package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.DisabilityCertificateDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface DisabilityService {

    void addDisabilityRegistration(DisabilityCertificateDTO disabilityRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<DisabilityCertificateDTO> getDisabilityRegistrations(HttpServletRequest request);

    DisabilityCertificateDTO getDisabilityCertificateByTokenId(String tokenId);

    void updateDisabilityCertificateByTokenId(DisabilityCertificateDTO disabilityRegistrationInfo, String tokenId);

    List<String> getDifferentlyAbledTypes();

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

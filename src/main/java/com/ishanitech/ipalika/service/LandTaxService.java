package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.LandTaxDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LandTaxService {

    void addLandRegistration(LandTaxDTO landRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<LandTaxDTO> getLandRegistrations(HttpServletRequest request);

    LandTaxDTO getLandCertificateByTokenId(String tokenId);

    void updateLandCertificateByTokenId(LandTaxDTO landRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

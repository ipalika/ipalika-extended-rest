package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.RightfulPersonDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface RightfulPersonService {

    void addRightfulRegistration(RightfulPersonDTO rightfulRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<RightfulPersonDTO> getRightfulRegistrations(HttpServletRequest request);

    RightfulPersonDTO getRightfulCertificateByTokenId(String tokenId);

    void updateRightfulCertificateByTokenId(RightfulPersonDTO rightfulRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.FreeHealthDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FreeHealthService {

    void addFreeHealthRegistration(FreeHealthDTO healthRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<FreeHealthDTO> getFreeHealthRegistrations(HttpServletRequest request);

    FreeHealthDTO getFreeHealthCertificateByTokenId(String tokenId);

    void updateFreeHealthCertificateByTokenId(FreeHealthDTO healthRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.ResettlementRegisterDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ResettlementRegisterService {

    void addResettlementRegistration(ResettlementRegisterDTO resettlementRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<ResettlementRegisterDTO> getResettlementRegistrations(HttpServletRequest request);

    ResettlementRegisterDTO getResettlementCertificateByTokenId(String tokenId);

    void updateResettlementCertificateByTokenId(ResettlementRegisterDTO resettlementRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

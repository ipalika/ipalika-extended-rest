package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.KothaKholneDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface KothaKholneService {

    void addKothaRegistration(KothaKholneDTO kothaRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<KothaKholneDTO> getKothaRegistrations(HttpServletRequest request);

    KothaKholneDTO getKothaCertificateByTokenId(String tokenId);

    void updateKothaCertificateByTokenId(KothaKholneDTO kothaRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

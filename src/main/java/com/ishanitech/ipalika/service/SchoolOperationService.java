package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.SchoolOperationDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface SchoolOperationService {


    void addSchoolRegistration(SchoolOperationDTO schoolRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<SchoolOperationDTO> getSchoolRegistrations(HttpServletRequest request);

    SchoolOperationDTO getSchoolCertificateByTokenId(String tokenId);

    void updateSchoolCertificateByTokenId(SchoolOperationDTO schoolRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.ManjurinamaVerificationDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ManjurinamaVerificationService {

    void addManjurinamaVerification(ManjurinamaVerificationDTO manjurinamaVerificationInfo);

    public void addAllImages(MultipartFile image);

    List<ManjurinamaVerificationDTO> getManjurinamaVerifications(HttpServletRequest request);

    ManjurinamaVerificationDTO getManjurinamaVerificationByTokenId(String tokenId);

    void updateManjurinamaVerificationByTokenId(ManjurinamaVerificationDTO manjurinamaVerificationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

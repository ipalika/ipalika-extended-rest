package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.LandValuationDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LandValuationService {

    void addLandRegistration(LandValuationDTO landRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<LandValuationDTO> getLandRegistrations(HttpServletRequest request);

    LandValuationDTO getLandCertificateByTokenId(String tokenId);

    void updateLandCertificateByTokenId(LandValuationDTO landRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.GuardianInstitutionalDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface GuardianInstitutionalService {

    void addGuardianRegistration(GuardianInstitutionalDTO guardianRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<GuardianInstitutionalDTO> getGuardianRegistrations(HttpServletRequest request);

    GuardianInstitutionalDTO getGuardianCertificateByTokenId(String tokenId);

    void updateGuardianCertificateByTokenId(GuardianInstitutionalDTO guardianRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.DivorceCertificateDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface DivorceService {


    void addDivorceRegistration(DivorceCertificateDTO divorceRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<DivorceCertificateDTO> getDivorceRegistrations(HttpServletRequest request);

    DivorceCertificateDTO getDivorceCertificateByTokenId(String tokenId);

    void updateDivorceCertificateByTokenId(DivorceCertificateDTO divorceRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);

}

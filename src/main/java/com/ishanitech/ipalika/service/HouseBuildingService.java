package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.HouseBuildingDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface HouseBuildingService {

    void addHouseBuildingRegistration(HouseBuildingDTO houseRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<HouseBuildingDTO> getHouseBuildingRegistrations(HttpServletRequest request);

    HouseBuildingDTO getHouseBuildingCertificateByTokenId(String tokenId);

    void updateHouseBuildingCertificateByTokenId(HouseBuildingDTO houseRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

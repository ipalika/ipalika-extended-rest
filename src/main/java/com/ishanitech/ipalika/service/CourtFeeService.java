package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.CourtFeeDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CourtFeeService {


    void addCourtRegistration(CourtFeeDTO courtRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<CourtFeeDTO> getCourtRegistrations(HttpServletRequest request);

    CourtFeeDTO getCourtCertificateByTokenId(String tokenId);

    void updateCourtCertificateByTokenId(CourtFeeDTO courtRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

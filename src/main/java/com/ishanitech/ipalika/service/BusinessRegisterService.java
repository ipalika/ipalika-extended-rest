package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.BusinessRegisterDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BusinessRegisterService {

    void addBusinessRegistration(BusinessRegisterDTO businessRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<BusinessRegisterDTO> getBusinessRegistrations(HttpServletRequest request);

    BusinessRegisterDTO getBusinessCertificateByTokenId(String tokenId);

    void updateBusinessCertificateByTokenId(BusinessRegisterDTO businessRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

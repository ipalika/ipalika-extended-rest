package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.ReconciliationPaperDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ReconciliationPaperService {

    void addReconciliationRegistration(ReconciliationPaperDTO reconciliationRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<ReconciliationPaperDTO> getReconciliationRegistrations(HttpServletRequest request);

    ReconciliationPaperDTO getReconciliationCertificateByTokenId(String tokenId);

    void updateReconciliationCertificateByTokenId(ReconciliationPaperDTO reconciliationRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

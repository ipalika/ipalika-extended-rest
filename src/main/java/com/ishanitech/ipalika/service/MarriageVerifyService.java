package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.MarriageVerifyDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface MarriageVerifyService {

    void addMarriageVerification(MarriageVerifyDTO marriageVerificationInfo);

    public void addAllImages(MultipartFile image);

    List<MarriageVerifyDTO> getMarriageVerifications(HttpServletRequest request);

    MarriageVerifyDTO getMarriageVerificationByTokenId(String tokenId);

    void updateMarriageVerificationByTokenId(MarriageVerifyDTO marriageVerificationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);

}

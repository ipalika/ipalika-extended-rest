package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.BusinessDisclosureDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BusinessDisclosureService {

    void addBusinessRegistration(BusinessDisclosureDTO businessRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<BusinessDisclosureDTO> getBusinessRegistrations(HttpServletRequest request);

    BusinessDisclosureDTO getBusinessCertificateByTokenId(String tokenId);

    void updateBusinessCertificateByTokenId(BusinessDisclosureDTO businessRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.PropertyTaxDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PropertyTaxService {

    void addPropertyTaxRegistration(PropertyTaxDTO propertyRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<PropertyTaxDTO> getPropertyTaxRegistrations(HttpServletRequest request);

    PropertyTaxDTO getPropertyTaxCertificateByTokenId(String tokenId);

    void updatePropertyTaxCertificateByTokenId(PropertyTaxDTO propertyRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

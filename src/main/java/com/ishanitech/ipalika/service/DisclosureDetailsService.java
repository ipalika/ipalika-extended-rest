package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.DisclosureDetailsDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface DisclosureDetailsService {

    void addDisclosureRegistration(DisclosureDetailsDTO disclosureRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<DisclosureDetailsDTO> getDisclosureRegistrations(HttpServletRequest request);

    DisclosureDetailsDTO getDisclosureCertificateByTokenId(String tokenId);

    void updateDisclosureCertificateByTokenId(DisclosureDetailsDTO disclosureRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.EnglishVerificationDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface EnglishVerificationService {

    void addEnglishVerification(EnglishVerificationDTO englishVerificationInfo);

    public void addAllImages(MultipartFile image);

    List<EnglishVerificationDTO> getEnglishVerifications(HttpServletRequest request);

    EnglishVerificationDTO getEnglishVerificationByTokenId(String tokenId);

    void updateEnglishVerificationByTokenId(EnglishVerificationDTO englishVerificationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

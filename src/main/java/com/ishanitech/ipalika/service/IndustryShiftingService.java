package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.IndustryShiftingDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface IndustryShiftingService {

    void addIndustryRegistration(IndustryShiftingDTO industryRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<IndustryShiftingDTO> getIndustryRegistrations(HttpServletRequest request);

    IndustryShiftingDTO getIndustryCertificateByTokenId(String tokenId);

    void updateIndustryCertificateByTokenId(IndustryShiftingDTO industryRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.LandPurjaLostDAO;
import com.ishanitech.ipalika.dto.LandPurjaLostDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.LandPurjaLostService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class LandPurjaLostServiceImpl implements LandPurjaLostService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public LandPurjaLostServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addLandRegistration(LandPurjaLostDTO landRegistrationInfo) {
        try {
            dbService.getDao(LandPurjaLostDAO.class).addNewEntity(landRegistrationInfo);

            String message = "";

            message = "Your request for 'Land Dhani-Purja Lost Registration' Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + landRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, landRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<LandPurjaLostDTO> getLandRegistrations(HttpServletRequest request) {
        List<LandPurjaLostDTO> landReg = new ArrayList<>();
        try {
            landReg = dbService.getDao(LandPurjaLostDAO.class).getLandRegistrations();
            return landReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public LandPurjaLostDTO getLandCertificateByTokenId(String tokenId) {
        LandPurjaLostDTO landInfo = new LandPurjaLostDTO();
        landInfo = dbService.getDao(LandPurjaLostDAO.class).getLandCertificateByTokenId(tokenId);
        if(landInfo.getWardSecretarySignature() != null){
            landInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getWardSecretarySignature()));
        }
        landInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getApplicationPhoto()));
        landInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getCitizenshipPhoto()));
        landInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getLandOwnershipCertificate()));
        landInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getSarjaminMuchulka()));
        landInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getTaxInformationPhoto()));

        return landInfo;
    }


    @Override
    public void updateLandCertificateByTokenId(LandPurjaLostDTO landRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(LandPurjaLostDAO.class).updateLandRegistrationByTokenId(landRegistrationInfo, tokenId);
            dbService.getDao(LandPurjaLostDAO.class).approveLandRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(LandPurjaLostDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.DisabilityDAO;
import com.ishanitech.ipalika.dto.DisabilityCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.DisabilityService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class DisabilityServiceImpl implements DisabilityService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public DisabilityServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addDisabilityRegistration(DisabilityCertificateDTO disabilityRegistrationInfo) {
        try {
            dbService.getDao(DisabilityDAO.class).addNewEntity(disabilityRegistrationInfo);

            String message = "";

            message = "Your request for disability registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + disabilityRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, disabilityRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<DisabilityCertificateDTO> getDisabilityRegistrations(HttpServletRequest request) {
        List<DisabilityCertificateDTO> disabilityReg = new ArrayList<>();
        try {
            disabilityReg = dbService.getDao(DisabilityDAO.class).getDisabilityRegistrations();
            return disabilityReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public DisabilityCertificateDTO getDisabilityCertificateByTokenId(String tokenId) {
        DisabilityCertificateDTO disabilityInfo = new DisabilityCertificateDTO();
        disabilityInfo = dbService.getDao(DisabilityDAO.class).getDisabilityRegistrationByTokenId(tokenId);
        if(disabilityInfo.getWardSecretarySignature() != null){
            disabilityInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, disabilityInfo.getWardSecretarySignature()));
        }
        disabilityInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, disabilityInfo.getApplicationPhoto()));
        disabilityInfo.setDisabilityCertificatePhoto(ImageUtilService.makeFullImageurl(restUrlProperty, disabilityInfo.getDisabilityCertificatePhoto()));

        return disabilityInfo;
    }


    @Override
    public void updateDisabilityCertificateByTokenId(DisabilityCertificateDTO disabilityRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(DisabilityDAO.class).updateDisabilityRegistrationByTokenId(disabilityRegistrationInfo, tokenId);
            dbService.getDao(DisabilityDAO.class).approveDisabilityRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public List<String> getDifferentlyAbledTypes() {
        return dbService.getDao(DisabilityDAO.class).getListofDifferentlyAbled();
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(DisabilityDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.GharPatalDAO;
import com.ishanitech.ipalika.dto.GharPatalDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.GharPatalService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class GharPatalServiceImpl implements GharPatalService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public GharPatalServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService) {
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public List<GharPatalDTO> getGharPatalVerifications(HttpServletRequest request) {
        List<GharPatalDTO> gharReg = new ArrayList<>();
        try {
            gharReg = dbService.getDao(GharPatalDAO.class).getGharPatalVerifications();
            return gharReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }

    }


    @Override
    public void addGharPatalVerification(GharPatalDTO gharVerificationInfo) {
        try {
            dbService.getDao(GharPatalDAO.class).addNewEntity(gharVerificationInfo);

            String message = "";

            message = "Your request for Ghar Patal Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + gharVerificationInfo.getTokenId() + ".";
            mailService.sendEmail(message, gharVerificationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image) {
        fileUtilService.storeFile(image);
    }


    @Override
    public GharPatalDTO getGharPatalVerificationByTokenId(String tokenId){
        GharPatalDTO gharInfo = new GharPatalDTO();
        gharInfo = dbService.getDao(GharPatalDAO.class).getGharPatalVerificationByTokenId(tokenId);
        if(gharInfo.getWardSecretarySignature() != null){
            gharInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getWardSecretarySignature()));
        }
        gharInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getApplicationPhoto()));
        gharInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getCitizenshipPhoto()));
        gharInfo.setHouseMap(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getHouseMap()));
        gharInfo.setMapPassCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getMapPassCertificate()));
        gharInfo.setOnsiteInspectionReport(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getOnsiteInspectionReport()));
        gharInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getSarjaminMuchulka()));
        gharInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getTaxInformationPhoto()));

        return gharInfo;
    }


    public void updateGharPatalVerificationByTokenId(GharPatalDTO gharVerificationInfo,String tokenId){
        try{
            dbService.getDao(GharPatalDAO.class).updateGharPatalVerificationByTokenId(gharVerificationInfo, tokenId);
            dbService.getDao(GharPatalDAO.class).approveGharPatalVerification(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(GharPatalDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

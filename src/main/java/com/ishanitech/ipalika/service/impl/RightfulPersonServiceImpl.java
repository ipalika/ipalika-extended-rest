package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.RightfulPersonDAO;
import com.ishanitech.ipalika.dto.RightfulPersonDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.RightfulPersonService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import lombok.extern.slf4j.Slf4j;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class RightfulPersonServiceImpl implements RightfulPersonService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public RightfulPersonServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addRightfulRegistration(RightfulPersonDTO rightfulRegistrationInfo) {
        try {
            dbService.getDao(RightfulPersonDAO.class).addNewEntity(rightfulRegistrationInfo);

            String message = "";

            message = "Your request for Rightful Person Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + rightfulRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, rightfulRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<RightfulPersonDTO> getRightfulRegistrations(HttpServletRequest request) {
        List<RightfulPersonDTO> rightfulReg = new ArrayList<>();
        try {
            rightfulReg = dbService.getDao(RightfulPersonDAO.class).getRightfulRegistrations();
            return rightfulReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public RightfulPersonDTO getRightfulCertificateByTokenId(String tokenId) {
        RightfulPersonDTO rightfulInfo = new RightfulPersonDTO();
        rightfulInfo = dbService.getDao(RightfulPersonDAO.class).getRightfulCertificateByTokenId(tokenId);
        if(rightfulInfo.getWardSecretarySignature() != null){
            rightfulInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getWardSecretarySignature()));
        }
        rightfulInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getApplicationPhoto()));
        rightfulInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getCitizenshipPhoto()));
        rightfulInfo.setRelationshipProofCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getRelationshipProofCertificate()));
        rightfulInfo.setOnsiteSarjamin(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getOnsiteSarjamin()));
        rightfulInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getTaxInformationPhoto()));
        rightfulInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getSarjaminMuchulka()));
        rightfulInfo.setProofDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getProofDocument1()));
        rightfulInfo.setProofDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getProofDocument2()));
        rightfulInfo.setProofDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, rightfulInfo.getProofDocument3()));

        return rightfulInfo;
    }


    @Override
    public void updateRightfulCertificateByTokenId(RightfulPersonDTO rightfulRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(RightfulPersonDAO.class).updateRightfulRegistrationByTokenId(rightfulRegistrationInfo, tokenId);
            dbService.getDao(RightfulPersonDAO.class).approveRightfulRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(RightfulPersonDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.ScholarshipDAO;
import com.ishanitech.ipalika.dto.ScholarshipCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.ScholarshipService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScholarshipServiceImpl implements ScholarshipService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public ScholarshipServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addScholarshipRegistration(ScholarshipCertificateDTO scholarshipRegistrationInfo) {
        try {
            dbService.getDao(ScholarshipDAO.class).addNewEntity(scholarshipRegistrationInfo);

            String message = "";

            message = "Your request for scholarship registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + scholarshipRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, scholarshipRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<ScholarshipCertificateDTO> getScholarshipRegistrations(HttpServletRequest request) {
        List<ScholarshipCertificateDTO> scholarshipReg = new ArrayList<>();
        try {
            scholarshipReg = dbService.getDao(ScholarshipDAO.class).getScholarshipRegistrations();
            return scholarshipReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public ScholarshipCertificateDTO getScholarshipCertificateByTokenId(String tokenId) {
        ScholarshipCertificateDTO scholarshipInfo = new ScholarshipCertificateDTO();
        scholarshipInfo = dbService.getDao(ScholarshipDAO.class).getScholarshipRegistrationByTokenId(tokenId);
        if(scholarshipInfo.getWardSecretarySignature() != null){
            scholarshipInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, scholarshipInfo.getWardSecretarySignature()));
        }
        scholarshipInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, scholarshipInfo.getApplicationPhoto()));
        scholarshipInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, scholarshipInfo.getCitizenshipPhoto()));
        scholarshipInfo.setEduQualificationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, scholarshipInfo.getEduQualificationCertificate()));

        return scholarshipInfo;
    }


    @Override
    public void updateScholarshipCertificateByTokenId(ScholarshipCertificateDTO scholarshipRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(ScholarshipDAO.class).updateScholarshipRegistrationByTokenId(scholarshipRegistrationInfo, tokenId);
            dbService.getDao(ScholarshipDAO.class).approveScholarshipRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(ScholarshipDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

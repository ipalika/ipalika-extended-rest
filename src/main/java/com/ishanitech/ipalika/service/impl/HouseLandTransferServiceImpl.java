package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.HouseLandTransferDAO;
import com.ishanitech.ipalika.dto.HouseLandTransferDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.HouseLandTransferService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class HouseLandTransferServiceImpl implements HouseLandTransferService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public HouseLandTransferServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addHouseLandRegistration(HouseLandTransferDTO houseRegistrationInfo) {
        try {
            dbService.getDao(HouseLandTransferDAO.class).addNewEntity(houseRegistrationInfo);

            String message = "";

            message = "Your request for House Land Transfer Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + houseRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, houseRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<HouseLandTransferDTO> getHouseLandRegistrations(HttpServletRequest request) {
        List<HouseLandTransferDTO> houseReg = new ArrayList<>();
        try {
            houseReg = dbService.getDao(HouseLandTransferDAO.class).getHouseLandRegistrations();
            return houseReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public HouseLandTransferDTO getHouseLandCertificateByTokenId(String tokenId) {
        HouseLandTransferDTO houseInfo = new HouseLandTransferDTO();
        houseInfo = dbService.getDao(HouseLandTransferDAO.class).getHouseLandCertificateByTokenId(tokenId);
        if(houseInfo.getWardSecretarySignature() != null){
            houseInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getWardSecretarySignature()));
        }
        houseInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getApplicationPhoto()));
        houseInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getCitizenshipPhoto()));
        houseInfo.setRelationshipCertificateOfDeceasedPetitioner(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getRelationshipCertificateOfDeceasedPetitioner()));
        houseInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getLandOwnershipCertificate()));
        houseInfo.setSarjaminWitnessCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getSarjaminWitnessCitizenshipPhoto()));

        return houseInfo;
    }


    @Override
    public void updateHouseLandCertificateByTokenId(HouseLandTransferDTO houseRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(HouseLandTransferDAO.class).updateHouseLandRegistrationByTokenId(houseRegistrationInfo, tokenId);
            dbService.getDao(HouseLandTransferDAO.class).approveHouseLandRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(HouseLandTransferDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

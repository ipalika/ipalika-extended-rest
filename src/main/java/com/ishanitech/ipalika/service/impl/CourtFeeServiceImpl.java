package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.CourtFeeDAO;
import com.ishanitech.ipalika.dto.CourtFeeDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.CourtFeeService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class CourtFeeServiceImpl implements CourtFeeService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public CourtFeeServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addCourtRegistration(CourtFeeDTO courtRegistrationInfo) {
        try {
            dbService.getDao(CourtFeeDAO.class).addNewEntity(courtRegistrationInfo);

            String message = "";

            message = "Your request for Court Fee Deduction Recommendation Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + courtRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, courtRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<CourtFeeDTO> getCourtRegistrations(HttpServletRequest request) {
        List<CourtFeeDTO> courtReg = new ArrayList<>();
        try {
            courtReg = dbService.getDao(CourtFeeDAO.class).getCourtRegistrations();
            return courtReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public CourtFeeDTO getCourtCertificateByTokenId(String tokenId) {
        CourtFeeDTO courtInfo = new CourtFeeDTO();
        courtInfo = dbService.getDao(CourtFeeDAO.class).getCourtCertificateByTokenId(tokenId);
        if(courtInfo.getWardSecretarySignature() != null){
            courtInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getWardSecretarySignature()));
        }
        courtInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getApplicationPhoto()));
        courtInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getCitizenshipPhoto()));
        courtInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getTaxInformationPhoto()));
        courtInfo.setEvidenceDocumentFiledInCourt1(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getEvidenceDocumentFiledInCourt1()));
        courtInfo.setEvidenceDocumentFiledInCourt2(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getEvidenceDocumentFiledInCourt2()));
        courtInfo.setWrittenReasonForWaivingFee(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getWrittenReasonForWaivingFee()));
        courtInfo.setLocalSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, courtInfo.getLocalSarjaminMuchulka()));

        return courtInfo;
    }


    @Override
    public void updateCourtCertificateByTokenId(CourtFeeDTO courtRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(CourtFeeDAO.class).updateCourtRegistrationByTokenId(courtRegistrationInfo, tokenId);
            dbService.getDao(CourtFeeDAO.class).approveCourtRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(CourtFeeDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

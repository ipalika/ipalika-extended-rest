package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.ElectricityConnectionDAO;
import com.ishanitech.ipalika.dto.ElectricityConnectionDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.security.CustomUserDetails;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.ElectricityConnectionService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class ElectricityConnectionServiceImpl implements ElectricityConnectionService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public ElectricityConnectionServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addElectricityRegistration(ElectricityConnectionDTO electricityRegistrationInfo) {
        try {
            dbService.getDao(ElectricityConnectionDAO.class).addNewEntity(electricityRegistrationInfo);

            String message = "";

            message = "Your request for Electricity Connection Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + electricityRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, electricityRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<ElectricityConnectionDTO> getElectricityRegistrations(HttpServletRequest request) {
        List<ElectricityConnectionDTO> electricityReg = new ArrayList<>();
        try {
            electricityReg = dbService.getDao(ElectricityConnectionDAO.class).getElectricityRegistrations();
            return electricityReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public ElectricityConnectionDTO getElectricityCertificateByTokenId(String tokenId) {
        ElectricityConnectionDTO electricityInfo = new ElectricityConnectionDTO();
        electricityInfo = dbService.getDao(ElectricityConnectionDAO.class).getElectricityCertificateByTokenId(tokenId);
        if(electricityInfo.getWardSecretarySignature() != null){
            electricityInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getWardSecretarySignature()));
        }
        electricityInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getApplicationPhoto()));
        electricityInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getCitizenshipPhoto()));
        electricityInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getLandOwnershipCertificate()));
        electricityInfo.setSourceOfRightDocument(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getSourceOfRightDocument()));
        electricityInfo.setProofOfMapPass(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getProofOfMapPass()));
        electricityInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getTaxInformationPhoto()));
        electricityInfo.setOtherDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getOtherDocument1()));
        electricityInfo.setOtherDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getOtherDocument2()));
        electricityInfo.setOtherDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, electricityInfo.getOtherDocument3()));

        return electricityInfo;
    }


    @Override
    public void updateElectricityCertificateByTokenId(ElectricityConnectionDTO electricityRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(ElectricityConnectionDAO.class).updateElectricityRegistrationByTokenId(electricityRegistrationInfo, tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId, String regNo, String pdfLocation, @AuthenticationPrincipal CustomUserDetails user ) {
        ElectricityConnectionDAO electricityConnectionDAO = dbService.getDao(ElectricityConnectionDAO.class);
        try {
            dbService.getDao(ElectricityConnectionDAO.class).updateStatus(tokenId, status, formInfoId, regNo);
            switch(status) {
                case 2:
                    electricityConnectionDAO.updateAuthorityColumn("ward_admin_name_eng", user.getUser().getFullName());
                    break;
                case 3:
                    // code block
                    electricityConnectionDAO.updateAuthorityColumn("registrar_name_eng", user.getUser().getFullName());
                    break;
                case 4:
                    electricityConnectionDAO.updateAuthorityColumn("ward_secretary_name", user.getUser().getFullName());
                    break;
                case 5:
                    int approved = electricityConnectionDAO.approveElectricityRegistration(tokenId);
                    if(approved == 1){
                        ElectricityConnectionDTO electricityInfo = new ElectricityConnectionDTO();
                        electricityInfo = dbService.getDao(ElectricityConnectionDAO.class).getElectricityCertificateByTokenId(tokenId);
                        String message = "";

                        message = "Your request for Electricity Connection Registration Certificate has been successfully verified "
                                +" please download this attachment send with you and you can print this or you can vist to our office"
                                + " Your registration number is "+ electricityInfo.getRegistrationNo() +""
                                + " Your unique token id is " + electricityInfo.getTokenId() + ".";


                        mailService.sendAccountVerifyEmail(message, electricityInfo.getEmail(), "/home/suraj/Documents/ishanitech/ipalika-extended/upload-dir/verificationLetter.pdf");
                    }
                    break;
            }


        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

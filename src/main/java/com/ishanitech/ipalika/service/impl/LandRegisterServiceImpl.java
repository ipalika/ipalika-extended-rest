package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.LandRegisterDAO;
import com.ishanitech.ipalika.dto.LandRegisterDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.LandRegisterService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class LandRegisterServiceImpl implements LandRegisterService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public LandRegisterServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addLandRegistration(LandRegisterDTO landRegistrationInfo) {
        try {
            dbService.getDao(LandRegisterDAO.class).addNewEntity(landRegistrationInfo);

            String message = "";

            message = "Your request for Land Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + landRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, landRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<LandRegisterDTO> getLandRegistrations(HttpServletRequest request) {
        List<LandRegisterDTO> landReg = new ArrayList<>();
        try {
            landReg = dbService.getDao(LandRegisterDAO.class).getLandRegistrations();
            return landReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public LandRegisterDTO getLandCertificateByTokenId(String tokenId) {
        LandRegisterDTO landInfo = new LandRegisterDTO();
        landInfo = dbService.getDao(LandRegisterDAO.class).getLandCertificateByTokenId(tokenId);
        if(landInfo.getWardSecretarySignature() != null){
            landInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getWardSecretarySignature()));
        }
        landInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getApplicationPhoto()));
        landInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getCitizenshipPhoto()));
        landInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getTaxInformationPhoto()));
        landInfo.setCopyOfPreviousData(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getCopyOfPreviousData()));
        landInfo.setFieldBook(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getFieldBook()));
        landInfo.setOnsiteInspectionReport(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getOnsiteInspectionReport()));
        landInfo.setLandSurveyMap(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getLandSurveyMap()));
        landInfo.setProofDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getProofDocument1()));
        landInfo.setProofDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getProofDocument2()));
        landInfo.setProofDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getProofDocument3()));
        landInfo.setLocalSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getLocalSarjaminMuchulka()));

        return landInfo;
    }


    @Override
    public void updateLandCertificateByTokenId(LandRegisterDTO landRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(LandRegisterDAO.class).updateLandRegistrationByTokenId(landRegistrationInfo, tokenId);
            dbService.getDao(LandRegisterDAO.class).approveLandRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(LandRegisterDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.PoorEconomyDAO;
import com.ishanitech.ipalika.dto.PoorEconomyDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.PoorEconomyService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class PoorEconomyServiceImpl implements PoorEconomyService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public PoorEconomyServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addPoorEconomyRegistration(PoorEconomyDTO economyRegistrationInfo) {
        try {
            dbService.getDao(PoorEconomyDAO.class).addNewEntity(economyRegistrationInfo);

            String message = "";

            message = "Your request for Poor Economic Condition Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + economyRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, economyRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<PoorEconomyDTO> getPoorEconomyRegistrations(HttpServletRequest request) {
        List<PoorEconomyDTO> economyReg = new ArrayList<>();
        try {
            economyReg = dbService.getDao(PoorEconomyDAO.class).getPoorEconomyRegistrations();
            return economyReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public PoorEconomyDTO getPoorEconomyCertificateByTokenId(String tokenId) {
        PoorEconomyDTO economyInfo = new PoorEconomyDTO();
        economyInfo = dbService.getDao(PoorEconomyDAO.class).getPoorEconomyCertificateByTokenId(tokenId);
        if(economyInfo.getWardSecretarySignature() != null){
            economyInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getWardSecretarySignature()));
        }
        economyInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getApplicationPhoto()));
        economyInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getCitizenshipPhoto()));
        economyInfo.setDocumentConfirmingPoor1(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getDocumentConfirmingPoor1()));
        economyInfo.setDocumentConfirmingPoor2(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getDocumentConfirmingPoor2()));
        economyInfo.setDocumentConfirmingPoor3(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getDocumentConfirmingPoor3()));

        return economyInfo;
    }


    @Override
    public void updatePoorEconomyCertificateByTokenId(PoorEconomyDTO economyRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(PoorEconomyDAO.class).updatePoorEconomyRegistrationByTokenId(economyRegistrationInfo, tokenId);
            dbService.getDao(PoorEconomyDAO.class).approvePoorEconomyRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(PoorEconomyDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

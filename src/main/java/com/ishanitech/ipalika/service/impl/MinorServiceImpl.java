package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.MinorDAO;
import com.ishanitech.ipalika.dto.MinorCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.MinorService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class MinorServiceImpl implements MinorService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public MinorServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addMinorRegistration(MinorCertificateDTO minorRegistrationInfo) {
        try {
            dbService.getDao(MinorDAO.class).addNewEntity(minorRegistrationInfo);

            String message = "";

            message = "Your request for minor registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + minorRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, minorRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<MinorCertificateDTO> getMinorRegistrations(HttpServletRequest request) {
        List<MinorCertificateDTO> minorReg = new ArrayList<>();
        try {
            minorReg = dbService.getDao(MinorDAO.class).getMinorRegistrations();
            return minorReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public MinorCertificateDTO getMinorCertificateByTokenId(String tokenId) {
        MinorCertificateDTO minorInfo = new MinorCertificateDTO();
        minorInfo = dbService.getDao(MinorDAO.class).getMinorRegistrationByTokenId(tokenId);
        if(minorInfo.getWardSecretarySignature() != null){
            minorInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, minorInfo.getWardSecretarySignature()));
        }
        minorInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, minorInfo.getApplicationPhoto()));
        minorInfo.setBirthCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, minorInfo.getBirthCertificate()));
        minorInfo.setFatherCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, minorInfo.getFatherCitizenshipPhoto()));
        minorInfo.setMotherCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, minorInfo.getMotherCitizenshipPhoto()));
        minorInfo.setPpPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, minorInfo.getPpPhoto()));
        minorInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, minorInfo.getTaxInformationPhoto()));

        return minorInfo;
    }


    @Override
    public void updateMinorCertificateByTokenId(MinorCertificateDTO minorRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(MinorDAO.class).updateMinorRegistrationByTokenId(minorRegistrationInfo, tokenId);
            dbService.getDao(MinorDAO.class).approveMinorRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(MinorDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

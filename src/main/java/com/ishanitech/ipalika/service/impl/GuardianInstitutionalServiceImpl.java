package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.GuardianInstitutionalDAO;
import com.ishanitech.ipalika.dto.GuardianInstitutionalDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.GuardianInstitutionalService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class GuardianInstitutionalServiceImpl implements GuardianInstitutionalService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public GuardianInstitutionalServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addGuardianRegistration(GuardianInstitutionalDTO guardianRegistrationInfo) {
        try {
            dbService.getDao(GuardianInstitutionalDAO.class).addNewEntity(guardianRegistrationInfo);

            String message = "";

            message = "Your request for Guardian Recommendation (Institutional) Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + guardianRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, guardianRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<GuardianInstitutionalDTO> getGuardianRegistrations(HttpServletRequest request) {
        List<GuardianInstitutionalDTO> guardianReg = new ArrayList<>();
        try {
            guardianReg = dbService.getDao(GuardianInstitutionalDAO.class).getGuardianRegistrations();
            return guardianReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public GuardianInstitutionalDTO getGuardianCertificateByTokenId(String tokenId) {
        GuardianInstitutionalDTO guardianInfo = new GuardianInstitutionalDTO();
        guardianInfo = dbService.getDao(GuardianInstitutionalDAO.class).getGuardianCertificateByTokenId(tokenId);
        if(guardianInfo.getWardSecretarySignature() != null){
            guardianInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getWardSecretarySignature()));
        }
        guardianInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getApplicationPhoto()));
        guardianInfo.setInstitutionRenewalCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getInstitutionRenewalCertificate()));
        guardianInfo.setCopyOfLegislation(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getCopyOfLegislation()));
        guardianInfo.setCopyOfRules(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getCopyOfRules()));
        guardianInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getTaxInformationPhoto()));
        guardianInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getBahalAgreementPhoto()));
        guardianInfo.setBahalTaxInformation(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getBahalTaxInformation()));
        guardianInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getSarjaminMuchulka()));

        return guardianInfo;
    }


    @Override
    public void updateGuardianCertificateByTokenId(GuardianInstitutionalDTO guardianRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(GuardianInstitutionalDAO.class).updateGuardianRegistrationByTokenId(guardianRegistrationInfo, tokenId);
            dbService.getDao(GuardianInstitutionalDAO.class).approveGuardianRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(GuardianInstitutionalDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.FreeHealthDAO;
import com.ishanitech.ipalika.dto.FreeHealthDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.FreeHealthService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class FreeHealthServiceImpl implements FreeHealthService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public FreeHealthServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addFreeHealthRegistration(FreeHealthDTO healthRegistrationInfo) {
        try {
            dbService.getDao(FreeHealthDAO.class).addNewEntity(healthRegistrationInfo);

            String message = "";

            message = "Your request for Free HealthCare Recommendation Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + healthRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, healthRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<FreeHealthDTO> getFreeHealthRegistrations(HttpServletRequest request) {
        List<FreeHealthDTO> healthReg = new ArrayList<>();
        try {
            healthReg = dbService.getDao(FreeHealthDAO.class).getFreeHealthRegistrations();
            return healthReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public FreeHealthDTO getFreeHealthCertificateByTokenId(String tokenId) {
        FreeHealthDTO healthInfo = new FreeHealthDTO();
        healthInfo = dbService.getDao(FreeHealthDAO.class).getFreeHealthCertificateByTokenId(tokenId);
        if(healthInfo.getWardSecretarySignature() != null){
            healthInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, healthInfo.getWardSecretarySignature()));
        }
        healthInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, healthInfo.getApplicationPhoto()));
        healthInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, healthInfo.getCitizenshipPhoto()));
        healthInfo.setDocumentProvingPoverty(ImageUtilService.makeFullImageurl(restUrlProperty, healthInfo.getDocumentProvingPoverty()));
        healthInfo.setOtherDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, healthInfo.getOtherDocument1()));
        healthInfo.setOtherDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, healthInfo.getOtherDocument2()));
        healthInfo.setOtherDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, healthInfo.getOtherDocument3()));

        return healthInfo;
    }


    @Override
    public void updateFreeHealthCertificateByTokenId(FreeHealthDTO healthRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(FreeHealthDAO.class).updateFreeHealthRegistrationByTokenId(healthRegistrationInfo, tokenId);
            dbService.getDao(FreeHealthDAO.class).approveFreeHealthRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(FreeHealthDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

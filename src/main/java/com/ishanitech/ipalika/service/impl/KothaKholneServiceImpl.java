package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.KothaKholneDAO;
import com.ishanitech.ipalika.dto.KothaKholneDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.KothaKholneService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class KothaKholneServiceImpl implements KothaKholneService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public KothaKholneServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addKothaRegistration(KothaKholneDTO kothaRegistrationInfo) {
        try {
            dbService.getDao(KothaKholneDAO.class).addNewEntity(kothaRegistrationInfo);

            String message = "";

            message = "Your request for Kotha Kholne Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + kothaRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, kothaRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<KothaKholneDTO> getKothaRegistrations(HttpServletRequest request) {
        List<KothaKholneDTO> kothaReg = new ArrayList<>();
        try {
            kothaReg = dbService.getDao(KothaKholneDAO.class).getKothaRegistrations();
            return kothaReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public KothaKholneDTO getKothaCertificateByTokenId(String tokenId) {
        KothaKholneDTO kothaInfo = new KothaKholneDTO();
        kothaInfo = dbService.getDao(KothaKholneDAO.class).getKothaCertificateByTokenId(tokenId);
        if(kothaInfo.getWardSecretarySignature() != null){
            kothaInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, kothaInfo.getWardSecretarySignature()));
        }
        kothaInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, kothaInfo.getApplicationPhoto()));
        kothaInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, kothaInfo.getTaxInformationPhoto()));
        kothaInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, kothaInfo.getBahalAgreementPhoto()));
        kothaInfo.setDistrictAdminOfficeLetter(ImageUtilService.makeFullImageurl(restUrlProperty, kothaInfo.getDistrictAdminOfficeLetter()));
        kothaInfo.setLocalSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, kothaInfo.getLocalSarjaminMuchulka()));
        kothaInfo.setPoliceSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, kothaInfo.getPoliceSarjaminMuchulka()));

        return kothaInfo;
    }


    @Override
    public void updateKothaCertificateByTokenId(KothaKholneDTO kothaRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(KothaKholneDAO.class).updateKothaRegistrationByTokenId(kothaRegistrationInfo, tokenId);
            dbService.getDao(KothaKholneDAO.class).approveKothaRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(KothaKholneDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

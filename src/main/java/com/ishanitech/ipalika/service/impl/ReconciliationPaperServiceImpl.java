package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.ReconciliationPaperDAO;
import com.ishanitech.ipalika.dto.ReconciliationPaperDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.ReconciliationPaperService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReconciliationPaperServiceImpl implements ReconciliationPaperService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public ReconciliationPaperServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addReconciliationRegistration(ReconciliationPaperDTO reconciliationRegistrationInfo) {
        try {
            dbService.getDao(ReconciliationPaperDAO.class).addNewEntity(reconciliationRegistrationInfo);

            String message = "";

            message = "Your request for Reconciliation Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + reconciliationRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, reconciliationRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<ReconciliationPaperDTO> getReconciliationRegistrations(HttpServletRequest request) {
        List<ReconciliationPaperDTO> reconciliationReg = new ArrayList<>();
        try {
            reconciliationReg = dbService.getDao(ReconciliationPaperDAO.class).getReconciliationRegistrations();
            return reconciliationReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public ReconciliationPaperDTO getReconciliationCertificateByTokenId(String tokenId) {
        ReconciliationPaperDTO reconciliationInfo = new ReconciliationPaperDTO();
        reconciliationInfo = dbService.getDao(ReconciliationPaperDAO.class).getReconciliationCertificateByTokenId(tokenId);
        if(reconciliationInfo.getWardSecretarySignature() != null){
            reconciliationInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getWardSecretarySignature()));
        }
        reconciliationInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getApplicationPhoto()));
        reconciliationInfo.setCitizenshipPhotoPerson1(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getCitizenshipPhotoPerson1()));
        reconciliationInfo.setCitizenshipPhotoPerson2(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getCitizenshipPhotoPerson2()));
        reconciliationInfo.setCitizenshipPhotoPerson3(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getCitizenshipPhotoPerson3()));
        reconciliationInfo.setCitizenshipPhotoPerson4(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getCitizenshipPhotoPerson4()));
        reconciliationInfo.setOtherDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getOtherDocument1()));
        reconciliationInfo.setOtherDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getOtherDocument2()));
        reconciliationInfo.setOtherDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, reconciliationInfo.getOtherDocument3()));

        return reconciliationInfo;
    }


    @Override
    public void updateReconciliationCertificateByTokenId(ReconciliationPaperDTO reconciliationRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(ReconciliationPaperDAO.class).updateReconciliationRegistrationByTokenId(reconciliationRegistrationInfo, tokenId);
            dbService.getDao(ReconciliationPaperDAO.class).approveReconciliationRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(ReconciliationPaperDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

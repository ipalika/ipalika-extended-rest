package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.BahalTaxDAO;
import com.ishanitech.ipalika.dto.BahalTaxDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.BahalTaxService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BahalTaxServiceImpl implements BahalTaxService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public BahalTaxServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addBahalTaxRegistration(BahalTaxDTO bahalRegistrationInfo) {
        try {
            dbService.getDao(BahalTaxDAO.class).addNewEntity(bahalRegistrationInfo);

            String message = "";

            message = "Your request for Bahal Tax Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + bahalRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, bahalRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<BahalTaxDTO> getBahalTaxRegistrations(HttpServletRequest request) {
        List<BahalTaxDTO> bahalReg = new ArrayList<>();
        try {
            bahalReg = dbService.getDao(BahalTaxDAO.class).getBahalTaxRegistrations();
            return bahalReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public BahalTaxDTO getBahalTaxCertificateByTokenId(String tokenId) {
        BahalTaxDTO bahalInfo = new BahalTaxDTO();
        bahalInfo = dbService.getDao(BahalTaxDAO.class).getBahalTaxCertificateByTokenId(tokenId);
        if(bahalInfo.getWardSecretarySignature() != null){
            bahalInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, bahalInfo.getWardSecretarySignature()));
        }
        bahalInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, bahalInfo.getApplicationPhoto()));
        bahalInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, bahalInfo.getCitizenshipPhoto()));
        bahalInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, bahalInfo.getBahalAgreementPhoto()));
        bahalInfo.setRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, bahalInfo.getRegistrationCertificate()));
        bahalInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, bahalInfo.getTaxInformationPhoto()));

        return bahalInfo;
    }


    @Override
    public void updateBahalTaxCertificateByTokenId(BahalTaxDTO bahalRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(BahalTaxDAO.class).updateBahalTaxRegistrationByTokenId(bahalRegistrationInfo, tokenId);
            dbService.getDao(BahalTaxDAO.class).approveBahalTaxRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(BahalTaxDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

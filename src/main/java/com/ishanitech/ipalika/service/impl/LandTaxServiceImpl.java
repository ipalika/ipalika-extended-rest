package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.LandTaxDAO;
import com.ishanitech.ipalika.dto.LandTaxDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.LandTaxService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class LandTaxServiceImpl implements LandTaxService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public LandTaxServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addLandRegistration(LandTaxDTO landRegistrationInfo) {
        try {
            dbService.getDao(LandTaxDAO.class).addNewEntity(landRegistrationInfo);

            String message = "";

            message = "Your request for Land Tax Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + landRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, landRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<LandTaxDTO> getLandRegistrations(HttpServletRequest request) {
        List<LandTaxDTO> landReg = new ArrayList<>();
        try {
            landReg = dbService.getDao(LandTaxDAO.class).getLandRegistrations();
            return landReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public LandTaxDTO getLandCertificateByTokenId(String tokenId) {
        LandTaxDTO landInfo = new LandTaxDTO();
        landInfo = dbService.getDao(LandTaxDAO.class).getLandCertificateByTokenId(tokenId);
        if(landInfo.getWardSecretarySignature() != null){
            landInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getWardSecretarySignature()));
        }
        landInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getApplicationPhoto()));
        landInfo.setLandOwnershipCertificateForFirstYear(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getLandOwnershipCertificateForFirstYear()));
        landInfo.setLandTaxReceiptForRenewal(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getLandTaxReceiptForRenewal()));
        landInfo.setLandRevenueRenewalBook(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getLandRevenueRenewalBook()));
        landInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getTaxInformationPhoto()));

        return landInfo;
    }


    @Override
    public void updateLandCertificateByTokenId(LandTaxDTO landRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(LandTaxDAO.class).updateLandRegistrationByTokenId(landRegistrationInfo, tokenId);
            dbService.getDao(LandTaxDAO.class).approveLandRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(LandTaxDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

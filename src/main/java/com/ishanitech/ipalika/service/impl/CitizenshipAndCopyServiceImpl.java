package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.CitizenshipAndCopyDAO;
import com.ishanitech.ipalika.dto.CitizenshipAndCopyDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.CitizenshipAndCopyService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class CitizenshipAndCopyServiceImpl implements CitizenshipAndCopyService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public CitizenshipAndCopyServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addCitizenshipRegistration(CitizenshipAndCopyDTO citizenshipRegistrationInfo) {
        try {
            dbService.getDao(CitizenshipAndCopyDAO.class).addNewEntity(citizenshipRegistrationInfo);

            String message = "";

            message = "Your request for Citizenship And Copy Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + citizenshipRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, citizenshipRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<CitizenshipAndCopyDTO> getCitizenshipRegistrations(HttpServletRequest request) {
        List<CitizenshipAndCopyDTO> citizenshipReg = new ArrayList<>();
        try {
            citizenshipReg = dbService.getDao(CitizenshipAndCopyDAO.class).getCitizenshipRegistrations();
            return citizenshipReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public CitizenshipAndCopyDTO getCitizenshipCertificateByTokenId(String tokenId) {
        CitizenshipAndCopyDTO citizenshipInfo = new CitizenshipAndCopyDTO();
        citizenshipInfo = dbService.getDao(CitizenshipAndCopyDAO.class).getCitizenshipCertificateByTokenId(tokenId);
        if(citizenshipInfo.getWardSecretarySignature() != null){
            citizenshipInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getWardSecretarySignature()));
        }
        citizenshipInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getApplicationPhoto()));
        citizenshipInfo.setFatherCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getFatherCitizenshipPhoto()));
        citizenshipInfo.setMotherCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getMotherCitizenshipPhoto()));
        citizenshipInfo.setBirthCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getBirthCertificate()));
        citizenshipInfo.setHusbandCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getHusbandCitizenshipPhoto()));
        citizenshipInfo.setStudentCharacterCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getStudentCharacterCertificate()));
        citizenshipInfo.setMarriageRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getMarriageRegistrationCertificate()));
        citizenshipInfo.setResettlementCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getResettlementCertificate()));
        citizenshipInfo.setPpPhotoCopy1(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getPpPhotoCopy1()));
        citizenshipInfo.setPpPhotoCopy2(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getPpPhotoCopy2()));
        citizenshipInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getTaxInformationPhoto()));
        citizenshipInfo.setConcernedOfficeLetter(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getConcernedOfficeLetter()));
        citizenshipInfo.setOldCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getOldCitizenshipPhoto()));

        return citizenshipInfo;
    }


    @Override
    public void updateCitizenshipCertificateByTokenId(CitizenshipAndCopyDTO citizenshipRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(CitizenshipAndCopyDAO.class).updateCitizenshipRegistrationByTokenId(citizenshipRegistrationInfo, tokenId);
            dbService.getDao(CitizenshipAndCopyDAO.class).approveCitizenshipRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(CitizenshipAndCopyDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

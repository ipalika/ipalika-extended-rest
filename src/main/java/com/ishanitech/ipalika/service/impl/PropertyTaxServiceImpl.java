package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.PropertyTaxDAO;
import com.ishanitech.ipalika.dto.PropertyTaxDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.PropertyTaxService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class PropertyTaxServiceImpl implements PropertyTaxService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public PropertyTaxServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addPropertyTaxRegistration(PropertyTaxDTO propertyRegistrationInfo) {
        try {
            dbService.getDao(PropertyTaxDAO.class).addNewEntity(propertyRegistrationInfo);

            String message = "";

            message = "Your request for Property Tax Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + propertyRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, propertyRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<PropertyTaxDTO> getPropertyTaxRegistrations(HttpServletRequest request) {
        List<PropertyTaxDTO> propertyReg = new ArrayList<>();
        try {
            propertyReg = dbService.getDao(PropertyTaxDAO.class).getPropertyTaxRegistrations();
            return propertyReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public PropertyTaxDTO getPropertyTaxCertificateByTokenId(String tokenId) {
        PropertyTaxDTO propertyInfo = new PropertyTaxDTO();
        propertyInfo = dbService.getDao(PropertyTaxDAO.class).getPropertyTaxCertificateByTokenId(tokenId);
        if(propertyInfo.getWardSecretarySignature() != null){
            propertyInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getWardSecretarySignature()));
        }
        propertyInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getApplicationPhoto()));
        propertyInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getLandOwnershipCertificate()));
        propertyInfo.setBuildingMapApprovalCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getBuildingMapApprovalCertificate()));
        propertyInfo.setCopyOfMap(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getCopyOfMap()));
        propertyInfo.setRegistrationPassCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getRegistrationPassCertificate()));
        propertyInfo.setLocalLevelSurveyMap(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getLocalLevelSurveyMap()));
        propertyInfo.setOnsiteTechnicalReport(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getOnsiteTechnicalReport()));
        propertyInfo.setTaxPaidToInlandRevenueOffice(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getTaxPaidToInlandRevenueOffice()));
        propertyInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, propertyInfo.getCitizenshipPhoto()));

        return propertyInfo;
    }


    @Override
    public void updatePropertyTaxCertificateByTokenId(PropertyTaxDTO propertyRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(PropertyTaxDAO.class).updatePropertyTaxRegistrationByTokenId(propertyRegistrationInfo, tokenId);
            dbService.getDao(PropertyTaxDAO.class).approvePropertyTaxRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(PropertyTaxDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

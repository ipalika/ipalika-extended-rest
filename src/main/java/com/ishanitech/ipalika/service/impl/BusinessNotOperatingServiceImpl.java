package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.BusinessNotOperatingDAO;
import com.ishanitech.ipalika.dto.BusinessNotOperatingDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.BusinessNotOperatingService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BusinessNotOperatingServiceImpl implements BusinessNotOperatingService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public BusinessNotOperatingServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addBusinessRegistration(BusinessNotOperatingDTO businessRegistrationInfo) {
        try {
            dbService.getDao(BusinessNotOperatingDAO.class).addNewEntity(businessRegistrationInfo);

            String message = "";

            message = "Your request for 'Business Not Operating Registration' Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + businessRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, businessRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<BusinessNotOperatingDTO> getBusinessRegistrations(HttpServletRequest request) {
        List<BusinessNotOperatingDTO> businessReg = new ArrayList<>();
        try {
            businessReg = dbService.getDao(BusinessNotOperatingDAO.class).getBusinessRegistrations();
            return businessReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public BusinessNotOperatingDTO getBusinessCertificateByTokenId(String tokenId) {
        BusinessNotOperatingDTO businessInfo = new BusinessNotOperatingDTO();
        businessInfo = dbService.getDao(BusinessNotOperatingDAO.class).getBusinessCertificateByTokenId(tokenId);
        if(businessInfo.getWardSecretarySignature() != null){
            businessInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getWardSecretarySignature()));
        }
        businessInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getApplicationPhoto()));
        businessInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getCitizenshipPhoto()));
        businessInfo.setBusinessRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getBusinessRegistrationCertificate()));
        businessInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getTaxInformationPhoto()));
        businessInfo.setOnsiteReport(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getOnsiteReport()));
        businessInfo.setGharBahalAgreement(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getGharBahalAgreement()));
        businessInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getSarjaminMuchulka()));

        return businessInfo;
    }


    @Override
    public void updateBusinessCertificateByTokenId(BusinessNotOperatingDTO businessRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(BusinessNotOperatingDAO.class).updateBusinessRegistrationByTokenId(businessRegistrationInfo, tokenId);
            dbService.getDao(BusinessNotOperatingDAO.class).approveBusinessRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(BusinessNotOperatingDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

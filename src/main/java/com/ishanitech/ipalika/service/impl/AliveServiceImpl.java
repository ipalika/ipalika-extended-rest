package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.AliveDAO;
import com.ishanitech.ipalika.dto.AliveCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.AliveService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class AliveServiceImpl implements AliveService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public AliveServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addAliveRegistration(AliveCertificateDTO aliveRegistrationInfo) {
        try {
            dbService.getDao(AliveDAO.class).addNewEntity(aliveRegistrationInfo);

            String message = "";

            message = "Your request for alive registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + aliveRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, aliveRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<AliveCertificateDTO> getAliveRegistrations(HttpServletRequest request) {
        List<AliveCertificateDTO> aliveReg = new ArrayList<>();
        try {
            aliveReg = dbService.getDao(AliveDAO.class).getAliveRegistrations();
            return aliveReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public AliveCertificateDTO getAliveCertificateByTokenId(String tokenId) {
        AliveCertificateDTO aliveInfo = new AliveCertificateDTO();
        aliveInfo = dbService.getDao(AliveDAO.class).getAliveRegistrationByTokenId(tokenId);
        if(aliveInfo.getWardSecretarySignature() != null){
            aliveInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, aliveInfo.getWardSecretarySignature()));

        }
        aliveInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, aliveInfo.getApplicationPhoto()));
        aliveInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, aliveInfo.getCitizenshipPhoto()));
        aliveInfo.setPpPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, aliveInfo.getPpPhoto()));
        aliveInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, aliveInfo.getTaxInformationPhoto()));

        return aliveInfo;
    }


    @Override
    public void updateAliveCertificateByTokenId(AliveCertificateDTO aliveRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(AliveDAO.class).updateAliveRegistrationByTokenId(aliveRegistrationInfo, tokenId);
            dbService.getDao(AliveDAO.class).approveAliveRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(AliveDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

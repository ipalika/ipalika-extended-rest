package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.InternalResidenceDAO;
import com.ishanitech.ipalika.dto.InternalResidenceDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.InternalResidenceService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class InternalResidenceServiceImpl implements InternalResidenceService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public InternalResidenceServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addResidenceRegistration(InternalResidenceDTO internalResidenceInfo) {
        try {
            dbService.getDao(InternalResidenceDAO.class).addNewEntity(internalResidenceInfo);

            String message = "";

            message = "Your request for Internal Residence registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + internalResidenceInfo.getTokenId() + ".";
            mailService.sendEmail(message, internalResidenceInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<InternalResidenceDTO> getResidenceRegistrations(HttpServletRequest request) {
        List<InternalResidenceDTO> residenceReg = new ArrayList<>();
        try {
            residenceReg = dbService.getDao(InternalResidenceDAO.class).getResidenceRegistrations();
            return residenceReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public InternalResidenceDTO getResidenceCertificateByTokenId(String tokenId) {
        InternalResidenceDTO residenceInfo = new InternalResidenceDTO();
        residenceInfo = dbService.getDao(InternalResidenceDAO.class).getResidenceCertificateByTokenId(tokenId);
        if(residenceInfo.getWardSecretarySignature() != null){
            residenceInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getWardSecretarySignature()));
        }
        residenceInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getApplicationPhoto()));
        residenceInfo.setRelocatingPersonCitizenshipPhoto1(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getRelocatingPersonCitizenshipPhoto1()));
        residenceInfo.setRelocatingPersonCitizenshipPhoto2(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getRelocatingPersonCitizenshipPhoto2()));
        residenceInfo.setRelocatingPersonCitizenshipPhoto3(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getRelocatingPersonCitizenshipPhoto3()));
        residenceInfo.setMarriageRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getMarriageRegistrationCertificate()));
        residenceInfo.setBirthRegistrationCertificate1(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getBirthRegistrationCertificate1()));
        residenceInfo.setBirthRegistrationCertificate2(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getBirthRegistrationCertificate2()));
        residenceInfo.setBirthRegistrationCertificate3(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getBirthRegistrationCertificate3()));
        residenceInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getLandOwnershipCertificate()));
        residenceInfo.setBusinessProofDocument(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getBusinessProofDocument()));
        residenceInfo.setResidenceProofDocument(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getResidenceProofDocument()));
        residenceInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getTaxInformationPhoto()));
        residenceInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getBahalAgreementPhoto()));

        return residenceInfo;
    }


    @Override
    public void updateResidenceCertificateByTokenId(InternalResidenceDTO internalResidenceInfo, String tokenId) {
        try {
            dbService.getDao(InternalResidenceDAO.class).updateResidenceCertificateByTokenId(internalResidenceInfo, tokenId);
            dbService.getDao(InternalResidenceDAO.class).approveResidenceCertificate(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(InternalResidenceDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

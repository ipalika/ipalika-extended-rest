package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.DeathDAO;
import com.ishanitech.ipalika.dto.DeathCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.DeathService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class DeathServiceImpl implements DeathService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public DeathServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addDeathRegistration(DeathCertificateDTO deathRegistrationInfo) {
        try {
            dbService.getDao(DeathDAO.class).addNewEntity(deathRegistrationInfo);

            String message = "";

            message = "Your request for death registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + deathRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, deathRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<DeathCertificateDTO> getDeathRegistrations(HttpServletRequest request) {
        List<DeathCertificateDTO> deathReg = new ArrayList<>();
        try {
            deathReg = dbService.getDao(DeathDAO.class).getDeathRegistrations();
            return deathReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public DeathCertificateDTO getDeathCertificateByTokenId(String tokenId) {
        DeathCertificateDTO deathInfo = new DeathCertificateDTO();
        deathInfo = dbService.getDao(DeathDAO.class).getDeathRegistrationByTokenId(tokenId);
        if(deathInfo.getWardSecretarySignature() != null){
            deathInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, deathInfo.getWardSecretarySignature()));
        }
        deathInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, deathInfo.getApplicationPhoto()));
        deathInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, deathInfo.getCitizenshipPhoto()));
        deathInfo.setInformantDocument(ImageUtilService.makeFullImageurl(restUrlProperty, deathInfo.getInformantDocument()));
        deathInfo.setRelationshipCertificatePhoto(ImageUtilService.makeFullImageurl(restUrlProperty, deathInfo.getRelationshipCertificatePhoto()));

        return deathInfo;
    }


    @Override
    public void updateDeathCertificateByTokenId(DeathCertificateDTO deathRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(DeathDAO.class).updateDeathRegistrationByTokenId(deathRegistrationInfo, tokenId);
            dbService.getDao(DeathDAO.class).approveDeathRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(DeathDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

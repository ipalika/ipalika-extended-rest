package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.AngikritCitizenshipDAO;
import com.ishanitech.ipalika.dto.AngikritCitizenshipDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.AngikritCitizenshipService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class AngikritCitizenshipServiceImpl implements AngikritCitizenshipService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public AngikritCitizenshipServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addCitizenshipRegistration(AngikritCitizenshipDTO citizenshipRegistrationInfo) {
        try {
            dbService.getDao(AngikritCitizenshipDAO.class).addNewEntity(citizenshipRegistrationInfo);

            String message = "";

            message = "Your request for Angikrit Citizenship Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + citizenshipRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, citizenshipRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<AngikritCitizenshipDTO> getCitizenshipRegistrations(HttpServletRequest request) {
        List<AngikritCitizenshipDTO> citizenshipReg = new ArrayList<>();
        try {
            citizenshipReg = dbService.getDao(AngikritCitizenshipDAO.class).getCitizenshipRegistrations();
            return citizenshipReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public AngikritCitizenshipDTO getCitizenshipCertificateByTokenId(String tokenId) {
        AngikritCitizenshipDTO citizenshipInfo = new AngikritCitizenshipDTO();
        citizenshipInfo = dbService.getDao(AngikritCitizenshipDAO.class).getCitizenshipCertificateByTokenId(tokenId);
        if(citizenshipInfo.getWardSecretarySignature() != null){
            citizenshipInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getWardSecretarySignature()));
        }
        citizenshipInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getApplicationPhoto()));
        citizenshipInfo.setDocRenouncingCitizenshipOfFormerCountry(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getDocRenouncingCitizenshipOfFormerCountry()));
        citizenshipInfo.setProofDocOfLivingInNepalFor15Years(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getProofDocOfLivingInNepalFor15Years()));
        citizenshipInfo.setMarriageRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getMarriageRegistrationCertificate()));
        citizenshipInfo.setOfficialCertificateOfConcernedCountry(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getOfficialCertificateOfConcernedCountry()));
        citizenshipInfo.setProofDocumentKnowingToReadWriteInNepali(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getProofDocumentKnowingToReadWriteInNepali()));
        citizenshipInfo.setPpPhoto1(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getPpPhoto1()));
        citizenshipInfo.setPpPhoto2(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getPpPhoto2()));
        citizenshipInfo.setPpPhoto3(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getPpPhoto3()));
        citizenshipInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getTaxInformationPhoto()));
        citizenshipInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, citizenshipInfo.getSarjaminMuchulka()));

        return citizenshipInfo;
    }


    @Override
    public void updateCitizenshipCertificateByTokenId(AngikritCitizenshipDTO citizenshipRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(AngikritCitizenshipDAO.class).updateCitizenshipRegistrationByTokenId(citizenshipRegistrationInfo, tokenId);
            dbService.getDao(AngikritCitizenshipDAO.class).approveCitizenshipRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(AngikritCitizenshipDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.StrongEconomyDAO;
import com.ishanitech.ipalika.dto.StrongEconomyDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.StrongEconomyService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class StrongEconomyServiceImpl implements StrongEconomyService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public StrongEconomyServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addStrongEconomyRegistration(StrongEconomyDTO economyRegistrationInfo) {
        try {
            dbService.getDao(StrongEconomyDAO.class).addNewEntity(economyRegistrationInfo);

            String message = "";

            message = "Your request for Strong Economic Condition Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + economyRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, economyRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<StrongEconomyDTO> getStrongEconomyRegistrations(HttpServletRequest request) {
        List<StrongEconomyDTO> economyReg = new ArrayList<>();
        try {
            economyReg = dbService.getDao(StrongEconomyDAO.class).getStrongEconomyRegistrations();
            return economyReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public StrongEconomyDTO getStrongEconomyCertificateByTokenId(String tokenId) {
        StrongEconomyDTO economyInfo = new StrongEconomyDTO();
        economyInfo = dbService.getDao(StrongEconomyDAO.class).getStrongEconomyCertificateByTokenId(tokenId);
        if(economyInfo.getWardSecretarySignature() != null){
            economyInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getWardSecretarySignature()));
        }
        economyInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getApplicationPhoto()));
        economyInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getCitizenshipPhoto()));
        economyInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getLandOwnershipCertificate()));
        economyInfo.setIncomeSourceDocument(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getIncomeSourceDocument()));
        economyInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getTaxInformationPhoto()));
        economyInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getSarjaminMuchulka()));
        economyInfo.setOtherDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getOtherDocument1()));
        economyInfo.setOtherDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getOtherDocument2()));
        economyInfo.setOtherDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, economyInfo.getOtherDocument3()));

        return economyInfo;
    }


    @Override
    public void updateStrongEconomyCertificateByTokenId(StrongEconomyDTO economyRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(StrongEconomyDAO.class).updateStrongEconomyRegistrationByTokenId(economyRegistrationInfo, tokenId);
            dbService.getDao(StrongEconomyDAO.class).approveStrongEconomyRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(StrongEconomyDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

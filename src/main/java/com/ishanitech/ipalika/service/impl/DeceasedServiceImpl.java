package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.DeceasedDAO;
import com.ishanitech.ipalika.dto.DeceasedCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.DeceasedService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class DeceasedServiceImpl implements DeceasedService {


    private final DbService dbService;
    private final FileUtilService fileUtilService;
    private final MailService mailService;


    @Autowired
    private RestBaseProperty restUrlProperty;

    public DeceasedServiceImpl(DbService dbService, FileUtilService fileUtilService, MailService mailService) {
        this.dbService = dbService;
        this.fileUtilService = fileUtilService;
        this.mailService = mailService;
    }



    @Override
    public void addRelationRegistration(DeceasedCertificateDTO relationRegistrationInfo) {
        try {
            dbService.getDao(DeceasedDAO.class).addNewEntity(relationRegistrationInfo);

            String message = "";

            message = "Your request for relationship registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + relationRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, relationRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void uploadRelationCertificate(MultipartFile image) {
        fileUtilService.storeFile(image);
    }


    @Override
    public List<DeceasedCertificateDTO> getRelationRegistrations(HttpServletRequest request) {
        List<DeceasedCertificateDTO> relationReg = new ArrayList<>();
        try {
            relationReg = dbService.getDao(DeceasedDAO.class).getRelationRegistrations();
            return relationReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public DeceasedCertificateDTO getRelationCertificateByTokenId(String tokenId) {
        DeceasedCertificateDTO relationInfo = new DeceasedCertificateDTO();
        relationInfo = dbService.getDao(DeceasedDAO.class).getRelationRegistrationByTokenId(tokenId);
        if(relationInfo.getWardSecretarySignature() != null){
            relationInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getWardSecretarySignature()));
        }
        relationInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getApplicationPhoto()));
        relationInfo.setCitizenshipPhotoApplicant(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getCitizenshipPhotoApplicant()));
        relationInfo.setPpPhotoApplicant(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getPpPhotoApplicant()));
        relationInfo.setCitizenshipPhotoDeceased(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getCitizenshipPhotoDeceased()));
        relationInfo.setPpPhotoDeceased(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getPpPhotoDeceased()));
        relationInfo.setRelationshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getRelationshipPhoto()));
        relationInfo.setDeathCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getDeathCertificate()));

        return relationInfo;
    }



    @Override
    public void updateRelationCertificateByTokenId(DeceasedCertificateDTO relationRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(DeceasedDAO.class).updateRelationRegistrationByTokenId(relationRegistrationInfo, tokenId);
            dbService.getDao(DeceasedDAO.class).approveRelationRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(DeceasedDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.MohiLagatDAO;
import com.ishanitech.ipalika.dto.MohiLagatDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.MohiLagatService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class MohiLagatServiceImpl implements MohiLagatService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public MohiLagatServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addMohiLagatRegistration(MohiLagatDTO mohiRegistrationInfo) {
        try {
            dbService.getDao(MohiLagatDAO.class).addNewEntity(mohiRegistrationInfo);

            String message = "";

            message = "Your request for Mohi Lagat Katta Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + mohiRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, mohiRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<MohiLagatDTO> getMohiLagatRegistrations(HttpServletRequest request) {
        List<MohiLagatDTO> mohiReg = new ArrayList<>();
        try {
            mohiReg = dbService.getDao(MohiLagatDAO.class).getMohiLagatRegistrations();
            return mohiReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public MohiLagatDTO getMohiLagatCertificateByTokenId(String tokenId) {
        MohiLagatDTO mohiInfo = new MohiLagatDTO();
        mohiInfo = dbService.getDao(MohiLagatDAO.class).getMohiLagatCertificateByTokenId(tokenId);
        if(mohiInfo.getWardSecretarySignature() != null){
            mohiInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getWardSecretarySignature()));
        }
        mohiInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getApplicationPhoto()));
        mohiInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getCitizenshipPhoto()));
        mohiInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getLandOwnershipCertificate()));
        mohiInfo.setLandSurveyMap(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getLandSurveyMap()));
        mohiInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getTaxInformationPhoto()));
        mohiInfo.setLandlordProofPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getLandlordProofPhoto()));
        mohiInfo.setFieldBook(ImageUtilService.makeFullImageurl(restUrlProperty, mohiInfo.getFieldBook()));

        return mohiInfo;
    }


    @Override
    public void updateMohiLagatCertificateByTokenId(MohiLagatDTO mohiRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(MohiLagatDAO.class).updateMohiLagatRegistrationByTokenId(mohiRegistrationInfo, tokenId);
            dbService.getDao(MohiLagatDAO.class).approveMohiLagatRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(MohiLagatDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

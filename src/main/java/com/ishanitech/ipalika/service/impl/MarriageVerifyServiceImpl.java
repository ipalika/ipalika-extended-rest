package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.MarriageVerifyDAO;
import com.ishanitech.ipalika.dto.MarriageVerifyDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.MarriageVerifyService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class MarriageVerifyServiceImpl implements MarriageVerifyService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public MarriageVerifyServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService) {
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public List<MarriageVerifyDTO> getMarriageVerifications(HttpServletRequest request) {
        List<MarriageVerifyDTO> marriageReg = new ArrayList<>();
        try {
            marriageReg = dbService.getDao(MarriageVerifyDAO.class).getMarriageVerifications();
            return marriageReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }

    }


    @Override
    public void addMarriageVerification(MarriageVerifyDTO marriageVerificationInfo) {
        try {
            dbService.getDao(MarriageVerifyDAO.class).addNewEntity(marriageVerificationInfo);

            String message = "";

            message = "Your request for marriage verification certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + marriageVerificationInfo.getTokenId() + ".";
            mailService.sendEmail(message, marriageVerificationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image) {
        fileUtilService.storeFile(image);
    }


    @Override
    public MarriageVerifyDTO getMarriageVerificationByTokenId(String tokenId){
        MarriageVerifyDTO marriageInfo = new MarriageVerifyDTO();
        marriageInfo = dbService.getDao(MarriageVerifyDAO.class).getMarriageVerificationByTokenId(tokenId);
        if(marriageInfo.getWardSecretarySignature() != null){
            marriageInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getWardSecretarySignature()));
        }
        marriageInfo.setHusbandCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getHusbandCitizenshipPhoto()));
        marriageInfo.setWifeCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getWifeCitizenshipPhoto()));
        marriageInfo.setMigrationCertificatePhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getMigrationCertificatePhoto()));
        marriageInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getTaxInformationPhoto()));
        marriageInfo.setMarriageCertificatePhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getMarriageCertificatePhoto()));

        return marriageInfo;
    }


    public void updateMarriageVerificationByTokenId(MarriageVerifyDTO marriageVerificationInfo,String tokenId){
        try{
            dbService.getDao(MarriageVerifyDAO.class).updateMarriageVerificationByTokenId(marriageVerificationInfo, tokenId);
            dbService.getDao(MarriageVerifyDAO.class).approveMarriageVerification(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(MarriageVerifyDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.LandMarkingDAO;
import com.ishanitech.ipalika.dto.LandMarkingDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.LandMarkingService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class LandMarkingServiceImpl implements LandMarkingService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public LandMarkingServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addLandMarkingRegistration(LandMarkingDTO landmarkRegistrationInfo) {
        try {
            dbService.getDao(LandMarkingDAO.class).addNewEntity(landmarkRegistrationInfo);

            String message = "";

            message = "Your request for Land Marking Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + landmarkRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, landmarkRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<LandMarkingDTO> getLandMarkingRegistrations(HttpServletRequest request) {
        List<LandMarkingDTO> landmarkReg = new ArrayList<>();
        try {
            landmarkReg = dbService.getDao(LandMarkingDAO.class).getLandMarkingRegistrations();
            return landmarkReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public LandMarkingDTO getLandMarkingCertificateByTokenId(String tokenId) {
        LandMarkingDTO landmarkInfo = new LandMarkingDTO();
        landmarkInfo = dbService.getDao(LandMarkingDAO.class).getLandMarkingCertificateByTokenId(tokenId);
        if(landmarkInfo.getWardSecretarySignature() != null){
            landmarkInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, landmarkInfo.getWardSecretarySignature()));
        }
        landmarkInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landmarkInfo.getApplicationPhoto()));
        landmarkInfo.setConcernedOfficeLetter(ImageUtilService.makeFullImageurl(restUrlProperty, landmarkInfo.getConcernedOfficeLetter()));
        landmarkInfo.setTechnicalReport(ImageUtilService.makeFullImageurl(restUrlProperty, landmarkInfo.getTechnicalReport()));
        landmarkInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landmarkInfo.getTaxInformationPhoto()));

        return landmarkInfo;
    }


    @Override
    public void updateLandMarkingCertificateByTokenId(LandMarkingDTO landmarkRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(LandMarkingDAO.class).updateLandMarkingRegistrationByTokenId(landmarkRegistrationInfo, tokenId);
            dbService.getDao(LandMarkingDAO.class).approveLandMarkingRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(LandMarkingDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

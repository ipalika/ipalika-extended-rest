package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.ResettlementRegisterDAO;
import com.ishanitech.ipalika.dto.ResettlementRegisterDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.ResettlementRegisterService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class ResettlementRegisterServiceImpl implements ResettlementRegisterService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public ResettlementRegisterServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addResettlementRegistration(ResettlementRegisterDTO resettlementRegistrationInfo) {
        try {
            dbService.getDao(ResettlementRegisterDAO.class).addNewEntity(resettlementRegistrationInfo);

            String message = "";

            message = "Your request for Resettlement Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + resettlementRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, resettlementRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<ResettlementRegisterDTO> getResettlementRegistrations(HttpServletRequest request) {
        List<ResettlementRegisterDTO> resettlementReg = new ArrayList<>();
        try {
            resettlementReg = dbService.getDao(ResettlementRegisterDAO.class).getResettlementRegistrations();
            return resettlementReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public ResettlementRegisterDTO getResettlementCertificateByTokenId(String tokenId) {
        ResettlementRegisterDTO resettlementInfo = new ResettlementRegisterDTO();
        resettlementInfo = dbService.getDao(ResettlementRegisterDAO.class).getResettlementCertificateByTokenId(tokenId);
        if(resettlementInfo.getWardSecretarySignature() != null){
            resettlementInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getWardSecretarySignature()));
        }
        resettlementInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getApplicationPhoto()));
        resettlementInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getCitizenshipPhoto()));
        resettlementInfo.setFamilyDetailsPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getFamilyDetailsPhoto()));
        resettlementInfo.setConcernedWardOfficeLetter(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getConcernedWardOfficeLetter()));
        resettlementInfo.setLalpurjaOfGoingPlace(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getLalpurjaOfGoingPlace()));
        resettlementInfo.setLalpurjaOfComingPlace(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getLalpurjaOfComingPlace()));
        resettlementInfo.setCertificateOfRelocation(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getCertificateOfRelocation()));
        resettlementInfo.setRelocatingPersonCitizenshipPhoto1(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getRelocatingPersonCitizenshipPhoto1()));
        resettlementInfo.setRelocatingPersonCitizenshipPhoto2(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getRelocatingPersonCitizenshipPhoto2()));
        resettlementInfo.setRelocatingPersonCitizenshipPhoto3(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getRelocatingPersonCitizenshipPhoto3()));
        resettlementInfo.setRelocatingPersonCitizenshipPhoto4(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getRelocatingPersonCitizenshipPhoto4()));
        resettlementInfo.setBirthRegistrationCertificate1(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getBirthRegistrationCertificate1()));
        resettlementInfo.setBirthRegistrationCertificate2(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getBirthRegistrationCertificate2()));
        resettlementInfo.setBirthRegistrationCertificate3(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getBirthRegistrationCertificate3()));
        resettlementInfo.setBirthRegistrationCertificate4(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getBirthRegistrationCertificate4()));
        resettlementInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, resettlementInfo.getTaxInformationPhoto()));

        return resettlementInfo;
    }


    @Override
    public void updateResettlementCertificateByTokenId(ResettlementRegisterDTO resettlementRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(ResettlementRegisterDAO.class).updateResettlementCertificateByTokenId(resettlementRegistrationInfo, tokenId);
            dbService.getDao(ResettlementRegisterDAO.class).approveResettlementCertificate(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(ResettlementRegisterDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.dao.RegistrationReportDAO;
import com.ishanitech.ipalika.dto.RegistrationReport;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.RegistrationReportService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RegistrationReportServiceImpl implements RegistrationReportService {

	private final RegistrationReportDAO registrationReportDAO;

	private HashMap<String, String> tableList = new HashMap<>();

	public RegistrationReportServiceImpl(DbService dbService) {

		this.registrationReportDAO = dbService.getDao(RegistrationReportDAO.class);
		tableList.put("egovernance_birth_certificate", "जन्म दर्ता");
		tableList.put("egovernance_relationship_living", "जिवित सँगको नाता प्रमाणित ");
		tableList.put("egovernance_relationship_deceased", "मृतक सँगको नाता प्रमाणित ");
		tableList.put("egovernance_marriage_certificate", "विवाह दर्ता");
		tableList.put("egovernance_death_certificate", "मृत्यु दर्ता");
		tableList.put("egovernance_divorce_certificate", "सम्बन्ध विच्छेद दर्ता");
		tableList.put("egovernance_disabled_certificate", "अपाङ्ग सिफारिस");
		tableList.put("egovernance_marriage_verification", "विवाह प्रमाणित");
		tableList.put("egovernance_alive_certificate", "विपन्न विद्यार्थी छात्रबृत्ति सिफारिस");
		tableList.put("egovernance_minor_certificate", "विपन्न विद्यार्थी छात्रबृत्ति सिफारिस");
		tableList.put("egovernance_scholarship_certificate", "छात्रबृत्ति सिफारिस ");
		tableList.put("egovernance_birth_verification", "जन्म मिति प्रमाणित");
		tableList.put("egovernance_permanent_residence", "स्थायी बसोबास सिफारिस");
		tableList.put("egovernance_pipeline_connection", "धारा जडान सिफारिस");
		tableList.put("egovernance_land_marking", "जग्गा रेखांकनको कार्य/सो कार्यमा रोहबर ");
//		tableList.put("egovernance_bahal_tax", "जन्म दर्ता");
//		tableList.put("egovernance_house_building", "जन्म दर्ता");
		tableList.put("egovernance_ghar_kayam", "घर कायम सिफारिस");
		tableList.put("egovernance_poor_economy", "आर्थिक अवस्था कमजोर वा विपन्नता प्रमाणित");
		tableList.put("egovernance_english_verification", "अंग्रेजी सिफारिस तथा प्रमाणित");
		tableList.put("egovernance_manjurinama_verification", "कागज/मञ्जुरीनामा प्रमाणित");
		tableList.put("egovernance_ghar_patal", "घर पाताल प्रमाणित");
//		tableList.put("egovernance_strong_economy", "जन्म दर्ता");
		tableList.put("egovernance_electricity_connection", "विद्युत जडान सिफारिस");
		tableList.put("egovernance_two_names", "दुवै नाम गरेको ब्यक्ति एकै हो भन्ने सिफारिस");
//		tableList.put("egovernance_land_valuation", "जन्म दर्ता");
		tableList.put("egovernance_business_not_operating", "ब्यवसाय सञ्चालन नभएको सिफारिस");
		tableList.put("egovernance_land_dhanipurja", "जग्गा धनीपूर्जा हराएको सिफारिस");
//		tableList.put("egovernance_rightful_person", "जन्म दर्ता");
		tableList.put("egovernance_कोठा_खोल्न_कार्य", "कोठा खोल्न कार्य/रोहबरमा बस्ने कार्य");
		tableList.put("egovernance_बाटो_कायम", "नेपाल सरकारको नाममा बाटो कायम सिफारिस");
		tableList.put("egovernance_संरक्षक_व्यक्तिगत", "संरक्षक सिफारिस (ब्यक्तिगत)");
		tableList.put("egovernance_संरक्षक_संस्थागत", "संरक्षक सिफारिस (संस्थागत)");
		tableList.put("egovernance_व्यक्तिगत_विवरण", "ब्यक्तिगत विवरण सिफारिस");
		tableList.put("egovernance_विद्यालय_ठाउँसारी", "विद्यालय ठाउंसारी सिफारिस");
		tableList.put("egovernance_उद्योग_ठाउँसारी", "उद्योग ठाउंसारी सिफारिस");
		tableList.put("egovernance_mohi_lagat", "मोही लगत कट्टा सिफारिस");
		tableList.put("egovernance_house_land_transfer", "घर जग्गा नामसारी सिफारिस");
		tableList.put("egovernance_home_road", "घर बाटो प्रमाणित");
		tableList.put("egovernance_मालपोत_भूमिकर", "मालपोत वा भूमीकर");
		tableList.put("egovernance_advertisement_tax", "विज्ञापन कर");
//		tableList.put("egovernance_property_tax", "जन्म दर्ता");
		tableList.put("egovernance_reconciliation_paper", "मिलापत्र कागज / उजुरी दर्ता ");
		tableList.put("egovernance_चार_कल्ला", "चार किल्ला प्रमाणित");
		tableList.put("egovernance_institution_register", "संस्था दर्ता सिफारिस");
		tableList.put("egovernance_free_healthcare", "निःशुल्क वा सःशुल्क स्वास्थ्य उपचार सिफारिस");
//		tableList.put("egovernance_land_register", "जन्म दर्ता");
		tableList.put("egovernance_school_operation", "विद्यालय सञ्चालन स्वीकृत/कक्षा बृद्धि सिफारिस ");
		tableList.put("egovernance_internal_residence", "अस्थाई बसोबास सिफारिस");
		tableList.put("egovernance_court_fee", "कोर्ट फि मिनाहा सिफारिस");
		tableList.put("egovernance_business_closure", "ब्यवसाय बन्द सिफारिस");
		tableList.put("egovernance_angikrit_citizenship", "अंगिकृत नागरिकता");
		tableList.put("egovernance_citizenship_and_copy", "नागरिकता र प्रतिलिपि सिफारिस");
		tableList.put("egovernance_resettlement_register", "आन्तरिक बसाई सराई सिफारिस");
		tableList.put("egovernance_business_register", "ब्यवसाय दर्ता सिफारिस");
		tableList.put("egovernance_quadruped", "चौपाय सम्वन्धी सिफारिस");
		tableList.put("egovernance_details_disclosure", "अन्य कार्यालयको माग अनुसार विवरण खुलाई पठाउने कार्य");



	}

	@Override
	public void populateRegistrationCount() {
		List<RegistrationReport> registrationReports = new ArrayList<RegistrationReport>();


	}

	public void saveRegistrationReports(List<RegistrationReport> registrationReports) {

		registrationReportDAO.saveRegistrationReport(registrationReports);
	}

	// just for testing
	@Override
	public void getCounts() {
		int count = registrationReportDAO.getTotalRegistrationCount(tableList.get(0));
		System.out.println("........................................");
		System.out.println(count);
	}

	@Override
	public List<RegistrationReport> getRegistrationReports() {
		return registrationReportDAO.getAllRegistrationReport();
	}
}

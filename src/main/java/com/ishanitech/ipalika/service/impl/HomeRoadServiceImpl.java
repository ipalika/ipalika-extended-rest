package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.HomeRoadDAO;
import com.ishanitech.ipalika.dto.HomeRoadDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.HomeRoadService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class HomeRoadServiceImpl implements HomeRoadService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public HomeRoadServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addHomeRoadRegistration(HomeRoadDTO homeRegistrationInfo) {
        try {
            dbService.getDao(HomeRoadDAO.class).addNewEntity(homeRegistrationInfo);

            String message = "";

            message = "Your request for Home Road Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + homeRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, homeRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<HomeRoadDTO> getHomeRoadRegistrations(HttpServletRequest request) {
        List<HomeRoadDTO> homeReg = new ArrayList<>();
        try {
            homeReg = dbService.getDao(HomeRoadDAO.class).getHomeRoadRegistrations();
            return homeReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public HomeRoadDTO getHomeRoadCertificateByTokenId(String tokenId) {
        HomeRoadDTO homeInfo = new HomeRoadDTO();
        homeInfo = dbService.getDao(HomeRoadDAO.class).getHomeRoadCertificateByTokenId(tokenId);
        if(homeInfo.getWardSecretarySignature() != null){
            homeInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getWardSecretarySignature()));
        }
        homeInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getApplicationPhoto()));
        homeInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getLandOwnershipCertificate()));
        homeInfo.setGrossSurveyMapOfLand(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getGrossSurveyMapOfLand()));
        homeInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getTaxInformationPhoto()));
        homeInfo.setGiverCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getGiverCitizenshipPhoto()));
        homeInfo.setReceiverCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getReceiverCitizenshipPhoto()));
        homeInfo.setOnsiteInspectionReport(ImageUtilService.makeFullImageurl(restUrlProperty, homeInfo.getOnsiteInspectionReport()));

        return homeInfo;
    }


    @Override
    public void updateHomeRoadCertificateByTokenId(HomeRoadDTO homeRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(HomeRoadDAO.class).updateHomeRoadRegistrationByTokenId(homeRegistrationInfo, tokenId);
            dbService.getDao(HomeRoadDAO.class).approveHomeRoadRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(HomeRoadDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

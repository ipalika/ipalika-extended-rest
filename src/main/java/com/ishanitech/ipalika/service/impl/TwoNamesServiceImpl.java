package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.TwoNamesDAO;
import com.ishanitech.ipalika.dto.TwoNamesDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.TwoNamesService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class TwoNamesServiceImpl implements TwoNamesService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public TwoNamesServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addTwoNamesRegistration(TwoNamesDTO namesRegistrationInfo) {
        try {
            dbService.getDao(TwoNamesDAO.class).addNewEntity(namesRegistrationInfo);

            String message = "";

            message = "Your request for two-named same person Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + namesRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, namesRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<TwoNamesDTO> getTwoNamesRegistrations(HttpServletRequest request) {
        List<TwoNamesDTO> namesReg = new ArrayList<>();
        try {
            namesReg = dbService.getDao(TwoNamesDAO.class).getTwoNamesRegistrations();
            return namesReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public TwoNamesDTO getTwoNamesCertificateByTokenId(String tokenId) {
        TwoNamesDTO namesInfo = new TwoNamesDTO();
        namesInfo = dbService.getDao(TwoNamesDAO.class).getTwoNamesCertificateByTokenId(tokenId);
        if(namesInfo.getWardSecretarySignature() != null){
            namesInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getWardSecretarySignature()));
        }
        namesInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getApplicationPhoto()));
        namesInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getCitizenshipPhoto()));
        namesInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getTaxInformationPhoto()));
        namesInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getSarjaminMuchulka()));
        namesInfo.setDocConfirmingNameChange1(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getDocConfirmingNameChange1()));
        namesInfo.setDocConfirmingNameChange2(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getDocConfirmingNameChange2()));
        namesInfo.setDocConfirmingNameChange3(ImageUtilService.makeFullImageurl(restUrlProperty, namesInfo.getDocConfirmingNameChange3()));

        return namesInfo;
    }


    @Override
    public void updateTwoNamesCertificateByTokenId(TwoNamesDTO namesRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(TwoNamesDAO.class).updateTwoNamesRegistrationByTokenId(namesRegistrationInfo, tokenId);
            dbService.getDao(TwoNamesDAO.class).approveTwoNamesRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(TwoNamesDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.SchoolOperationDAO;
import com.ishanitech.ipalika.dto.SchoolOperationDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.SchoolOperationService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class SchoolOperationServiceImpl implements SchoolOperationService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public SchoolOperationServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addSchoolRegistration(SchoolOperationDTO schoolRegistrationInfo) {
        try {
            dbService.getDao(SchoolOperationDAO.class).addNewEntity(schoolRegistrationInfo);

            String message = "";

            message = "Your request for School Operation Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + schoolRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, schoolRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<SchoolOperationDTO> getSchoolRegistrations(HttpServletRequest request) {
        List<SchoolOperationDTO> schoolReg = new ArrayList<>();
        try {
            schoolReg = dbService.getDao(SchoolOperationDAO.class).getSchoolRegistrations();
            return schoolReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public SchoolOperationDTO getSchoolCertificateByTokenId(String tokenId) {
        SchoolOperationDTO schoolInfo = new SchoolOperationDTO();
        schoolInfo = dbService.getDao(SchoolOperationDAO.class).getSchoolCertificateByTokenId(tokenId);
        if(schoolInfo.getWardSecretarySignature() != null){
            schoolInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getWardSecretarySignature()));
        }
        schoolInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getApplicationPhoto()));
        schoolInfo.setSchoolRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getSchoolRegistrationCertificate()));
        schoolInfo.setLocalLevelRenewalCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getLocalLevelRenewalCertificate()));
        schoolInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getTaxInformationPhoto()));
        schoolInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getBahalAgreementPhoto()));
        schoolInfo.setBahalTaxInformation(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getBahalTaxInformation()));
        schoolInfo.setInspectionReport(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getInspectionReport()));

        return schoolInfo;
    }


    @Override
    public void updateSchoolCertificateByTokenId(SchoolOperationDTO schoolRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(SchoolOperationDAO.class).updateSchoolRegistrationByTokenId(schoolRegistrationInfo, tokenId);
            dbService.getDao(SchoolOperationDAO.class).approveSchoolRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(SchoolOperationDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

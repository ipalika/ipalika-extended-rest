package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.SchoolShiftingDAO;
import com.ishanitech.ipalika.dto.SchoolShiftingDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.SchoolShiftingService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class SchoolShiftingServiceImpl implements SchoolShiftingService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public SchoolShiftingServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addSchoolRegistration(SchoolShiftingDTO schoolRegistrationInfo) {
        try {
            dbService.getDao(SchoolShiftingDAO.class).addNewEntity(schoolRegistrationInfo);

            String message = "";

            message = "Your request for School Shifting Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + schoolRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, schoolRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<SchoolShiftingDTO> getSchoolRegistrations(HttpServletRequest request) {
        List<SchoolShiftingDTO> schoolReg = new ArrayList<>();
        try {
            schoolReg = dbService.getDao(SchoolShiftingDAO.class).getSchoolRegistrations();
            return schoolReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public SchoolShiftingDTO getSchoolCertificateByTokenId(String tokenId) {
        SchoolShiftingDTO schoolInfo = new SchoolShiftingDTO();
        schoolInfo = dbService.getDao(SchoolShiftingDAO.class).getSchoolCertificateByTokenId(tokenId);
        if(schoolInfo.getWardSecretarySignature() != null){
            schoolInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getWardSecretarySignature()));
        }
        schoolInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getApplicationPhoto()));
        schoolInfo.setSchoolRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getSchoolRegistrationCertificate()));
        schoolInfo.setLocalLevelRenewalCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getLocalLevelRenewalCertificate()));
        schoolInfo.setTaxInformationCurrentPlace(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getTaxInformationCurrentPlace()));
        schoolInfo.setTaxInformationShiftingPlace(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getTaxInformationShiftingPlace()));
        schoolInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getBahalAgreementPhoto()));
        schoolInfo.setBahalTaxInformation(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getBahalTaxInformation()));
        schoolInfo.setPermanentAccountNoCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getPermanentAccountNoCertificate()));
        schoolInfo.setInspectionReport(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getInspectionReport()));
        schoolInfo.setWardOfficePermitLetter(ImageUtilService.makeFullImageurl(restUrlProperty, schoolInfo.getWardOfficePermitLetter()));

        return schoolInfo;
    }


    @Override
    public void updateSchoolCertificateByTokenId(SchoolShiftingDTO schoolRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(SchoolShiftingDAO.class).updateSchoolRegistrationByTokenId(schoolRegistrationInfo, tokenId);
            dbService.getDao(SchoolShiftingDAO.class).approveSchoolRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(SchoolShiftingDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

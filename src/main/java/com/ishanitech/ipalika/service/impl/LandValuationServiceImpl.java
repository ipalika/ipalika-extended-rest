package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.LandValuationDAO;
import com.ishanitech.ipalika.dto.LandValuationDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.LandValuationService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class LandValuationServiceImpl implements LandValuationService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public LandValuationServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addLandRegistration(LandValuationDTO landRegistrationInfo) {
        try {
            dbService.getDao(LandValuationDAO.class).addNewEntity(landRegistrationInfo);

            String message = "";

            message = "Your request for Land Valuation Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + landRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, landRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<LandValuationDTO> getLandRegistrations(HttpServletRequest request) {
        List<LandValuationDTO> landReg = new ArrayList<>();
        try {
            landReg = dbService.getDao(LandValuationDAO.class).getLandRegistrations();
            return landReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public LandValuationDTO getLandCertificateByTokenId(String tokenId) {
        LandValuationDTO landInfo = new LandValuationDTO();
        landInfo = dbService.getDao(LandValuationDAO.class).getLandCertificateByTokenId(tokenId);
        if(landInfo.getWardSecretarySignature() != null){
            landInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getWardSecretarySignature()));
        }
        landInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getApplicationPhoto()));
        landInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getCitizenshipPhoto()));
        landInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getLandOwnershipCertificate()));
        landInfo.setSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getSarjaminMuchulka()));
        landInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, landInfo.getTaxInformationPhoto()));

        return landInfo;
    }


    @Override
    public void updateLandCertificateByTokenId(LandValuationDTO landRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(LandValuationDAO.class).updateLandRegistrationByTokenId(landRegistrationInfo, tokenId);
            dbService.getDao(LandValuationDAO.class).approveLandRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(LandValuationDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

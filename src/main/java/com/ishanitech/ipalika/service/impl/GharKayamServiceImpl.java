package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.GharKayamDAO;
import com.ishanitech.ipalika.dto.GharKayamDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.GharKayamService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class GharKayamServiceImpl implements GharKayamService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public GharKayamServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addGharKayamRegistration(GharKayamDTO gharRegistrationInfo) {
        try {
            dbService.getDao(GharKayamDAO.class).addNewEntity(gharRegistrationInfo);

            String message = "";

            message = "Your request for Ghar Kayam Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + gharRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, gharRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<GharKayamDTO> getGharKayamRegistrations(HttpServletRequest request) {
        List<GharKayamDTO> gharReg = new ArrayList<>();
        try {
            gharReg = dbService.getDao(GharKayamDAO.class).getGharKayamRegistrations();
            return gharReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public GharKayamDTO getGharKayamCertificateByTokenId(String tokenId) {
        GharKayamDTO gharInfo = new GharKayamDTO();
        gharInfo = dbService.getDao(GharKayamDAO.class).getGharKayamCertificateByTokenId(tokenId);
        if(gharInfo.getWardSecretarySignature() != null){
            gharInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getWardSecretarySignature()));
        }
        gharInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getApplicationPhoto()));
        gharInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getCitizenshipPhoto()));
        gharInfo.setLandLalpurja(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getLandLalpurja()));
        gharInfo.setOnsiteReport(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getOnsiteReport()));
        gharInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, gharInfo.getTaxInformationPhoto()));

        return gharInfo;
    }


    @Override
    public void updateGharKayamCertificateByTokenId(GharKayamDTO gharRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(GharKayamDAO.class).updateGharKayamRegistrationByTokenId(gharRegistrationInfo, tokenId);
            dbService.getDao(GharKayamDAO.class).approveGharKayamRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(GharKayamDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.InstitutionRegisterDAO;
import com.ishanitech.ipalika.dto.InstitutionRegisterDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.InstitutionRegisterService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class InstitutionRegisterServiceImpl implements InstitutionRegisterService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public InstitutionRegisterServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addInstitutionRegistration(InstitutionRegisterDTO institutionRegistrationInfo) {
        try {
            dbService.getDao(InstitutionRegisterDAO.class).addNewEntity(institutionRegistrationInfo);

            String message = "";

            message = "Your request for Institution Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + institutionRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, institutionRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<InstitutionRegisterDTO> getInstitutionRegistrations(HttpServletRequest request) {
        List<InstitutionRegisterDTO> institutionReg = new ArrayList<>();
        try {
            institutionReg = dbService.getDao(InstitutionRegisterDAO.class).getInstitutionRegistrations();
            return institutionReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public InstitutionRegisterDTO getInstitutionCertificateByTokenId(String tokenId) {
        InstitutionRegisterDTO institutionInfo = new InstitutionRegisterDTO();
        institutionInfo = dbService.getDao(InstitutionRegisterDAO.class).getInstitutionCertificateByTokenId(tokenId);
        if(institutionInfo.getWardSecretarySignature() != null){
            institutionInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getWardSecretarySignature()));
        }
        institutionInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getApplicationPhoto()));
        institutionInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getCitizenshipPhoto()));
        institutionInfo.setCopyOfLegislationOrRules(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getCopyOfLegislationOrRules()));
        institutionInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getBahalAgreementPhoto()));
        institutionInfo.setBahalTaxInformation(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getBahalTaxInformation()));
        institutionInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getLandOwnershipCertificate()));
        institutionInfo.setMapPassCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getMapPassCertificate()));
        institutionInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, institutionInfo.getTaxInformationPhoto()));

        return institutionInfo;
    }


    @Override
    public void updateInstitutionCertificateByTokenId(InstitutionRegisterDTO institutionRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(InstitutionRegisterDAO.class).updateInstitutionRegistrationByTokenId(institutionRegistrationInfo, tokenId);
            dbService.getDao(InstitutionRegisterDAO.class).approveInstitutionRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(InstitutionRegisterDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

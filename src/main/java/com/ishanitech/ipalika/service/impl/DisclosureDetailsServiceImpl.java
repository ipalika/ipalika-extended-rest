package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.DisclosureDetailsDAO;
import com.ishanitech.ipalika.dto.DisclosureDetailsDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.DisclosureDetailsService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class DisclosureDetailsServiceImpl implements DisclosureDetailsService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public DisclosureDetailsServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addDisclosureRegistration(DisclosureDetailsDTO disclosureRegistrationInfo) {
        try {
            dbService.getDao(DisclosureDetailsDAO.class).addNewEntity(disclosureRegistrationInfo);

            String message = "";

            message = "Your request for Disclosure Details Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + disclosureRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, disclosureRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<DisclosureDetailsDTO> getDisclosureRegistrations(HttpServletRequest request) {
        List<DisclosureDetailsDTO> disclosureReg = new ArrayList<>();
        try {
            disclosureReg = dbService.getDao(DisclosureDetailsDAO.class).getDisclosureRegistrations();
            return disclosureReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public DisclosureDetailsDTO getDisclosureCertificateByTokenId(String tokenId) {
        DisclosureDetailsDTO disclosureInfo = new DisclosureDetailsDTO();
        disclosureInfo = dbService.getDao(DisclosureDetailsDAO.class).getDisclosureCertificateByTokenId(tokenId);
        if(disclosureInfo.getWardSecretarySignature() != null){
            disclosureInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, disclosureInfo.getWardSecretarySignature()));
        }
        disclosureInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, disclosureInfo.getApplicationPhoto()));
        disclosureInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, disclosureInfo.getCitizenshipPhoto()));
        disclosureInfo.setOfficeLetter(ImageUtilService.makeFullImageurl(restUrlProperty, disclosureInfo.getOfficeLetter()));
        disclosureInfo.setOtherDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, disclosureInfo.getOtherDocument1()));
        disclosureInfo.setOtherDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, disclosureInfo.getOtherDocument2()));
        disclosureInfo.setOtherDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, disclosureInfo.getOtherDocument3()));

        return disclosureInfo;
    }


    @Override
    public void updateDisclosureCertificateByTokenId(DisclosureDetailsDTO disclosureRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(DisclosureDetailsDAO.class).updateDisclosureRegistrationByTokenId(disclosureRegistrationInfo, tokenId);
            dbService.getDao(DisclosureDetailsDAO.class).approveDisclosureRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(DisclosureDetailsDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.BusinessRegisterDAO;
import com.ishanitech.ipalika.dto.BusinessRegisterDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.BusinessRegisterService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BusinessRegisterServiceImpl implements BusinessRegisterService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public BusinessRegisterServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addBusinessRegistration(BusinessRegisterDTO businessRegistrationInfo) {
        try {
            dbService.getDao(BusinessRegisterDAO.class).addNewEntity(businessRegistrationInfo);

            String message = "";

            message = "Your request for 'Business Registration' Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + businessRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, businessRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<BusinessRegisterDTO> getBusinessRegistrations(HttpServletRequest request) {
        List<BusinessRegisterDTO> businessReg = new ArrayList<>();
        try {
            businessReg = dbService.getDao(BusinessRegisterDAO.class).getBusinessRegistrations();
            return businessReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public BusinessRegisterDTO getBusinessCertificateByTokenId(String tokenId) {
        BusinessRegisterDTO businessInfo = new BusinessRegisterDTO();
        businessInfo = dbService.getDao(BusinessRegisterDAO.class).getBusinessCertificateByTokenId(tokenId);
        if(businessInfo.getWardSecretarySignature() != null){
            businessInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getWardSecretarySignature()));
        }
        businessInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getApplicationPhoto()));
        businessInfo.setBusinessRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getBusinessRegistrationCertificate()));
        businessInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getTaxInformationPhoto()));
        businessInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getBahalAgreementPhoto()));
        businessInfo.setPpPhotoCopy1(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getPpPhotoCopy1()));
        businessInfo.setPpPhotoCopy2(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getPpPhotoCopy2()));

        return businessInfo;
    }


    @Override
    public void updateBusinessCertificateByTokenId(BusinessRegisterDTO businessRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(BusinessRegisterDAO.class).updateBusinessRegistrationByTokenId(businessRegistrationInfo, tokenId);
            dbService.getDao(BusinessRegisterDAO.class).approveBusinessRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(BusinessRegisterDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

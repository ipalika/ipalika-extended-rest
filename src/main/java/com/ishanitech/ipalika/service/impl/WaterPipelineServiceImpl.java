package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.WaterPipelineDAO;
import com.ishanitech.ipalika.dto.WaterPipelineDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.WaterPipelineService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class WaterPipelineServiceImpl implements WaterPipelineService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public WaterPipelineServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addPipelineRegistration(WaterPipelineDTO pipelineRegistrationInfo) {
        try {
            dbService.getDao(WaterPipelineDAO.class).addNewEntity(pipelineRegistrationInfo);

            String message = "";

            message = "Your request for Pipeline Connection Recommendation Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + pipelineRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, pipelineRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<WaterPipelineDTO> getPipelineRegistrations(HttpServletRequest request) {
        List<WaterPipelineDTO> pipelineReg = new ArrayList<>();
        try {
            pipelineReg = dbService.getDao(WaterPipelineDAO.class).getPipelineRegistrations();
            return pipelineReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public WaterPipelineDTO getPipelineCertificateByTokenId(String tokenId) {
        WaterPipelineDTO pipelineInfo = new WaterPipelineDTO();
        pipelineInfo = dbService.getDao(WaterPipelineDAO.class).getPipelineRegistrationByTokenId(tokenId);
        if(pipelineInfo.getWardSecretarySignature() != null){
            pipelineInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, pipelineInfo.getWardSecretarySignature()));
        }
        pipelineInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, pipelineInfo.getApplicationPhoto()));
        pipelineInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, pipelineInfo.getCitizenshipPhoto()));
        pipelineInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, pipelineInfo.getLandOwnershipCertificate()));
        pipelineInfo.setProofOfMapPass(ImageUtilService.makeFullImageurl(restUrlProperty, pipelineInfo.getProofOfMapPass()));
        pipelineInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, pipelineInfo.getTaxInformationPhoto()));

        return pipelineInfo;
    }


    @Override
    public void updatePipelineCertificateByTokenId(WaterPipelineDTO pipelineRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(WaterPipelineDAO.class).updatePipelineRegistrationByTokenId(pipelineRegistrationInfo, tokenId);
            dbService.getDao(WaterPipelineDAO.class).approvePipelineRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(WaterPipelineDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.HouseBuildingDAO;
import com.ishanitech.ipalika.dto.HouseBuildingDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.HouseBuildingService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class HouseBuildingServiceImpl implements HouseBuildingService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public HouseBuildingServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addHouseBuildingRegistration(HouseBuildingDTO houseRegistrationInfo) {
        try {
            dbService.getDao(HouseBuildingDAO.class).addNewEntity(houseRegistrationInfo);

            String message = "";

            message = "Your request for House Building Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + houseRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, houseRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<HouseBuildingDTO> getHouseBuildingRegistrations(HttpServletRequest request) {
        List<HouseBuildingDTO> houseReg = new ArrayList<>();
        try {
            houseReg = dbService.getDao(HouseBuildingDAO.class).getHouseBuildingRegistrations();
            return houseReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public HouseBuildingDTO getHouseBuildingCertificateByTokenId(String tokenId) {
        HouseBuildingDTO houseInfo = new HouseBuildingDTO();
        houseInfo = dbService.getDao(HouseBuildingDAO.class).getHouseBuildingCertificateByTokenId(tokenId);
        if(houseInfo.getWardSecretarySignature() != null){
            houseInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getWardSecretarySignature()));
        }
        houseInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getApplicationPhoto()));
        houseInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getCitizenshipPhoto()));
        houseInfo.setBuildingMapPass(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getBuildingMapPass()));
        houseInfo.setConstructionCompletionCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getConstructionCompletionCertificate()));
        houseInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getLandOwnershipCertificate()));
        houseInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, houseInfo.getTaxInformationPhoto()));

        return houseInfo;
    }


    @Override
    public void updateHouseBuildingCertificateByTokenId(HouseBuildingDTO houseRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(HouseBuildingDAO.class).updateHouseBuildingRegistrationByTokenId(houseRegistrationInfo, tokenId);
            dbService.getDao(HouseBuildingDAO.class).approveHouseBuildingRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(HouseBuildingDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.PermanentResidenceDAO;
import com.ishanitech.ipalika.dto.PermanentResidenceDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.PermanentResidenceService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class PermanentResidenceServiceImpl implements PermanentResidenceService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public PermanentResidenceServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addPermanentResidenceRegistration(PermanentResidenceDTO permanentResidenceInfo) {
        try {
            dbService.getDao(PermanentResidenceDAO.class).addNewEntity(permanentResidenceInfo);

            String message = "";

            message = "Your request for Permanent Residence registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + permanentResidenceInfo.getTokenId() + ".";
            mailService.sendEmail(message, permanentResidenceInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<PermanentResidenceDTO> getPermanentResidenceRegistrations(HttpServletRequest request) {
        List<PermanentResidenceDTO> residenceReg = new ArrayList<>();
        try {
            residenceReg = dbService.getDao(PermanentResidenceDAO.class).getPermanentResidenceRegistrations();
            return residenceReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public PermanentResidenceDTO getPermanentResidenceCertificateByTokenId(String tokenId) {
        PermanentResidenceDTO residenceInfo = new PermanentResidenceDTO();
        residenceInfo = dbService.getDao(PermanentResidenceDAO.class).getPermanentResidenceCertificateByTokenId(tokenId);
        if(residenceInfo.getWardSecretarySignature() != null){
            residenceInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getWardSecretarySignature()));
        }
        residenceInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getApplicationPhoto()));
        residenceInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getCitizenshipPhoto()));
        residenceInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getLandOwnershipCertificate()));
        residenceInfo.setResettlementCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getResettlementCertificate()));
        residenceInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, residenceInfo.getTaxInformationPhoto()));

        return residenceInfo;
    }


    @Override
    public void updatePermanentResidenceCertificateByTokenId(PermanentResidenceDTO permanentResidenceInfo, String tokenId) {
        try {
            dbService.getDao(PermanentResidenceDAO.class).updatePermanentResidenceCertificateByTokenId(permanentResidenceInfo, tokenId);
            dbService.getDao(PermanentResidenceDAO.class).approvePermanentResidenceCertificate(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(PermanentResidenceDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

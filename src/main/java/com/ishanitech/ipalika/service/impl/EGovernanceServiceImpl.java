package com.ishanitech.ipalika.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;

import com.ishanitech.ipalika.dto.FormInfoDTO;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.converter.impl.LgDistrictConverter;
import com.ishanitech.ipalika.converter.impl.LgMunicipalityConverter;
import com.ishanitech.ipalika.converter.impl.LgProvinceConverter;
import com.ishanitech.ipalika.converter.impl.LgWardConverter;
import com.ishanitech.ipalika.dao.EGovernanceDAO;
import com.ishanitech.ipalika.dao.LgDistrictDAO;
import com.ishanitech.ipalika.dao.LgMunicipalityDAO;
import com.ishanitech.ipalika.dao.LgProvinceDAO;
import com.ishanitech.ipalika.dao.LgWardDAO;
import com.ishanitech.ipalika.dto.BirthCertificateDTO;
import com.ishanitech.ipalika.dto.RelationCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.EGovernanceService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;

@Service
public class EGovernanceServiceImpl implements EGovernanceService {
	
	private final DbService dbService;
	private final FileUtilService fileUtilService;
	private final MailService mailService;
	
	
	@Autowired
	private RestBaseProperty restUrlProperty;
	
	public EGovernanceServiceImpl(DbService dbService, FileUtilService fileUtilService, MailService mailService) {
		this.dbService = dbService;
		this.fileUtilService = fileUtilService;
		this.mailService = mailService;
	}



	@Override
	public List<BirthCertificateDTO> getBirthRegistrations(HttpServletRequest request) {
//		String caseQuery = CustomQueryCreator.generateQueryWithCase(request, PaginationTypeClass.FAV_PLACES);
		List<BirthCertificateDTO> birthReg = new ArrayList<>();
		List <FormInfoDTO> forminfos = new ArrayList();

		try {
			birthReg = dbService.getDao(EGovernanceDAO.class).getBirthRegistrations();
			forminfos = dbService.getDao(EGovernanceDAO.class).getAllFormsInfoData();
//			for(FormInfoDTO form: forminfos){
//				System.out.println(form.getForm_type().toUpperCase()+"("+form.getId()+"),");
//			};

			return birthReg;
			
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
		}
	}



	@Override
	public void addBirthRegistration(BirthCertificateDTO birthRegistrationInfo) {
		try {
			dbService.getDao(EGovernanceDAO.class).addNewBirth(birthRegistrationInfo);
			
			String message = "";
			
			message = "Your request for birth registration certificate has been succesfully created and is awaiting verification. <br>"
					+ " Please wait for admin to verify your account and bring this token to track the record..<br>"
					+ " Your unique token id is " + birthRegistrationInfo.getTokenId() + ".";
			mailService.sendEmail(message, birthRegistrationInfo.getEmail());
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
		}
	}



	@Override
	public BirthCertificateDTO getBirthCertificateByTokenId(String tokenId) {
		BirthCertificateDTO birthInfo = new BirthCertificateDTO();
		
		
		
		birthInfo = dbService.getDao(EGovernanceDAO.class).getBirthRegistrationByTokenId(tokenId);
		birthInfo.setFatherIdImg(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getFatherIdImg()));
		birthInfo.setMotherIdImg(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getMotherIdImg()));
		birthInfo.setBirthPlaceCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getBirthPlaceCertificate()));

		birthInfo.setProvince(new LgProvinceConverter().fromEntity(dbService.getDao(LgProvinceDAO.class).getProvinceById(Integer.parseInt(birthInfo.getLgProvince()))));
		birthInfo.setDistrict(new LgDistrictConverter().fromEntity(dbService.getDao(LgDistrictDAO.class).getDistrictById(Integer.parseInt(birthInfo.getLgDistrict()))));
		birthInfo.setMunicipality(new LgMunicipalityConverter().fromEntity(dbService.getDao(LgMunicipalityDAO.class).getMunicipalityById(Integer.parseInt(birthInfo.getLgMunicipality()))));
		birthInfo.setWard(new LgWardConverter().fromEntity(dbService.getDao(LgWardDAO.class).getWardById(Integer.parseInt(birthInfo.getLgWard()))));
		
		return birthInfo;
	}



	@Override
	public void updateBirthCertificateByTokenId(BirthCertificateDTO birthRegistrationInfo, String tokenId) {
		try {
			dbService.getDao(EGovernanceDAO.class).updateBirthRegistrationByTokenId(birthRegistrationInfo, tokenId);
			dbService.getDao(EGovernanceDAO.class).approveBirthRegistration(tokenId);
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
		}
		
	}



	@Override
	public void updateBirthCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
		try {
			dbService.getDao(EGovernanceDAO.class).updateStatus(tokenId, status, formInfoId);
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
		}
		
	}

	@Override
	public void updateRelationCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
		try {
			dbService.getDao(EGovernanceDAO.class).updateRelationStatus(tokenId, status, formInfoId);
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
		}
	}

	@Override
	public void addBirthCertificateImage(MultipartFile image) {
		fileUtilService.storeFile(image);
	}


	
	
	

	@Override
	public List<RelationCertificateDTO> getRelationRegistrations(HttpServletRequest request) {
		List<RelationCertificateDTO> relationReg = new ArrayList<>();
		try {
			relationReg = dbService.getDao(EGovernanceDAO.class).getRelationRegistrations();
			return relationReg;
			
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
		}
	}



	@Override
	public void addRelationRegistration(RelationCertificateDTO relationRegistrationInfo) {
		try {
			dbService.getDao(EGovernanceDAO.class).addNewRelation(relationRegistrationInfo);
			
			String message = "";
			
			message = "Your request for relationship registration certificate has been succesfully created and is awaiting verification. <br>"
					+ " Please wait for admin to verify your account and bring this token to track the record..<br>"
					+ " Your unique token id is " + relationRegistrationInfo.getTokenId() + ".";
			mailService.sendEmail(message, relationRegistrationInfo.getEmail());
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
		}
	}



	@Override
	public RelationCertificateDTO getRelationCertificateByTokenId(String tokenId) {
		RelationCertificateDTO relationInfo = new RelationCertificateDTO();
		relationInfo = dbService.getDao(EGovernanceDAO.class).getRelationRegistrationByTokenId(tokenId);
		if(relationInfo.getWardSecretarySignature() != null){
			relationInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getWardSecretarySignature()));
		}
		relationInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getApplicationPhoto()));
		relationInfo.setCitizenshipPhotoApplicant(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getCitizenshipPhotoApplicant()));
		relationInfo.setPpPhotoApplicant(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getPpPhotoApplicant()));
		relationInfo.setCitizenshipPhotoRelative(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getCitizenshipPhotoRelative()));
		relationInfo.setPpPhotoRelative(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getPpPhotoRelative()));
		relationInfo.setRelationshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getRelationshipPhoto()));
		relationInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, relationInfo.getTaxInformationPhoto()));
		
		return relationInfo;
	}



	@Override
	public void updateRelationCertificateByTokenId(RelationCertificateDTO relationRegistrationInfo, String tokenId) {
		try {
			dbService.getDao(EGovernanceDAO.class).updateRelationRegistrationByTokenId(relationRegistrationInfo, tokenId);
			dbService.getDao(EGovernanceDAO.class).approveRelationRegistration(tokenId);
		} catch (JdbiException jex) {
			throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
		}
	}



	@Override
	public void uploadRelationCertificate(MultipartFile image) {
		fileUtilService.storeFile(image);
	}


}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.BatoKayamDAO;
import com.ishanitech.ipalika.dto.BatoKayamDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.BatoKayamService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BatoKayamServiceImpl implements BatoKayamService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public BatoKayamServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addBatoKayamRegistration(BatoKayamDTO batoRegistrationInfo) {
        try {
            dbService.getDao(BatoKayamDAO.class).addNewEntity(batoRegistrationInfo);

            String message = "";

            message = "Your request for Bato Kayam Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + batoRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, batoRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<BatoKayamDTO> getBatoKayamRegistrations(HttpServletRequest request) {
        List<BatoKayamDTO> batoReg = new ArrayList<>();
        try {
            batoReg = dbService.getDao(BatoKayamDAO.class).getBatoKayamRegistrations();
            return batoReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public BatoKayamDTO getBatoKayamCertificateByTokenId(String tokenId) {
        BatoKayamDTO batoInfo = new BatoKayamDTO();
        batoInfo = dbService.getDao(BatoKayamDAO.class).getBatoKayamCertificateByTokenId(tokenId);
        if(batoInfo.getWardSecretarySignature() != null){
            batoInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, batoInfo.getWardSecretarySignature()));
        }
        batoInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, batoInfo.getApplicationPhoto()));
        batoInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, batoInfo.getLandOwnershipCertificate()));
        batoInfo.setSurveyMap(ImageUtilService.makeFullImageurl(restUrlProperty, batoInfo.getSurveyMap()));
        batoInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, batoInfo.getTaxInformationPhoto()));
        batoInfo.setLandOwnerApprovalLetter(ImageUtilService.makeFullImageurl(restUrlProperty, batoInfo.getLandOwnerApprovalLetter()));

        return batoInfo;
    }


    @Override
    public void updateBatoKayamCertificateByTokenId(BatoKayamDTO batoRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(BatoKayamDAO.class).updateBatoKayamRegistrationByTokenId(batoRegistrationInfo, tokenId);
            dbService.getDao(BatoKayamDAO.class).approveBatoKayamRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(BatoKayamDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

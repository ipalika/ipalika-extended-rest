package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.EnglishVerificationDAO;
import com.ishanitech.ipalika.dto.EnglishVerificationDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.EnglishVerificationService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class EnglishVerificationServiceImpl implements EnglishVerificationService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public EnglishVerificationServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService) {
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public List<EnglishVerificationDTO> getEnglishVerifications(HttpServletRequest request) {
        List<EnglishVerificationDTO> englishReg = new ArrayList<>();
        try {
            englishReg = dbService.getDao(EnglishVerificationDAO.class).getEnglishVerifications();
            return englishReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }

    }


    @Override
    public void addEnglishVerification(EnglishVerificationDTO englishVerificationInfo) {
        try {
            dbService.getDao(EnglishVerificationDAO.class).addNewEntity(englishVerificationInfo);

            String message = "";

            message = "Your request for English Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + englishVerificationInfo.getTokenId() + ".";
            mailService.sendEmail(message, englishVerificationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image) {
        fileUtilService.storeFile(image);
    }


    @Override
    public EnglishVerificationDTO getEnglishVerificationByTokenId(String tokenId){
        EnglishVerificationDTO englishInfo = new EnglishVerificationDTO();
        englishInfo = dbService.getDao(EnglishVerificationDAO.class).getEnglishVerificationByTokenId(tokenId);
        if(englishInfo.getWardSecretarySignature() != null){
            englishInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, englishInfo.getWardSecretarySignature()));
        }
        englishInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, englishInfo.getApplicationPhoto()));
        englishInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, englishInfo.getCitizenshipPhoto()));
        englishInfo.setProofDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, englishInfo.getProofDocument1()));
        englishInfo.setProofDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, englishInfo.getProofDocument2()));
        englishInfo.setProofDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, englishInfo.getProofDocument3()));
        englishInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, englishInfo.getTaxInformationPhoto()));

        return englishInfo;
    }


    public void updateEnglishVerificationByTokenId(EnglishVerificationDTO englishVerificationInfo,String tokenId){
        try{
            dbService.getDao(EnglishVerificationDAO.class).updateEnglishVerificationByTokenId(englishVerificationInfo, tokenId);
            dbService.getDao(EnglishVerificationDAO.class).approveEnglishVerification(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(EnglishVerificationDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.QuadrupedDAO;
import com.ishanitech.ipalika.dto.QuadrupedDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.QuadrupedService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class QuadrupedServiceImpl implements QuadrupedService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public QuadrupedServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addQuadrupedRegistration(QuadrupedDTO quadrupedRegistrationInfo) {
        try {
            dbService.getDao(QuadrupedDAO.class).addNewEntity(quadrupedRegistrationInfo);

            String message = "";

            message = "Your request for Quadruped Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + quadrupedRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, quadrupedRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<QuadrupedDTO> getQuadrupedRegistrations(HttpServletRequest request) {
        List<QuadrupedDTO> quadrupedReg = new ArrayList<>();
        try {
            quadrupedReg = dbService.getDao(QuadrupedDAO.class).getQuadrupedRegistrations();
            return quadrupedReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public QuadrupedDTO getQuadrupedCertificateByTokenId(String tokenId) {
        QuadrupedDTO quadrupedInfo = new QuadrupedDTO();
        quadrupedInfo = dbService.getDao(QuadrupedDAO.class).getQuadrupedCertificateByTokenId(tokenId);
        if(quadrupedInfo.getWardSecretarySignature() != null){
            quadrupedInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, quadrupedInfo.getWardSecretarySignature()));
        }
        quadrupedInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, quadrupedInfo.getApplicationPhoto()));
        quadrupedInfo.setApprovalLetter(ImageUtilService.makeFullImageurl(restUrlProperty, quadrupedInfo.getApprovalLetter()));
        quadrupedInfo.setGiverIdentificationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, quadrupedInfo.getGiverIdentificationPhoto()));
        quadrupedInfo.setReceiverIdentificationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, quadrupedInfo.getReceiverIdentificationPhoto()));
        quadrupedInfo.setNoEffectOfRearingOnSurroundingConfirmation(ImageUtilService.makeFullImageurl(restUrlProperty, quadrupedInfo.getNoEffectOfRearingOnSurroundingConfirmation()));

        return quadrupedInfo;
    }


    @Override
    public void updateQuadrupedCertificateByTokenId(QuadrupedDTO quadrupedRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(QuadrupedDAO.class).updateQuadrupedRegistrationByTokenId(quadrupedRegistrationInfo, tokenId);
            dbService.getDao(QuadrupedDAO.class).approveQuadrupedRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(QuadrupedDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

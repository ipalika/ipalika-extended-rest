package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.FourFortsDAO;
import com.ishanitech.ipalika.dto.FourFortsDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.FourFortsService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class FourFortsServiceImpl implements FourFortsService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public FourFortsServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addFourFortsRegistration(FourFortsDTO fortRegistrationInfo) {
        try {
            dbService.getDao(FourFortsDAO.class).addNewEntity(fortRegistrationInfo);

            String message = "";

            message = "Your request for Four Forts Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + fortRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, fortRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<FourFortsDTO> getFourFortsRegistrations(HttpServletRequest request) {
        List<FourFortsDTO> fortReg = new ArrayList<>();
        try {
            fortReg = dbService.getDao(FourFortsDAO.class).getFourFortsRegistrations();
            return fortReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public FourFortsDTO getFourFortsCertificateByTokenId(String tokenId) {
        FourFortsDTO fortInfo = new FourFortsDTO();
        fortInfo = dbService.getDao(FourFortsDAO.class).getFourFortsCertificateByTokenId(tokenId);
        if(fortInfo.getWardSecretarySignature() != null){
            fortInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, fortInfo.getWardSecretarySignature()));
        }
        fortInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, fortInfo.getApplicationPhoto()));
        fortInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, fortInfo.getCitizenshipPhoto()));
        fortInfo.setLandOwnershipCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, fortInfo.getLandOwnershipCertificate()));
        fortInfo.setLandAreaSurveyMap(ImageUtilService.makeFullImageurl(restUrlProperty, fortInfo.getLandAreaSurveyMap()));
        fortInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, fortInfo.getTaxInformationPhoto()));
        fortInfo.setAuthorizedHeirCopy(ImageUtilService.makeFullImageurl(restUrlProperty, fortInfo.getAuthorizedHeirCopy()));

        return fortInfo;
    }


    @Override
    public void updateFourFortsCertificateByTokenId(FourFortsDTO fortRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(FourFortsDAO.class).updateFourFortsRegistrationByTokenId(fortRegistrationInfo, tokenId);
            dbService.getDao(FourFortsDAO.class).approveFourFortsRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(FourFortsDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.BusinessDisclosureDAO;
import com.ishanitech.ipalika.dto.BusinessDisclosureDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.BusinessDisclosureService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BusinessDisclosureServiceImpl implements BusinessDisclosureService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public BusinessDisclosureServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addBusinessRegistration(BusinessDisclosureDTO businessRegistrationInfo) {
        try {
            dbService.getDao(BusinessDisclosureDAO.class).addNewEntity(businessRegistrationInfo);

            String message = "";

            message = "Your request for 'Business Disclosure Registration' Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + businessRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, businessRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<BusinessDisclosureDTO> getBusinessRegistrations(HttpServletRequest request) {
        List<BusinessDisclosureDTO> businessReg = new ArrayList<>();
        try {
            businessReg = dbService.getDao(BusinessDisclosureDAO.class).getBusinessRegistrations();
            return businessReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public BusinessDisclosureDTO getBusinessCertificateByTokenId(String tokenId) {
        BusinessDisclosureDTO businessInfo = new BusinessDisclosureDTO();
        businessInfo = dbService.getDao(BusinessDisclosureDAO.class).getBusinessCertificateByTokenId(tokenId);
        if(businessInfo.getWardSecretarySignature() != null){
            businessInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getWardSecretarySignature()));
        }
        businessInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getApplicationPhoto()));
        businessInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getCitizenshipPhoto()));
        businessInfo.setBusinessRenewalCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getBusinessRenewalCertificate()));
        businessInfo.setGharBahalAgreement(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getGharBahalAgreement()));
        businessInfo.setOnsiteReport(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getOnsiteReport()));
        businessInfo.setIdentityDocumentIfForeigner(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getIdentityDocumentIfForeigner()));
        businessInfo.setLetterFromEmbassy(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getLetterFromEmbassy()));
        businessInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, businessInfo.getTaxInformationPhoto()));

        return businessInfo;
    }


    @Override
    public void updateBusinessCertificateByTokenId(BusinessDisclosureDTO businessRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(BusinessDisclosureDAO.class).updateBusinessRegistrationByTokenId(businessRegistrationInfo, tokenId);
            dbService.getDao(BusinessDisclosureDAO.class).approveBusinessRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(BusinessDisclosureDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

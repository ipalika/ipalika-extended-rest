package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.DivorceDAO;
import com.ishanitech.ipalika.dto.DivorceCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.DivorceService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class DivorceServiceImpl implements DivorceService {


    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public DivorceServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addDivorceRegistration(DivorceCertificateDTO divorceRegistrationInfo) {
        try {
            dbService.getDao(DivorceDAO.class).addNewEntity(divorceRegistrationInfo);

            String message = "";

            message = "Your request for divorce registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + divorceRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, divorceRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<DivorceCertificateDTO> getDivorceRegistrations(HttpServletRequest request) {
        List<DivorceCertificateDTO> divorceReg = new ArrayList<>();
        try {
            divorceReg = dbService.getDao(DivorceDAO.class).getDivorceRegistrations();
            return divorceReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public DivorceCertificateDTO getDivorceCertificateByTokenId(String tokenId) {
        DivorceCertificateDTO divorceInfo = new DivorceCertificateDTO();
        divorceInfo = dbService.getDao(DivorceDAO.class).getDivorceRegistrationByTokenId(tokenId);
        if(divorceInfo.getWardSecretarySignature() != null){
            divorceInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, divorceInfo.getWardSecretarySignature()));
        }
        divorceInfo.setHusbandCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, divorceInfo.getHusbandCitizenshipPhoto()));
        divorceInfo.setWifeCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, divorceInfo.getWifeCitizenshipPhoto()));
        divorceInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, divorceInfo.getApplicationPhoto()));
        divorceInfo.setDissolutionCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, divorceInfo.getDissolutionCertificate()));

        return divorceInfo;
    }


    @Override
    public void updateDivorceCertificateByTokenId(DivorceCertificateDTO divorceRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(DivorceDAO.class).updateDivorceRegistrationByTokenId(divorceRegistrationInfo, tokenId);
            dbService.getDao(DivorceDAO.class).approveDivorceRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(DivorceDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}



package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.PersonalDetailsDAO;
import com.ishanitech.ipalika.dto.PersonalDetailsDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.PersonalDetailsService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonalDetailsServiceImpl implements PersonalDetailsService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public PersonalDetailsServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addPersonalDetailsRegistration(PersonalDetailsDTO personalRegistrationInfo) {
        try {
            dbService.getDao(PersonalDetailsDAO.class).addNewEntity(personalRegistrationInfo);

            String message = "";

            message = "Your request for Personal Details Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + personalRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, personalRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<PersonalDetailsDTO> getPersonalDetailsRegistrations(HttpServletRequest request) {
        List<PersonalDetailsDTO> personalReg = new ArrayList<>();
        try {
            personalReg = dbService.getDao(PersonalDetailsDAO.class).getPersonalDetailsRegistrations();
            return personalReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public PersonalDetailsDTO getPersonalDetailsCertificateByTokenId(String tokenId) {
        PersonalDetailsDTO personalInfo = new PersonalDetailsDTO();
        personalInfo = dbService.getDao(PersonalDetailsDAO.class).getPersonalDetailsCertificateByTokenId(tokenId);
        if(personalInfo.getWardSecretarySignature() != null){
            personalInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, personalInfo.getWardSecretarySignature()));
        }
        personalInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, personalInfo.getApplicationPhoto()));
        personalInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, personalInfo.getCitizenshipPhoto()));
        personalInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, personalInfo.getTaxInformationPhoto()));
        personalInfo.setProofDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, personalInfo.getProofDocument1()));
        personalInfo.setProofDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, personalInfo.getProofDocument2()));
        personalInfo.setProofDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, personalInfo.getProofDocument3()));

        return personalInfo;
    }


    @Override
    public void updatePersonalDetailsCertificateByTokenId(PersonalDetailsDTO personalRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(PersonalDetailsDAO.class).updatePersonalDetailsRegistrationByTokenId(personalRegistrationInfo, tokenId);
            dbService.getDao(PersonalDetailsDAO.class).approvePersonalDetailsRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(PersonalDetailsDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

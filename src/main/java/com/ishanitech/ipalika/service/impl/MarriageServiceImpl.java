package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.MarriageDAO;
import com.ishanitech.ipalika.dto.MarriageCertificateDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.MarriageService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.xa.XAException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MarriageServiceImpl implements MarriageService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public MarriageServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService) {
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public List<MarriageCertificateDTO> getMarriageRegistration(HttpServletRequest request) {
        List<MarriageCertificateDTO> marriageReg = new ArrayList<>();
        try {
            marriageReg = dbService.getDao(MarriageDAO.class).getMarriageRegistration();
            return marriageReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }

    }


    @Override
    public void addMarriageRegistration(MarriageCertificateDTO marriageRegistrationInfo) {
        try {
            dbService.getDao(MarriageDAO.class).addNewEntity(marriageRegistrationInfo);

            String message = "";

            message = "Your request for marriage registration certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + marriageRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, marriageRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image) {
        fileUtilService.storeFile(image);
    }


    @Override
    public MarriageCertificateDTO getMarriageCertificateByTokenId(String tokenId){
        MarriageCertificateDTO marriageInfo = new MarriageCertificateDTO();
        marriageInfo = dbService.getDao(MarriageDAO.class).getMarriageRegistrationByTokenId(tokenId);
        if(marriageInfo.getWardSecretarySignature() != null){
            marriageInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getWardSecretarySignature()));
        }
        marriageInfo.setMarriageApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getMarriageApplicationPhoto()));
        marriageInfo.setHusbandCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getHusbandCitizenshipPhoto()));
        marriageInfo.setWifeCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getWifeCitizenshipPhoto()));
        marriageInfo.setBrideFamilyCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getBrideFamilyCitizenshipPhoto()));
        marriageInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, marriageInfo.getTaxInformationPhoto()));
        return marriageInfo;
    }


    public void updateMarriageCertificateByTokenId(MarriageCertificateDTO marriageRegistrationInfo,String tokenId){
      try{
          dbService.getDao(MarriageDAO.class).updateMarriageRegistrationByTokenId(marriageRegistrationInfo, tokenId);
          dbService.getDao(MarriageDAO.class).approveMarriageRegistration(tokenId);
      } catch (JdbiException jex) {
          throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
      }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(MarriageDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

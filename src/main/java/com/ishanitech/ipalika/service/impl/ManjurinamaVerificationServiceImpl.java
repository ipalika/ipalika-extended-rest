package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.ManjurinamaVerificationDAO;
import com.ishanitech.ipalika.dto.ManjurinamaVerificationDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.service.ManjurinamaVerificationService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class ManjurinamaVerificationServiceImpl implements ManjurinamaVerificationService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public ManjurinamaVerificationServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService) {
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public List<ManjurinamaVerificationDTO> getManjurinamaVerifications(HttpServletRequest request) {
        List<ManjurinamaVerificationDTO> manjurinamaReg = new ArrayList<>();
        try {
            manjurinamaReg = dbService.getDao(ManjurinamaVerificationDAO.class).getManjurinamaVerifications();
            return manjurinamaReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }

    }


    @Override
    public void addManjurinamaVerification(ManjurinamaVerificationDTO manjurinamaVerificationInfo) {
        try {
            dbService.getDao(ManjurinamaVerificationDAO.class).addNewEntity(manjurinamaVerificationInfo);

            String message = "";

            message = "Your request for Manjurinama Verification Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + manjurinamaVerificationInfo.getTokenId() + ".";
            mailService.sendEmail(message, manjurinamaVerificationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image) {
        fileUtilService.storeFile(image);
    }


    @Override
    public ManjurinamaVerificationDTO getManjurinamaVerificationByTokenId(String tokenId){
        ManjurinamaVerificationDTO manjurinamaInfo = new ManjurinamaVerificationDTO();
        manjurinamaInfo = dbService.getDao(ManjurinamaVerificationDAO.class).getManjurinamaVerificationByTokenId(tokenId);
        if(manjurinamaInfo.getWardSecretarySignature() != null){
            manjurinamaInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, manjurinamaInfo.getWardSecretarySignature()));
        }
        manjurinamaInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, manjurinamaInfo.getApplicationPhoto()));
        manjurinamaInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, manjurinamaInfo.getCitizenshipPhoto()));
        manjurinamaInfo.setProofDocument1(ImageUtilService.makeFullImageurl(restUrlProperty, manjurinamaInfo.getProofDocument1()));
        manjurinamaInfo.setProofDocument2(ImageUtilService.makeFullImageurl(restUrlProperty, manjurinamaInfo.getProofDocument2()));
        manjurinamaInfo.setProofDocument3(ImageUtilService.makeFullImageurl(restUrlProperty, manjurinamaInfo.getProofDocument3()));
        manjurinamaInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, manjurinamaInfo.getTaxInformationPhoto()));

        return manjurinamaInfo;
    }


    public void updateManjurinamaVerificationByTokenId(ManjurinamaVerificationDTO manjurinamaVerificationInfo,String tokenId){
        try{
            dbService.getDao(ManjurinamaVerificationDAO.class).updateManjurinamaVerificationByTokenId(manjurinamaVerificationInfo, tokenId);
            dbService.getDao(ManjurinamaVerificationDAO.class).approveManjurinamaVerification(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(ManjurinamaVerificationDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.GuardianPersonalDAO;
import com.ishanitech.ipalika.dto.GuardianPersonalDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.GuardianPersonalService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class GuardianPersonalServiceImpl implements GuardianPersonalService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public GuardianPersonalServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addGuardianRegistration(GuardianPersonalDTO guardianRegistrationInfo) {
        try {
            dbService.getDao(GuardianPersonalDAO.class).addNewEntity(guardianRegistrationInfo);

            String message = "";

            message = "Your request for Guardian Recommendation (Personal) Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + guardianRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, guardianRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<GuardianPersonalDTO> getGuardianRegistrations(HttpServletRequest request) {
        List<GuardianPersonalDTO> guardianReg = new ArrayList<>();
        try {
            guardianReg = dbService.getDao(GuardianPersonalDAO.class).getGuardianRegistrations();
            return guardianReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public GuardianPersonalDTO getGuardianCertificateByTokenId(String tokenId) {
        GuardianPersonalDTO guardianInfo = new GuardianPersonalDTO();
        guardianInfo = dbService.getDao(GuardianPersonalDAO.class).getGuardianCertificateByTokenId(tokenId);
        if(guardianInfo.getWardSecretarySignature() != null){
            guardianInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getWardSecretarySignature()));
        }
        guardianInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getApplicationPhoto()));
        guardianInfo.setGuardianGiverCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getGuardianGiverCitizenshipPhoto()));
        guardianInfo.setGuardianGiverBirthCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getGuardianGiverBirthCertificate()));
        guardianInfo.setGuardianReceiverCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getGuardianReceiverCitizenshipPhoto()));
        guardianInfo.setGuardianReceiverBirthCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getGuardianReceiverBirthCertificate()));
        guardianInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getTaxInformationPhoto()));
        guardianInfo.setOnsiteSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getOnsiteSarjaminMuchulka()));
        guardianInfo.setLocalSarjaminMuchulka(ImageUtilService.makeFullImageurl(restUrlProperty, guardianInfo.getLocalSarjaminMuchulka()));

        return guardianInfo;
    }


    @Override
    public void updateGuardianCertificateByTokenId(GuardianPersonalDTO guardianRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(GuardianPersonalDAO.class).updateGuardianRegistrationByTokenId(guardianRegistrationInfo, tokenId);
            dbService.getDao(GuardianPersonalDAO.class).approveGuardianRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(GuardianPersonalDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

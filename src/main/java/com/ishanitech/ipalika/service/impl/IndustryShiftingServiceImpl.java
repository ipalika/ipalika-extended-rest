package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.IndustryShiftingDAO;
import com.ishanitech.ipalika.dto.IndustryShiftingDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.IndustryShiftingService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class IndustryShiftingServiceImpl implements IndustryShiftingService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public IndustryShiftingServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService){
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public void addIndustryRegistration(IndustryShiftingDTO industryRegistrationInfo) {
        try {
            dbService.getDao(IndustryShiftingDAO.class).addNewEntity(industryRegistrationInfo);

            String message = "";

            message = "Your request for Industry Shifting Registration Certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + industryRegistrationInfo.getTokenId() + ".";
            mailService.sendEmail(message, industryRegistrationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image){
        fileUtilService.storeFile(image);
    }


    @Override
    public List<IndustryShiftingDTO> getIndustryRegistrations(HttpServletRequest request) {
        List<IndustryShiftingDTO> industryReg = new ArrayList<>();
        try {
            industryReg = dbService.getDao(IndustryShiftingDAO.class).getIndustryRegistrations();
            return industryReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception: " + jex.getLocalizedMessage());
        }
    }


    @Override
    public IndustryShiftingDTO getIndustryCertificateByTokenId(String tokenId) {
        IndustryShiftingDTO industryInfo = new IndustryShiftingDTO();
        industryInfo = dbService.getDao(IndustryShiftingDAO.class).getIndustryCertificateByTokenId(tokenId);
        if(industryInfo.getWardSecretarySignature() != null){
            industryInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, industryInfo.getWardSecretarySignature()));
        }
        industryInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, industryInfo.getApplicationPhoto()));
        industryInfo.setIndustryRegistrationCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, industryInfo.getIndustryRegistrationCertificate()));
        industryInfo.setLocalLevelRenewalCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, industryInfo.getLocalLevelRenewalCertificate()));
        industryInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, industryInfo.getTaxInformationPhoto()));
        industryInfo.setBahalAgreementPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, industryInfo.getBahalAgreementPhoto()));
        industryInfo.setBahalTaxInformation(ImageUtilService.makeFullImageurl(restUrlProperty, industryInfo.getBahalTaxInformation()));

        return industryInfo;
    }


    @Override
    public void updateIndustryCertificateByTokenId(IndustryShiftingDTO industryRegistrationInfo, String tokenId) {
        try {
            dbService.getDao(IndustryShiftingDAO.class).updateIndustryRegistrationByTokenId(industryRegistrationInfo, tokenId);
            dbService.getDao(IndustryShiftingDAO.class).approveIndustryRegistration(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(IndustryShiftingDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }
}

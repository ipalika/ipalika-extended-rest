package com.ishanitech.ipalika.service.impl;

import com.ishanitech.ipalika.config.properties.RestBaseProperty;
import com.ishanitech.ipalika.dao.AdvertisementTaxDAO;
import com.ishanitech.ipalika.dao.BirthVerifyDAO;
import com.ishanitech.ipalika.dto.BirthVerifyDTO;
import com.ishanitech.ipalika.exception.CustomSqlException;
import com.ishanitech.ipalika.service.BirthVerifyService;
import com.ishanitech.ipalika.service.DbService;
import com.ishanitech.ipalika.service.MailService;
import com.ishanitech.ipalika.utils.FileUtilService;
import com.ishanitech.ipalika.utils.ImageUtilService;
import org.jdbi.v3.core.JdbiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BirthVerifyServiceImpl implements BirthVerifyService {

    private final DbService dbService;
    private final MailService mailService;
    private final FileUtilService fileUtilService;

    @Autowired
    private RestBaseProperty restUrlProperty;

    public BirthVerifyServiceImpl(DbService dbService, MailService mailService, FileUtilService fileUtilService) {
        this.dbService = dbService;
        this.mailService = mailService;
        this.fileUtilService = fileUtilService;
    }


    @Override
    public List<BirthVerifyDTO> getBirthVerifications(HttpServletRequest request) {
        List<BirthVerifyDTO> birthReg = new ArrayList<>();
        try {
            birthReg = dbService.getDao(BirthVerifyDAO.class).getBirthVerifications();
            return birthReg;

        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }

    }


    @Override
    public void addBirthVerification(BirthVerifyDTO birthVerificationInfo) {
        try {
            dbService.getDao(BirthVerifyDAO.class).addNewEntity(birthVerificationInfo);

            String message = "";

            message = "Your request for birth verification certificate has been successfully created and is awaiting verification. <br>"
                    + " Please wait for admin to verify your account and bring this token to track the record..<br>"
                    + " Your unique token id is " + birthVerificationInfo.getTokenId() + ".";
            mailService.sendEmail(message, birthVerificationInfo.getEmail());
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception :" + jex.getLocalizedMessage());
        }
    }


    @Override
    public void addAllImages(MultipartFile image) {
        fileUtilService.storeFile(image);
    }


    @Override
    public BirthVerifyDTO getBirthVerificationByTokenId(String tokenId){
        BirthVerifyDTO birthInfo = new BirthVerifyDTO();
        birthInfo = dbService.getDao(BirthVerifyDAO.class).getBirthVerificationByTokenId(tokenId);
        if(birthInfo.getWardSecretarySignature() != null){
            birthInfo.setWardSecretarySignature(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getWardSecretarySignature()));
        }
        birthInfo.setApplicationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getApplicationPhoto()));
        birthInfo.setCitizenshipPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getCitizenshipPhoto()));
        birthInfo.setBirthCertificateMinor(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getBirthCertificateMinor()));
        birthInfo.setResettlementCertificate(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getResettlementCertificate()));
        birthInfo.setTaxInformationPhoto(ImageUtilService.makeFullImageurl(restUrlProperty, birthInfo.getTaxInformationPhoto()));

        return birthInfo;
    }


    public void updateBirthVerificationByTokenId(BirthVerifyDTO birthVerificationInfo,String tokenId){
        try{
            dbService.getDao(BirthVerifyDAO.class).updateBirthVerificationByTokenId(birthVerificationInfo, tokenId);
            dbService.getDao(BirthVerifyDAO.class).approveBirthVerification(tokenId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception:" + jex.getLocalizedMessage());
        }
    }

    @Override
    public void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId) {
        try {
            dbService.getDao(BirthVerifyDAO.class).updateStatus(tokenId, status, formInfoId);
        } catch (JdbiException jex) {
            throw new CustomSqlException("Exception : " + jex.getLocalizedMessage());
        }
    }

}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.MohiLagatDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface MohiLagatService {

    void addMohiLagatRegistration(MohiLagatDTO mohiRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<MohiLagatDTO> getMohiLagatRegistrations(HttpServletRequest request);

    MohiLagatDTO getMohiLagatCertificateByTokenId(String tokenId);

    void updateMohiLagatCertificateByTokenId(MohiLagatDTO mohiRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

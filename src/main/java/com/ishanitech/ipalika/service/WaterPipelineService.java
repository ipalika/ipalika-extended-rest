package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.WaterPipelineDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface WaterPipelineService {

    void addPipelineRegistration(WaterPipelineDTO pipelineRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<WaterPipelineDTO> getPipelineRegistrations(HttpServletRequest request);

    WaterPipelineDTO getPipelineCertificateByTokenId(String tokenId);

    void updatePipelineCertificateByTokenId(WaterPipelineDTO minorRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.BatoKayamDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BatoKayamService {

    void addBatoKayamRegistration(BatoKayamDTO batoRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<BatoKayamDTO> getBatoKayamRegistrations(HttpServletRequest request);

    BatoKayamDTO getBatoKayamCertificateByTokenId(String tokenId);

    void updateBatoKayamCertificateByTokenId(BatoKayamDTO batoRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

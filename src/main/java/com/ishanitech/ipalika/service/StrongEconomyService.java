package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.StrongEconomyDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface StrongEconomyService {

    void addStrongEconomyRegistration(StrongEconomyDTO economyRegistrationInfo);

    public void addAllImages(MultipartFile image);

    List<StrongEconomyDTO> getStrongEconomyRegistrations(HttpServletRequest request);

    StrongEconomyDTO getStrongEconomyCertificateByTokenId(String tokenId);

    void updateStrongEconomyCertificateByTokenId(StrongEconomyDTO economyRegistrationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}

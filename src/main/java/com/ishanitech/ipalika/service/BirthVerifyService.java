package com.ishanitech.ipalika.service;

import com.ishanitech.ipalika.dto.BirthVerifyDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BirthVerifyService {

    void addBirthVerification(BirthVerifyDTO birthVerificationInfo);

    public void addAllImages(MultipartFile image);

    List<BirthVerifyDTO> getBirthVerifications(HttpServletRequest request);

    BirthVerifyDTO getBirthVerificationByTokenId(String tokenId);

    void updateBirthVerificationByTokenId(BirthVerifyDTO birthVerificationInfo, String tokenId);

    void updateCertificateStatus(String tokenId, Integer status, Integer formInfoId);
}
